#!/bin/bash


## Funsiones en paralelo ##

function my_function1() {
  # código de la función
}

function my_function2() {
  # código de la función
}

function my_function3() {
  # código de la función
}

function my_function4() {
  # código de la función
}

# Ejecutar las funciones 1 y 2 en paralelo.
my_function1 &
my_function2 &
wait


# En este ejemplo, las funciones my_function1 y my_function2 se ejecutan en paralelo en procesos separados en segundo plano utilizando el ampersand &. El comando wait se asegura de que el script espere a que se completen todas las funciones (1 y 2) en segundo plano antes de continuar.

# Ejecutar las funsiones 3 y 4 en paralelo.
my_function3 &
my_function4 &
wait

# En este ejemplo, las funciones my_function3 y my_function4 se ejecutan en paralelo en procesos separados en segundo plano utilizando el ampersand &. El comando wait se asegura de que el script espere a que se completen todas las funciones (3 y 4) en segundo plano antes de continuar.


## Scripts en paralelo ##

# Requiere del paquete "parallel".
# pacman -S parallel


# Supongamos que tienes dos scripts llamados script1.sh y script2.sh que deseas ejecutar en paralelo. Puedes hacerlo de la siguiente manera:
parallel ::: './script1.sh' './script2.sh'


# Ejecutar parallel con 4 procesos.
parallel -j 4 ::: './script1.sh' './script2.sh'
