#!/bin/bash

# La escritura en línea es una técnica similar a la lectura en línea, pero en lugar de leer archivos, se utiliza para escribir en ellos. En lugar de escribir todo el contenido del archivo en un solo bloque, la escritura en línea permite escribir en el archivo línea por línea o bloque por bloque.


# La escritura en línea es útil cuando se trabaja con archivos grandes o cuando se necesita procesar el archivo de forma progresiva en lugar de escribir todo el archivo de una vez. La técnica de escritura en línea se puede implementar fácilmente en Bash usando el comando echo y la redirección de salida (>).


# Ejemplo:
#
for i in {1..10}
do
  echo "Línea $i" >> archivo.txt
done

