#!/bin/bash

# La lectura en línea es una técnica de eficiencia para leer línea por línea los archivos, en lugar de leer todo el archivo en memoria y procesarlo en un solo bloque.

# La lectura en línea es útil cuando se trabaja con archivos grandes o cuando se necesita procesar el archivo de forma progresiva en lugar de leer todo el archivo de una vez. La técnica de lectura en línea se puede implementar fácilmente en Bash usando un bucle while y el comando read.

# Ejemplo:

while read linea
do
  echo "La línea es: $linea"
done < archivo.txt

# En este ejemplo, el bucle while lee el archivo línea por línea, y el contenido de cada línea se almacena en la variable $linea. Luego, se procesa la línea dentro del bucle, en este caso simplemente imprimiendo su contenido.

