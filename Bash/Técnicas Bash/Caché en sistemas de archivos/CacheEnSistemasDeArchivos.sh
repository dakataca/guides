#!/bin/bash


# El caché en el sistema de archivos es una técnica simple y eficiente que se integra bien con Bash y está disponible en todas las distribuciones de Linux, incluyendo Archlinux.

# Para implementar el caché en el sistema de archivos en Bash, puedes escribir datos en un archivo en el sistema de archivos y leer los datos de ese archivo cuando sea necesario recuperarlos. Aquí hay un ejemplo simple de cómo implementar el caché en el sistema de archivos en Bash:


# El comando mktemp se puede utilizar para crear un archivo temporal para almacenar los datos de caché. De esta manera, se garantiza que el archivo se creará de forma segura y no se sobrescribirá ningún archivo existente.

# El comando mktemp crea un archivo o directorio temporal único y devuelve el nombre del archivo o directorio creado. El archivo o directorio creado por mktemp tiene un nombre aleatorio que incluye información única, como el identificador del usuario, el identificador del proceso y la hora actual. Esto garantiza que el nombre del archivo o directorio sea único y que no haya conflictos con otros procesos que puedan crear archivos temporales en el mismo directorio.


## Directorios temporales ##

# Crear un directorio temporal principal llamado "CACHE_DIR". 
CACHE_DIR=$(mktemp -d)

# Crear un directorio temporal llamado "sh_cache" dentro del directorio temporal principal.
SH_CACHE_DIR=$(mktemp -d "$CACHE_DIR/sh_cache.XXXXXXXXXX")
#
# Crear un directorio temporal llamado "json_cache" dentro del directorio temporal principal.
JSON_CACHE_DIR=$(mktemp -d "$CACHE_DIR/json_cache.XXXXXXXXXX")


## Archivos temporales ##

# Crear un archivo temporal dentro de SH_CACHE_DIR y guarda su ruta en una variable.
SH_CACHE_FILE=$(mktemp "$SH_CACHE_DIR/user.XXXXXXXXXX")



# Verificar si el archivo de caché existe
if [ -f "$SH_CACHE_FILE" ]; then
    # Leer los datos de caché del archivo
    cache_data=$(cat "$SH_CACHE_FILE")
else
    # Si el archivo de caché no existe, calcular los datos y guardarlos en el archivo de caché
    cache_data=$(cal)
    echo "$cache_data" > "$SH_CACHE_FILE"
fi

# Utilizar los datos de caché
echo "$cache_data"

# Eliminar el archivo temporal de caché
rm -f "$SH_CACHE_FILE"

