[toc]



# [Xserverrc](https://wiki.archlinux.org/title/Xinit#xserverrc)

Es un script que permite iniciar Xorg con un poco más de *seguridad*.

- `vt$XDG_VTNR`: Garantizar que  [Xorg](https://wiki.archlinux.org/title/Xorg) se inice en el mismo terminal virtual donde se produjo el inicio de sesión.
- `-nolisten local`: Deshabilitar los sockets abstractos de X11 para ayudar con el aislamiento y la *seguridad* de X11.

```sh
sed -E 's/(tcp) ("\$@")$/\1 -nolisten local \2 vt\$XDG_VTNR/' /etc/X11/xinit/xserverrc > ~/.xserverrc
#sed -E '/tcp "\$@"$/ s/"\$@"/-nolisten local & vt\$XDG_VTNR/' /etc/X11/xinit/xserverrc > ~/.xserverrc
#sed -E '/-nolisten tcp "\$@"/s/(-nolisten tcp) ("\$@")/\1 -nolisten local \2 vt\$XDG_VTNR/' /etc/X11/xinit/xserverrc > ~/.xserverrc
diff /etc/X11/xinit/xserverrc ~/.xserverrc
```

> **Nota**: Para eliminar una instancia de Xorg  específica del administrador de ventanas use `pkill -15 -t tty"$XDG_VTNR" Xorg`.



# Xinitrc y Xauthority

Generar script de inicio de X `~/.xinitrc`:

```sh
echo -e '#!/bin/bash\n\n' > ~/.xinitrc
#sed -E "/^(xclock|(exec )?xterm|twm)/s/^/#/g" /etc/X11/xinit/xinitrc > ~/.xinitrc
```

Generar fichero de credenciales de `Xorg` Xauthority:

```sh
install -m 600 /dev/null ~/.Xauthority
```



## Tipos de inicio

Existen tres formas minimalistas de implementar el inicio de un entorno de escritorio con `Xinitrc`, de manera automática, manual o con el Administrador de Escritorios [Ly](https://archlinux.org/packages/?name=ly).

<table>
<thead>
    <tr>
		<th>Tipo de Inicio</th> 
        <th>Ventajas</th>
        <th>Desventajas</th>
    </tr>
</thead>
<tbody>
    <tr>
    	<th>User default 💙</th>
        <td>- No requiere el servicio de un adminstrador de escritorios. <br>- Ahorro de recursos. <br>- Permite configurar el inicio de Xorg de forma más segura y eficiente con <code>xserverrc</code></td>
        <td>- Requiere configurar manualmente los ficheros de configuración <code>~/.xserverrc</code> y <code>~/.bash_profile</code>.</td>
    </tr>
    <tr>
        <th>Manual</th>
        <td>- No requiere el servicio de un adminstrador de escritorios. <br>- Ahorro de recursos.</td>
        <td>Necesita escribir manualmente el nombre de usuario, contraseña y el comando de ejecución del entorno del servidor Xorg, a menos que habilite el <a href="https://wiki.archlinux.org/title/Xinit#Autostart_X_at_login" target="_black">autoinicio de X con el login</a>. <code>startx</code>.</td>
    </tr>
    <tr>
        <th><a href="https://archlinux.org/packages/?name=ly" target="_blank">Ly</a> DM </th>
        <td>- Minimalista. <br>- Permite personalizar un Script de Bash que contiene exclusivamente las aplicaciones que el usuario desee en el inicio del sistema.<br>- Útil para inicar entornos de escritorios como <code>Qtile</code> y aplicaciones extras como <code>Redshift</code>,<code>Picom</code>, entre otros.</td>
        <td>- Si requiere aplicaciones como <i>TeamViewer</i>🤮, que dependen de un DM gráfico como <a href="https://wiki.archlinux.org/title/LightDM" target="_blank">lightdm</a>, entonces Ly no va a ser una buena alternativa, ya que no permitirá ejcutar dichas aplicaciones. <br> - No permite el inicio en paralelo con más de un usuario 😥.</td>
    </tr>
</tbody>
</table>



### User default

Usuario predeterminado `dakataca` al inicio de sesión:

```sh
su
```

```sh
mkdir -p /etc/systemd/system/getty@tty1.service.d/
bash -c "cat > /etc/systemd/system/getty@tty1.service.d/skip-dakataca.conf" << 'EOF'
# 🌟🌟🌟 Establecer usuario predeterminado dakataca para el inicio de sesión en tty1 🌟🌟🌟 
# 🌐 wiki: https://wiki.archlinux.org/title/getty#Prompt_only_the_password_for_a_default_user_in_virtual_console_login

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -- dakataca' --noclear --skip-login - $TERM
#Environment=XDG_SESSION_TYPE=wayland
Environment=XDG_SESSION_TYPE=x11

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
systemctl enable getty@tty1.service
exit
```




#### [Auto inicio de *X* al iniciar sesión](https://wiki.archlinux.org/title/Xinit#Autostart_X_at_login) y binarios del usuario

Para iniciar *X* al iniciar sesión en las consolas virtuales `tty1` o `tty2`, coloque ésto en su fichero de configuración a nivel de usuario `~/.bash_profile`para [Bash](https://wiki.archlinux.org/title/Bash), o `~/.zprofile`para [Zsh](https://wiki.archlinux.org/title/Zsh).

- `-keeptty` [redirige la salilda *Xorg* de `stderr` y `stdout`](https://wiki.archlinux.org/title/Xorg#Session_log_redirection) "`2>&1`" al fichero `~/.xorg.log`.

```sh
mkdir -p ${XDG_CONFIG_HOME:-$HOME}/.local/bin/
cat <<'EOF' >> ~/.bash_profile
# 📂 ~/.bash_profile 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Xinit#Autostart_X_at_login
#if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -le 2 ]; then
if [ -z "${WAYLAND_DISPLAY}" ] && [ -n "$XDG_VTNR" ] && (( $XDG_VTNR <= 2 )); then
    exec sway
elif [ -z "${DISPLAY}" ] && [ -n "$XDG_VTNR" ] && (( $XDG_VTNR <= 2 )); then
 # Iniciar Xorg de manera normal (poco segura).
 #exec startx
 
 # 🌐 wiki: https://wiki.archlinux.org/title/Xorg#Session_log_redirection
 # 🌐 bbs: https://bbs.archlinux.org/viewtopic.php?pid=1446402#p1446402
 # Iniciar Xorg de manera segura.
   exec xinit -- vt$XDG_VTNR -keeptty > ~/.xorg.log 2>&1
fi

# Binarios del usuario dakataca.
export PATH="${PATH}:${XDG_CONFIG_HOME:-$HOME}/.local/bin/"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



### Manual

No necesita de configuración adicional, tan solo indicar el usuario y contraseña, y luego el comando `startx` para iniciar *X* con [`xinit`](https://wiki.archlinux.org/title/xinit).

```sh
startx
```



### Inicio con Desktop Manager [ly](https://archlinux.org/packages/?name=ly)

No recomendado para más un usuario.

```sh
su
pacman -S ly --needed --noconfirm
```

Servicio Ly DM

```sh
systemctl enable ly.service
```

> **Nota**: Requiere configurar ly en el primer inicio, para que use `xinitrc` como muestra la siguiente captura:
>
> ![xinitrc](imgs/lyXinitrc.png)
>
> Ésta configuración será recordada en los siguientes inicios automáticamente.

### Lightdm

```sh
pacman -S lightdm {,-gtk-greeter{,-settings}} --needed --noconfirm
```

```sh
systemctl enable lightdm.service
```



## Instalación del entorno gráfico para Qtile

Administrador de ventanas y escritorios, emulador de terminal, multiplexor de terminal, compositor, utilidades para el cuidado de la vista y el brillo de la pantalla, y navegadores web.

```sh
sudo pacman -S qtile python-{iwlib,dbus-next,psutil,dateutil} {alacr,k}itty xclip tmux pamixer yad picom redshift dunst mp{d,c} ncmpcpp chromium firefox xdg-desktop-portal gimp --needed --noconfirm
#pacman -S xfce4{,-{screenshooter,notifyd},-{pulseaudio,battery,mount,mpc}-plugin} lightdm-gtk-greeter {alacr,k}itty tmux picom redshift light qtile chromium firefox --needed --noconfirm
```

```sh
paru -S light
```



## Brillo mínimo de pantalla del portátil

Establecer brillo mínimo de la pantalla del portátil:

```sh
light -N 3
```



## Configuración

### Qtile --> `xinitrc`

```sh
grep '^exec qtile start$' ~/.xinitrc || echo 'exec qtile start' >> ~/.xinitrc
#grep '^exec startxfce4$' ~/.xinitrc || echo 'exec startxfce4' >> ~/.xinitrc
```

### Picom --> `xinitrc`

```sh
grep '^exec picom &$' ~/.xinitrc || sed -i '/^exec qtile start/i exec picom &' ~/.xinitrc
#grep '^exec picom &$' ~/.xinitrc || sed -i '/^exec startxfce4/i exec picom &' ~/.xinitrc
```

### Redshift --> `xinitrc`

```sh
grep '^exec redshift &$' ~/.xinitrc || sed -i '/^exec qtile start/i exec redshift &' ~/.xinitrc
#grep '^exec redshift &$' ~/.xinitrc || sed -i '/^exec startxfce4/i exec redshift &' ~/.xinitrc
```

### Dunst --> `xinitrc`

```sh
grep '^exec dunst &$' ~/.xinitrc || sed -i '/^exec qtile start/i exec dunst &' ~/.xinitrc
#grep '^exec dunst &$' ~/.xinitrc || sed -i '/^exec startxfce4/i exec dunst &' ~/.xinitrc
```

### Xauthority --> `xinitrc`

```sh
grep "^export XAUTHORITY=$HOME/.Xauthority$" ~/.xinitrc || sed -i "/^exec picom &/i export XAUTHORITY=$HOME/.Xauthority" ~/.xinitrc
```



### Permisos de ejecución para el usuario

```sh
chmod u+x ~/.xinitrc
```

```sh
diff /etc/X11/xinit/xinitrc ~/.xinitrc
............................................................
< twm &
< xclock -geometry 50x50-1+1 &
< xterm -geometry 80x50+494+51 &
< xterm -geometry 80x20+494-0 &
< exec xterm -geometry 80x66+0+0 -name login
---
> #twm &
> #xclock -geometry 50x50-1+1 &
> #xterm -geometry 80x50+494+51 &
> #xterm -geometry 80x20+494-0 &
> #exec xterm -geometry 80x66+0+0 -name login
> exec picom &
> exec redshift &
> exec qtile start
```



# Ficheros de configuración



## Qtile

Fichero de configuración por defecto:

```sh
install -D /usr/share/doc/qtile/default_config.py ~/.config/qtile/config.py
```



### Background

```sh
# https://www.wallpaperflare.com/yourname-night-anime-sky-illustration-art-wallpaper-biycp
cp imgs/background.jpg ~/.config/qtile/
```

### Ficheros de configuración estructurados

```sh
mkdir -p ~/.config/qtile/
```



#### keys

```sh
mkdir -p ~/.config/qtile/dakataca
```

```sh
cat > ~/.config/qtile/dakataca/keys.py << EOF
# Atajos de teclado.
# 📂 ~/.config/dakataca/keys.py

# In ~/.config/qtile/config.py
# from dakataca.keys import get_keys
# keys = get_keys(mod) + [

from libqtile.config import Key
from libqtile.lazy import lazy

mod = "mod4"
# Configuraciones de atajos de teclado del usuario dakataca.
def get_keys(mod):
    return [
        # at https://docs.qtile.org/en/latest/manual/config/lazy.html
        # Abrir enlace del portapapeles en el reproductor mpv con F12, requiere xclip.
        Key([], "F12",lazy.spawn('bash -c "mpv \$(xclip -o)""')),
        Key(["shift"], "F12",lazy.spawn('bash -c "mpv --no-video \$(xclip -o)""')),
        # Shorcuts de aplicaciones favoritas.
        Key([mod, "shift"], "w",lazy.spawn("chromium"),desc="Iniciar chromium"),
        Key([mod, "shift"], "t", lazy.spawn("telegram-desktop"),desc="Iniciar Telegram"),
        Key([mod, "shift"], "c", lazy.spawn("code-oss"),desc="Iniciar code"),
        #Key([mod, "shift"], "f", lazy.spawn("thunar"),desc="Iniciar Thunar"),
        Key([mod, "shift"], "f", lazy.spawn("pcmanfm"),desc="Iniciar PcmanFm"),
        Key([mod, "shift"], "y", lazy.spawn("typora"),desc="Iniciar Typora"),
        # 🌐 wiki: https://docs.qtile.org/en/stable/manual/config/keys.html#special-keys
        # 🌐 wiki: https://wiki.archlinux.org/title/PulseAudio#Keyboard_volume_control
        # Volume:
        Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
        Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -2%")),
        Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +2%")),
    ]
EOF
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

#### groups

```sh
cat > ~/.config/qtile/dakataca/groups.py << EOF
# Grupos.
# 📂 ~/.config/dakataca/group.py

## In ~/.config/qtile/config.py:
# from dakataca.groups import get_groups
# groups = get_groups()

from libqtile.config import Group, Match

# 🔗 Fuente externa: https://docs.qtile.org/en/stable/manual/config/groups.html#groups
# 🌐 wiki: https://wiki.archlinux.org/title/Qtile#Group_Rules
def get_groups():
    return [
        Group(name=str(idx), **group)
        for idx, group in enumerate(
            [
                #{"label": "\uf489", "matches": [Match(func=is_terminal)]},
                {"label": "\uf489", "matches": [Match(wm_class='Alacritty')]},
                {"label": "\uf268", "matches": [Match(role="browser")]},
                #{"label": "\uf1d8", "matches": [Match(wm_class='telegram-desktop')]},
                {"label": "\ue217", "matches": [Match(wm_class='telegram-desktop')]},
                #{"label": "['\uf4d4', '\u77a3']", "matches": [Match(wm_class=['pcmanfm', 'ristretto'])]},
                {"label": "\uf4d4", "matches": [Match(wm_class=['pcmanfm', 'ristretto'])]},
                {"label": "\ue77e", "matches": [Match(wm_class='typora')]},
                #{"label": "\ue647", "matches": [Match(wm_class='code-oss')]},
                {"label": "\uf6cc", "matches": [Match(wm_class='code-oss')]},
                {"label": "\uf338", "matches": [Match(wm_class='gimp')]},
            ],
            start=1,
        )
    ]
EOF
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

widgets

```sh
cat > ~/.config/qtile/dakataca/widgets.py << EOF
# 📂 ~/.config/dakataca/widgets.py
## In ~/.config/qtile/dakataca/screen.py:
# Screen(
#     wallpaper = wallpaper,
#     wallpaper_mode = wallpaper_mode,
#     top=bar.Bar(
#         [
#--->        *get_widgets(main),
#         ],
#         24,
#         background = '#1b041c'
#         ),
# )
## In ~/.config/qtile/dakataca/config.py:
# from dakataca.widgets import get_default_widgets
#widget_defaults = extension_defaults = get_default_widgets()
##widget_defaults = get_default_widgets()

from libqtile import bar, widget
from libqtile.config import Match
from libqtile.lazy import lazy

# Configuraciones predeterminadas de los widgets.
def get_default_widgets():
    return dict(
        font="opensans-bold",
        fontsize=13,
        padding=0,
    )

# Widgets del centro.
def center_widgets():
    return [
                widget.Spacer(length=bar.STRETCH),
                widget.Clock(
                    format="%Y-%m-%d %a\n%H:%M ",
                    fmt='<span font_size="92%" line_height=".73%">{}</span>',
                    #mouse_callbacks={"Button1" : lazy.spawn("alacritty --hold -t 'terminal_floating' -e cal")},
                    mouse_callbacks={
                        'Button1': lazy.spawn('gsimplecal'),
                        'Button2': lazy.spawn('killall -q gsimplecal'),
                        'Button3': lazy.spawn('killall -q gsimplecal')
                    },
                ),
                widget.Spacer(length=bar.STRETCH),
            ]

# 🔗 Fuente externa:  https://docs.qtile.org/en/stable/manual/ref/widgets.html#groupbox
# Widgets a la izquierda.
def left_widgets():
    return [
        widget.GroupBox(
            hide_unused = True,                        # Ocultar grupos que no tienen ventanas.
            highlight_method='line',                   # Método de resaltado ('borde', 'bloque', 'texto' o 'línea').
            fontshadow = '#17181a',                    # Color de sombra de fuente.
            #inactive = '#ffffff',
            inactive = '#b496b5',
            fontsize=23,
            #🌟 💻🖥️   Si usa más de una pantalla.
            # 💻 Pantalla enfocada.
            highlight_color = ['#590d5c', '#e3346e'],  # Colores de degradado cuando se usa "línea".
            this_current_screen_border = '#f886fc',    # Color de línea del grupo visible en la pantalla enfocada.
            other_screen_border = '#733e75',           # Color de línea del grupo no visible en la pantalla enfocada.
            # 🖥️   Pantalla no enfocada.
            this_screen_border = '#f886fc',            # Color de línea del grupo visible en la pantalla no enfocada.
            other_current_screen_border = '#733e75'    # Color de línea del grupo no visible en la pantalla no enfocada.
        ),
        widget.Prompt()
    ]

# Widgets a la derecha.
def right_widgets():
    return [
 #       widget.Cmus(
 #           #play_icon = '\U0000F025',
 #           #play_icon = '\uF025',
 #           play_icon = '\ue077',       # Requiere paquete ttf-fonts-awesome.
 #           fontshadow = '#17181a',
 #           #fmt = '<i><b>{}</b></i>',
 #           fmt = '<i>{}</i>',
 #           format=' {play_icon} {title} - {artist}',
 #           max_chars=46,
 #           play_color='#f886fc',
 #           noplay_color='#733e75',
 #       ),
        widget.Sep(padding = 10),
        widget.Spacer(length=1),
        widget.Volume(
            emoji = True,
            emoji_list = ['\uf466 ', '\uf026 ', '\uf027', '\uf028'],
            fmt='<span font_size="184%" foreground="#f886fc">{}</span>',
            get_volume_command = 'pamixer --get-volume-human',
            width = 17
        ),
        widget.Volume(
            format = '{volume}',
            get_volume_command = 'pamixer --get-volume-human',
            fmt='<sup><span font_weight="bold"><sup>{}</sup></span></sup>',
        ),
        widget.Spacer(length=2),
        widget.Sep(padding = 10),
        widget.Wlan(
            interface = 'wlp2s0',
            disconnected_message = '\uf127  \ueab8',
            #disconnected_message = '\uf44f \uf127',
            #format = '{essid} {percent:2.0%}'
            #format = '\ue619 {percent:2.0%}',
            format = ' {percent:2.0%}',
            fmt='<span font_size="225%" foreground="#f886fc">\uf1eb</span><sup><span font_weight="bold" font_size="115%">{}</span></sup>',
            mouse_callbacks = {
                'Button1': lazy.spawn('iwgtk'),
                'Button2': lazy.spawn('killall -q iwgtk'),
                'Button3': lazy.spawn('killall -q iwgtk')
            },
        ),
        widget.Spacer(length=7),
        widget.Sep(padding = 10),
        widget.Battery(
#            full_char = '\uf240',
            show_short_text = False,
            full_char = '\ue55b',
#            discharge_char = '\ue6a9',
            discharge_char = '\uee9e',
            unknown_char = '\uf240',
            #format = '{char} {percent:2.0%}',
#            charge_char = '\uf0e7',
#            charge_char = '\ue55b',
            charge_char = '\ue00a',
#            not_charging_char = '\uf5e7',
#            not_charging_char = '\ue55d',
            not_charging_char = '\ued82',
            format = '{char}',
            fmt='<span font_size="97%" foreground="#f886fc">{}</span>',
            update_interval = 5,
            mouse_callbacks = {
                'Button1': lazy.spawn('sh -c "acpi -i | yad --text-info --geometry=750x105"'),
                'Button2': lazy.spawn('killall -q yad'),
                'Button3': lazy.spawn('killall -q yad'),
            },
            width = 17
        ),
        widget.Battery(
            format = '{percent:2.0%}',
            fmt='<sup><span font_weight="bold" font_size="100%"><sup>{}</sup></span></sup>',
            update_interval = 5,
            mouse_callbacks = {
                'Button1': lazy.spawn('sh -c "acpi -i | yad --text-info --geometry=750x105"'),
                'Button2': lazy.spawn('killall -q yad'),
                'Button3': lazy.spawn('killall -q yad'),
            },
        ),
        widget.Spacer(length=3),
            ]

def get_widgets(main):
    if main:
        return [*left_widgets(), *center_widgets(), widget.Systray(), *right_widgets()]
    else:
        return [*left_widgets(), *center_widgets(), *right_widgets()]
EOF
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```



#### screen

```sh
cat > ~/.config/qtile/dakataca/screen.py << EOF
# Pantalla.
# 📂 ~/.config/dakataca/screen.py

## In ~/.config/qtile/config.py:
# from dakataca.screen import get_screen
# screens = [  
#-->  get_screen('~/.config/qtile/background.jpg', 'fill', True),   # Main screen.
#-->  get_screen('~/.config/qtile/background.jpg', 'fill', False),  # Second screen.
# ]

from libqtile import bar
from libqtile.config import Screen
from .widgets import get_widgets

# 🔗 Fuente externa: https://wall.alphacoders.com/wallpaper.php?i=1028306&w=1366&h=768&type=stretch
def get_screen(wallpaper, wallpaper_mode, main):
    return Screen(
        wallpaper = wallpaper,
        wallpaper_mode = wallpaper_mode,
        top=bar.Bar(
            [
                *get_widgets(main),
            ],
            24,
            background = '#1b041c'
            ),
        )
EOF
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

#### layouts

```sh
cat > ~/.config/qtile/dakataca/layouts.py << EOF
# Diseños.
# 📂 ~/.config/dakataca/layouts.py

## In ~/.config/qtile/config.py:
# from dakataca.layouts import get_layouts
#layouts = get_layouts()

from libqtile import layout

def get_layouts():
    layout_theme = {
        "border_width": 2,
        "margin": 2,
        "border_focus": "#f82aac",
        "border_normal": "#733e75",
    }
    return [
        #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
        #layout.Max(),
        layout.Columns(**layout_theme),
        layout.Max(**layout_theme),
        layout.RatioTile(**layout_theme),
        layout.MonadTall(**layout_theme),
        layout.Stack(stacks=2, **layout_theme),
        layout.Tile(shift_windows=True, **layout_theme),

        # Try more layouts by unleashing below layouts.
        # layout.Stack(num_stacks=2),
        # layout.Bsp(),
        # layout.Matrix(),
        # layout.MonadTall(),
        # layout.MonadWide(),
        # layout.RatioTile(),
        # layout.Tile(),
        # layout.TreeTab(),
        # layout.VerticalTile(),
        # layout.Zoomy(),
    ]
EOF
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```



### Fichero de configuración Qtile

```python
nvim .config/qtile/config.py
-------------------------------------------------------------------
# 🌟🌟🌟🐍  Editar el fichero de configuración del administrador de ventanas Qtile.
# 🌟 Imports from dakataca user
from dakataca.groups import get_groups
from dakataca.widgets import get_default_widgets
from dakataca.screen import get_screen
from dakataca.keys import get_keys
from dakataca.layouts import get_layouts

# 🌟⌨️ Shortcuts
keys =  get_keys(mod) + [
#...
# 🌟 Groups config
# ------------------------------------------

#groups = [Group(i) for i in "123456789"]
# 🔗 Fuente externa: https://docs.qtile.org/en/stable/manual/config/groups.html#groups
# 🌐 wiki: https://wiki.archlinux.org/title/Qtile#Group_Rules
groups = get_groups()
# 🌟 Layouts config
# ------------------------------------------

layouts = get_layouts()

widget_defaults = extension_defaults = get_default_widgets()
#widget_defaults = get_default_widgets()

# 🌟  Screen config
# -----------------------------------------
# Widgets:

screens = [
    # Main screen.
    get_screen('~/.config/qtile/background.png', 'fill', True),
    # Second screen.
    get_screen('~/.config/qtile/background.png', 'fill', False),
]

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(wm_type="utility"),
        Match(wm_type="notification"),
        Match(wm_type="toolbar"),
        Match(wm_type="splash"),
        Match(wm_type="dialog"),
        Match(wm_class="file_progress"),
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="yad"),
        Match(title="terminal_floating"),
        Match(title="iwgtk"),
    ]
)

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

Testear fichero de configuración

```sh
python -m py_compile ~/.config/qtile/config.py
```





## Alacritty

Crear directorios:

```sh
mkdir -p ~/.config/alacritty/themes/
```

### Blueberries theme

```yaml
cat > ~/.config/alacritty/themes/blueberries.toml << EOF
# 🌟🌟🌟 Blueberries Theme 🌟🌟🌟

[colors.bright]
black = "#6b6b6b"
blue = "#7fa8c9"
cyan = "#56f5ea"
green = "#a8d5a2"
magenta = "#a7a7a7"
red = "#e74c3c"
white = "#f7f2e6"
yellow = "#f0b639"

[colors.cursor]
cursor = "#ffe1e1"
text = "#2b1315"

[colors.normal]
black = "#0d0c08"
blue = "#66839a"
cyan = "#009090"
green = "#6ab091"
magenta = "#9e3960"
red = "#cc1641"
white = "#fce9b8"
yellow = "#fc7926"

[colors.primary]
background = "#0d080d"
foreground = "#ffffff"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

### Fuentes

Puede elegir entre las fuentes [recomendadas disponibles](https://www.codingfont.com/) y sus paquetes, para solo aplicar los de su preferencia.

Listar fuentes:

```sh
fc-list : family style | grep -E 'Hack|Fira Code|Source Code Pro|Inconsolata'
```

#### Falta alguna fuente?

Listar todos los paquetes de fuentes:

```sh
pacman -Ss '^ttf-*'
```

Instalar paquetes de fuentes recomendadas:

```sh
sudo pacman -S ttf-{inconsolata,dejavu,hack,roboto,liberation,fira-{sans,code}} --needed --noconfirm
```

### Fichero de configuración centralizado

```yaml
cat > ~/.config/alacritty/alacritty.toml << EOF
# 🌟🌟🌟🐚 Fichero de configuración centralizado a nivel de usuario, del emulador de terminal Alacritty:
# 📂 ~/.config/alacritty/alacritty.toml
# 🌐 wiki: https://wiki.archlinux.org/title/Alacritty#Configuration
# 🌐 https://alacritty.org/config-alacritty.html

[general]
# 🌟⚙️  Establecer configuraciones centralizadas:
import = [
"/home/dakataca/.config/alacritty/themes/blueberries.toml",
]

[cursor]
style = "Block"
unfocused_hollow = true

[font]
size = 15

[font.normal]
family = "Inconsolata"

[window]
opacity = 0.95

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Kitty

### Configuración general

```sh
cat > ~/.config/kitty/general.conf << EOF
# Opacidad
background_opacity 0.93
EOF
```

### Tema

```sh
cat > ~/.config/kitty/theme.conf << EOF
# Blueberries Theme in Kitty

# Primary colors
background #0D080D
foreground #FFFFFF

# Cursor
cursor #FFE1E1
cursor_text_color #2B1315

# Normal colors
color0 #2B1315
color1 #CC1641
color2 #6AB091
color3 #F0B639
color4 #66839A
color5 #9E3960
color6 #009090
color7 #D5E9E0

# Bright colors
color8 #DBE9B7
color9 #CC1641
color10 #6AB091
color11 #F0B639
color12 #66839A
color13 #9E3960
color14 #009090
color15 #F0B639
EOF
```

### Fuente

```sh
cat > ~/.config/kitty/font.conf << EOF
font_family      Inconsolata-Regular
bold_font        Inconsolata-Bold
italic_font      Inconsolata-Italic
bold_italic_font Inconsolata-Bold-Italic
font_size 14.0
EOF
```

### Configuración centralizada

```sh
cat > ~/.config/kitty/kitty.conf << EOF
include general.conf
include theme.conf
include font.conf
EOF
```



## Tmux

```sh
mkdir -p ~/.config/tmux/
cat > ~/.config/tmux/tmux.conf << EOF
# 🌟🌟🌟 Fichero de configuración a nivel de usuario, del multiplexor de terminal tmux.
# 📂 ~/.config/tmux/tmux.conf 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Tmux#Configuration

# ⌨️ Cambiar enlace de prefijo Ctrl+b, a Ctrl+a.
unbind C-b
set -g prefix C-a
bind C-a send-prefix

# 🖱️ Habilitar el uso del mouse.
set -g mouse on

# 💻 Habilitar terminal con 256 colores.
set -g default-terminal "tmux-256color"


# 📂 Configuración de yazi en tmux.
set -g allow-passthrough on
set -ga update-environment TERM
set -ga update-environment TERM_PROGRAM

# 🌈 Establecer profundidad del color de alacritty en 24 bit.
set -ga terminal-overrides ", alacritty: Tc"

# 🖥️ Iniciar en 1 en ves de 0, la numeración de sesiones, ventanas y paneles.
set -g base-index 1

# 🖥️⌨️ Reorganizar ventanas con Shift+Left/Right.
bind-key -n S-Left swap-window -t -1 \; select-window -t -1
bind-key -n S-Right swap-window -t +1 \; select-window -t +1

# 📝 Recargar fichero de configuración manualmente.
bind r source-file ${HOME}/.tmux.conf \; display-message "source-file reloaded"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Chromium

```sh
cat > ~/.config/chromium-flags.conf << EOF
# 🌟🌟🌟 Fichero de configuración a nivel de usuario del navegador chromium habilitando VULKAN y VAAPI 🌟🌟🌟
# 📂 ~/.config/chromium-flags.conf 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Chromium

# 🌟🌟 Apariencia ------------------------------------------------------------------------
# 🌟Forzar modo nocturno:
--force-dark-mode
--enable-features=WebUIDarkMode

# 🌟 Habilitar VULKAN y VAAPI:
# 🌐 wiki: https://wiki.archlinux.org/title/Chromium#Hardware_video_acceleration
--enable-features=VaapiVideoDecoderLinuxGL,VaapiIgnoreDriverChecks,Vulkan,DefaultANGLEVulkan,VulkanFromANGLE

# 🌟🌟 CPU aceleración --------------------------------------------------------------------
# 🌟 Deshabilitar aceleración por hardware:
#--disable-gpu

# 🌟🌟 Wayland support --------------------------------------------------------------------
#--ozone-platform-hint=auto

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Picom

```sh
install -D /etc/xdg/picom.conf ~/.config/picom/picom.conf
```

```sh
nvim ~/.config/picom/picom.conf
-----------------------------------------------------------------
# 🌟🌟🌟 Edite la configuración del compositor picom a nivel de usuario 🌟🌟🌟
# Comentar todas las configuraciones de sombras (shadow).

fade-in-step = 0.07;       # Duración del desvanecimiento de entrada.
fade-out-step = 0.07;      # Duración del desvanecimiento de salida.

backend = "glx"            # Usar OpenGL.

# Comentar xrender
#backend = "xrender";

wintypes:
{
  ## Ventanas emergentes de información.
  # Desactivar sombras (shadow = false).
  tooltip = { fade = true; shadow = false; opacity = 0.93; focus = true; full-shadow = false; };


  ## Menús emergentes y desplegables.
  popup_menu = { opacity = 0.96; }
  dropdown_menu = { opacity = 0.96; }
};
```



## Redshift

```sh
mkdir ~/.config/redshift/
cat > ~/.config/redshift/redshift.conf << EOF
;; 🌟🌟🌟💡 Fichero de configuración de la herramienta de ajuste de color y temperatura de pantalla redshift 🌟🌟🌟
;; 📂 ~/.config/redshift/redshift.conf 👤
;; 🌐 config-file: https://github.com/jonls/redshift/blob/master/redshift.conf.sample
;; 🌐 wiki: https://wiki.archlinux.org/title/redshift

; 🌟🌟 Configuraciones globales para redshift 🌟🌟
[redshift]
; 🌟🌡️🖥️ Establezca las temperaturas de pantalla diurnas y nocturnas.
temp-day=3500
temp-night=2900
;temp-night=3500

; 🌟 Deshabilite el desvanecimiento suave entre temperaturas cuando Redshift se inicia y se detiene.
; 0 provocará un cambio inmediato entre las temperaturas de la pantalla.
; 1 aplicará gradualmente la nueva temperatura de la pantalla durante un par de segundos.
fade=1

; 🌟☀️ Umbrales de elevación solar.
; De forma predeterminada, Redshift utilizará la elevación actual del sol para determinar
; ya sea de día, de noche o en transición (amanecer/atardecer). Cuando el sol es
; por encima de los grados especificados con elevación-alta se considera diurno y
; por debajo de cota-baja se considera noche.
;elevation-high=3
;elevation-low=-6

; 🌅🌆Intervalos personalizados de amanecer/anochecer.
; En lugar de utilizar la elevación solar, los intervalos de tiempo del amanecer y el anochecer.
; ⏰ se puede especificar manualmente. Los tiempos deben especificarse como HH:MM en formato 24 horas.
;dawn-time=06:00-07:45
;dusk-time=18:35-20:15

; 💡 Establece el brillo de la pantalla. El valor predeterminado es 1.0.
;brightness=0.9
; ☀️🌙 También es posible utilizar diferentes configuraciones para el día y la noche.
; desde la versión 1.8.
brightness-day=1.0
brightness-night=0.75
; 🌈 Configure la gamma de la pantalla (para todos los colores, o cada canal de color
; individualmente)
;gamma=0.9
;gamma=0.8:0.7:0.8
; ☀️🌙 Esto también se puede configurar individualmente para el día y la noche desde la versión 1.10.
;gamma-day=0.8:0.7:0.8
gamma-day=1.0
gamma-night=0.9
;gamma-night=1.0:0.95:1.0

; 🌍📍 Establezca el proveedor de ubicación: 'geoclue2', 'manual'
; escriba 'redshift -l list' para ver los valores posibles.
; La configuración del proveedor de ubicación se encuentra en una sección diferente.
location-provider=manual

; ⚙️ Configure el método de ajuste: 'randr', 'vidmode'
; escriba 'redshift -m list' para ver todos los valores posibles.
; 'randr' es el método preferido, 'vidmode' es una API más antigua.
; pero funciona en algunos casos cuando 'randr' no lo hace.
; La configuración del método de ajuste se encuentra en una sección diferente.
adjustment-method=randr

; ⚙️🌍📍 Configuración del proveedor de ubicación:
; escriba 'redshift -l PROVIDER:help' para ver la configuración.
; ej: 'redshift -l manual:help'
; Tenga en cuenta que las longitudes al oeste de Greenwich (por ejemplo, las Américas) son números negativos.
[manual]
; Corregimiento San Cristobal de Caracol, Municipio de Toluviejo, Departamento de Sucre, País Colombia.
lat=9.419068130974736
lon=-75.39748748324705

; ⚙️ Configuración del método de ajuste:
; escriba 'redshift -m METHOD:help' para ver la configuración.
; ej: 'redshift -m randr:help'
; En este ejemplo, randr está configurado para ajustar solo la pantalla 0.
; Tenga en cuenta que la numeración comienza desde 0, por lo que ésta es en realidad la primera pantalla.
; Si no se especifica esta opción, Redshift intentará ajustar todas las pantallas.
[randr]
;screen=0

; 👤 Autor: https://t.me/dakataca 💻 🐬 ;
EOF
```



```sh
mkdir -p ~/.config/redshift/hooks
cat <<'EOF' > ~/.config/redshift/hooks/brightness.sh
#!/bin/bash
# 🌟🌟🌟🐚 Bash script to automatically alter screen brightness using the light tool 🌟🌟🌟
# 🌟🌟🌟🐚 [es]: Script Bash para alterar automáticamente el brillo de la pantalla usando la herramienta light 🌟🌟🌟
# 📂 ~/.config/redshift/hooks/brightness.sh 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Redshift#Use_real_screen_brightness
# 🔗 External Source: https://stackoverflow.com/posts/22698077/timeline
# 💡 Set brightness via light when redshift status changes.
# 💡[es]: Ajusta el brillo a través de la herramienta light, cuando cambie el estado de redshift.

# 🌟 Adjust brightness by setting the light tool to a value within the range of 1 to 100.
brightness_night=35           # Brillo nocturno.
brightness_transition=65      # Brillo transitorio.
brightness_day=99             # Brillo diurno.

# 🌟 Adjust the minimum brightness of the light tool at each time of the day.
minimum_brightness_night=2            # Brillo mínimo nocturno.
minimum_brightness_transition=4       # Brillo mínimo transitorio.
minimum_brightness_day=6              # Brillo mínimo diurno.

# 🌟 Set current and minimum brightness.
set_brightness(){
   declare -r current_brightness=$1, minimum_brightness=$2
   light -S $current_brightness
   light -N $minimum_brightness
}

# 🌟🌟 Set brightness according to change period.
if [ "$1" = period-changed ]; then
        case $3 in
                night)
                        set_brightness $brightness_night $minimum_brightness_night
                        ;;
                transition)
                        set_brightness $brightness_transition $minimum_brightness_transition
                        ;;
                daytime)
                        set_brightness $brightness_day $minimum_brightness_day
                        ;;
        esac
fi

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Permisos de ejecución al usuario:

```sh
chmod u+x ~/.config/redshift/hooks/brightness.sh
```

Si usa **administrador de sesiones** como **`lightdm`**o **`sddm`**:

```sh
systemctl --user enable redshift.service
```



## Dunst

```sh
mkdir -p ~/.config/dunst
cp /etc/dunst/dunstrc ~/.config/dunst/
```

```sh
nvim ~/.config/dunst/dunstrc
-------------------------------------------------
# 🌟🌟🌟 Configuración a nivel de usuario del servidor de notificaciones dunst 🌟🌟🌟
# 📂 ~/.config/dunst/dunstrc
# 🌐 wiki: https://wiki.archlinux.org/title/Dunst#Installation

    font = Awesome 8

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```



## mpd + ncmpcpp + dunst

Directorio de configuración del reproductor TUI `ncmpcpp`:

```sh
mkdir -p ~/.{local/state/mpd/,config/{mpd/playlists,ncmpcpp/previews}/}
touch ~/.{local/state/mpd/state,config/mpd/sticker.sql}
```

Ficheros de configuración por defecto:

```sh
#cp /usr/share/doc/mpd/mpdconf.example ~/.config/mpd/mpd.conf
#cp /usr/share/doc/ncmpcpp/config ~/.config/ncmpcpp/
```

Servidor de audio:

```sh
cat <<'EOF' > ~/.config/mpd/mpd.conf
# 🌟🌟🌟 Edite la configuración del servidor de audio mpd a nivel de usuario 🌟🌟🌟
# 📂 ~/.config/mpd/mpd.conf 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Music_Player_Daemon#Audio_configuration

# Files status.
music_directory    "~/Music"
playlist_directory "~/.config/mpd/playlists"
#pid_file           "~/.config/mpd/pid"
state_file         "~/.local/state/mpd/state"
sticker_file       "~/.config/mpd/sticker.sql"

# Servidor de audio Pipewire*, PulseAudio o simplemente ALSA?
audio_output {
        ## PipeWire:
        type            "pipewire"
        name            "PipeWire Sound Server"
        ## PulseAudio:
#        type            "pulse"
#        name            "pulse audio"
        ## Alsa:
#        type          "alsa"
#        name          "ALSA sound card"
        # Optional
        #device        "iec958:CARD=Intel,DEV=0"
        #mixer_control "PCM"
}

# 🌟🌟🌟 Edite la configuración del servidor de audio mpd a nivel de usuario 🌟🌟🌟
# 📂 ~/.config/mpd/mpd.conf 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Ncmpcpp#Enabling_visualization
audio_output {
    type                    "fifo"
    name                    "my_fifo"
    path                    "/tmp/mpd.fifo"
    format                  "44100:16:2"
}

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



```sh
cat <<'EOF' >  ~/.config/ncmpcpp/config
# 🌟🌟🌟 Edite la configuración a nivel de usuario de ncmpcpp, un cliente del servidor de audio mpd 🌟🌟🌟
# 📂 ~/.config/ncmpcpp/config 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Ncmpcpp

# Music directory. [es] Directorio de la Música.
mpd_music_dir = ~/Music/

# 🌐 wiki: https://wiki.archlinux.org/title/Ncmpcpp#Enabling_visualization
visualizer_data_source = "/tmp/mpd.fifo"
visualizer_output_name = "my_fifo"
visualizer_in_stereo = "yes"
visualizer_type = "spectrum"
visualizer_look = "+|"


# Unicode characters "\ue224\uea71".
progressbar_look = ""

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



Notificaciones de canción incluyendo carátula usando `duns` y `dunstify`.

```sh
mkdir -p ~/.local/bin/
cat <<'EOF' > ~/.local/bin/songinfo
#!/bin/bash
# 📂 ~/.local/bin/songinfo 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Ncmpcpp#With_album_art

music_dir="$HOME/Music/"
previewdir="${XDG_CONFIG_HOME:-$HOME/.config}/ncmpcpp/previews"
filename="$(mpc --format "$music_dir/%file%" current)"
previewname="$previewdir/$(mpc --format %album% current | base64 -w 0).jpg"

mkdir -p "$previewdir"
[ -e "$previewname" ] || ffmpeg -y -i "$filename" -an -vcodec copy "$previewname" > /dev/null 2>&1

dunstify -r 27072 "Now Playing" "$(mpc --format '%title% \n%artist% - %album%' current)" -i "$previewname"
EOF
chmod u+x ~/.local/bin/songinfo
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

```properties
nvim ~/.config/ncmpcpp/config
---------------------------------------------------------------
# 🌟🌟🌟 Edite la configuración a nivel de usuario de ncmpcpp, un cliente del servidor de audio mpd 🌟🌟🌟
# 📂 ~/.config/ncmpcpp/config 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Ncmpcpp#With_album_art
execute_on_song_change = $HOME/.local/bin/songinfo
# Idealmente podría agregar a PATH la ruta ~/.local/bin/ y solo establecer:
#execute_on_song_change = songinfo
```

```sh
mpc update
systemctl --user enable mpd.service
```

