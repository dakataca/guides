

# FFMPEG

En ésta guía estaremos abarcando el uso del comando `ffmpeg` para:

- Captura y combinación de flujos de audio.
- Captura de flujos de video de la pantalla.
- Captura y combinación de flujos de video y audio.

En los siguientes ejemplos se usa el servidor de audio `Pipewire` para administrar `alsa` y `pulse`.


[toc]



# Fuentes de audio

------



## Listar fuentes de audio

Listar las fuentes de entrada y salida de audio disponibles en el sistema, filtrando aquellas que correspondan a dispositivos de audio "alsa" (Advanced Linux Sound Architecture), es decir, líneas específicas que contienen la cadena "alsa.output" o "alsa.input":

```sh
pactl list short sources | grep -E "alsa.(output|input)"
.......................................................
46 alsa_output.pci-0000_00_1b.0.analog-stereo.monitor PipeWire s32le 2ch 48000Hz SUSPENDED
47 alsa_input.pci-0000_00_1b.0.analog-stereo          PipeWire s32le 2ch 48000Hz SUSPENDED
```

> `46`: Monitor o audio que reproduce el sistema.
> `47`: Micrófono de la tarjeta de sonido interna del sistema.



## Capturar fuentes de audio

### Micrófono

Las siguientes son formas distintas de capturar el audio de la entrada por defecto, es decir, el micrófono, y guardarlo en un archivo llamado `microfono.m4a`.

**Usando el nombre por defecto:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i default -acodec aac microfono.m4a
```

> - `-f pulse`: Se utilizará el servidor de sonido PulseAudio para capturar el audio.
> - `-thread_queue_size 1024`: Establece el tamaño máximo de la cola de hilos en 1024. Esto puede mejorar el rendimiento del proceso de grabación y evitar errores relacionados con la cola de hilos.
> - `-i default`: indica que se utilizará la fuente de audio predeterminada como entrada de audio.
> - `-acodec aac`: Especifica el códec de audio a utilizar para la salida de audio. En este caso, se utilizará el códec AAC, que es un formato de audio de alta calidad y eficiencia de compresión.
> - `microfono.m4a`: Establece el nombre del archivo de salida como `microfono.m4a`. Esto significa que el archivo grabado se guardará en el directorio actual con el nombre `microfono.m4a` y el formato de archivo `.m4a`.

**Usando nombre:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 \
-i alsa_input.pci-0000_00_1b.0.analog-stereo -acodec aac microfono.m4a
```

> `-i alsa_input.pci-0000_00_1b.0.analog-stereo`: Indica la entrada de audio predeterminada del sistema, es decir, el micrófono de la tarjeta de sonido interna del sistema.

**Usando número:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i 47 -acodec aac microfono.m4a
```

> `-i 47`: Especifica la fuente de audio número 47 como flujo de audio, es decir, la entrada de audio del micrófono de la tarjeta de sonido interna del sistema.



### Monitor

Las siguientes son formas distintas de capturar el audio que reproduce el dispositivo de salida predeterminado (por ejemplo, los altavoces). El monitor de audio es una fuente de audio especial que captura todo el audio que se reproduce en el sistema.

**Usando nombre:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 \
-i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor monitor.m4a
```

> `-i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor`: Especifica la fuente de audio del monitor de la tarjeta de sonido interna del sistema.

**Usando número:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i 46 monitor.m4a
```

> `-i 46`: Especifica la fuente de audio número 46 como flujo de audio, es decir, la salida de audio del monitor de la tarjeta de sonido interna del sistema.



### Micrófono y monitor



#### En un solo archivo

Capturar audio desde dos fuentes diferentes (por ejemplo, `micrófono` y `monitor`) en un solo archivo:

**Usando nombres:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i default \
-f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor \
-filter_complex amix=inputs=2 -acodec aac microfono+monitor.m4a
```

Si presenta retraso puede probar añadiendo un delay como filtro en el número que más le convenga, en mi caso `120`:

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i default -f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -filter_complex "[1]adelay=120ms:all=1[audio1];[0:a][audio1]amix=inputs=2" -acodec aac microfono+monitor.m4a
```

> - Aplique un retraso de 120 milisegundos al primer flujo `[1]adelay=120ms` , en este caso el audio del monitor interno del sistema, `:all=1[audio1]` guarde el resultado en `audio1`, luego `;[0:a][audio1]amix=inputs=2` mezcle el resultado del flujo anterior con el flujo número `0:a`, perteneciente al audio del micrófono.

**Usando números:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i 47 \
-f pulse -thread_queue_size 1024 -i 46 \
-filter_complex amix=inputs=2 -acodec aac microfono+monitor.m4a
```

> `-filter_complex amix=inputs=2`: Indica que se debe utilizar el filtro "amix" de FFmpeg para mezclar los dos flujos de audio de entrada en un solo flujo de salida. El parámetro "inputs=2" indica que se deben mezclar dos flujos de entrada, en éste caso el flujo de entrada del micrófono y el de salida de audio del monitor del sistema en el archivo de salida `microfono+monitor.m4a`.



#### En archivos separados

Capturar audio desde dos fuentes diferentes, (por ejemplo, `micrófono` y `monitor`) en ficheros separados.

**Usando nombres:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i default -acodec aac -map 0:a microfono.m4a \
-f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor \
-acodec aac -map 1:a monitor.m4a
```

> `-map 0:a`: Indica que se utilizará el primer flujo de audio, es decir, `-i default` (micrófono), como fuente de audio para el archivo de salida `microfono.m4a`.
> `-map 1:a`: Indica que se utilizará el segundo flujo de audio, es decir, `-i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor` (monitor),  como fuente de audio para el archivo de salida `monitor.m4a`.

**Usando números:**

```sh
ffmpeg -f pulse -thread_queue_size 1024 -i 47 -acodec aac -map 0:a microfono.m4a \
-f pulse -thread_queue_size 1024 -i 46 -acodec aac -map 1:a monitor.m4a
```

> `-map 0:a`: Indica que se utilizará el primer flujo de audio, es decir, `-i 47` (micrófono), como fuente de audio para el archivo de salida `microfono.m4a`.
> `-map 1:a`: Indica que se utilizará el segundo flujo de audio, es decir, `-i 46` (monitor),  como fuente de audio para el archivo de salida `monitor.m4a`.



# Fuente de video X11 (Xorg)

------



## Capturar fuente de video

Capturar video de la pantalla `:0` usando el protocolo X11 en el archivo `video.mp4`.

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 video.mp4
```

> `-vaapi_device /dev/dri/renderD128`: Usa el dispositivo VAAPI especificado en `/dev/dri/renderD128` para acelerar la codificación de video.
> `-f x11grab `: Especifica que la entrada de video que se grabará utilizará el protocolo X11.
> `-thread_queue_size 2048`: Establece el tamaño máximo de la cola de hilos en 2048. Esto puede mejorar el rendimiento del proceso de grabación y evitar errores relacionados con la cola de hilos.
> `-video_size 1366x768 -i :0`: Especifica que la captura de video tendrá un tamaño de 1366x768 píxeles sobre la pantalla `:0`.
> `-vf 'format=nv12,hwupload'`: Establece el filtro que convierte el formato de pixel a NV12 para mejorar compatibilidad, y para aprovechar la capacidad de aceleración de hardware de la GPU, respectivamente.
> `-c:v h264_vaapi -qp 17`: Establece el códec de video que se utilizará (`h264_vaapi`) y la calidad de video (`qp 17`).



## Capturar fuentes de video y audio



### Audio del micrófono independiente

Capturar:

1. Audio del *micrófono* en un archivo independiente  llamado `microfono.m4a`.
2. Audio del *monitor* de la tarjeta de sonido interna del sistema combinado con el video en un archivo llamado `video+monitor.mp4`.

**Usando nombres:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i default -map 0:a microfono.m4a -f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 1:a -map 2:v  video+monitor.mp4
```

> - `-f pulse -thread_queue_size 1024 -i default -map 0:a microfono.m4a`: Captura el primer flujo de audio desde dispositivo de entrada predeterminado (por ejemplo, un micrófono) a través del servidor de sonido PulseAudio y lo guarda en el archivo `microfono.m4a`.
> - `-f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor`: Captura el audio que se está reproduciendo actualmente en el dispositivo de salida `alsa_output.pci-0000_00_1b.0.analog-stereo.monitor` (por ejemplo, los altavoces) a través de PulseAudio.
> - `-map 1:a -map 2:v video+monitor.mp4`: Especifica los flujos de audio y video que se combinarán para crear el archivo de salida `video+monitor.mp4`, en éste caso, se combinarán el segundo flujo de audio, es decir, el monitor de la tarjeta interna del sistema, con el tercer flujo, es decir, el video.

**Usando números:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i 47 -map 0:a microfono.m4a -f pulse -thread_queue_size 1024 -i 46 -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 1:a -map 2:v  video+monitor.mp4
```



### Audio del monitor independiente

Capturar:

1. Audio del *monitor* de la tarjeta de sonido interna del sistema, en un archivo independiente llamado `monitor.m4a`.
2. Audio del *micrófono* combinado con el video, en un archivo llamado `video+microfono.mp4`.

**Usando nombres:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 0:a monitor.m4a -f pulse -thread_queue_size 1024 -i default -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 1:a -map 2:v  video+microfono.mp4
```

**Usando números:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i 46 -map 0:a monitor.m4a -f pulse -thread_queue_size 1024 -i 47 -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 1:a -map 2:v  video+microfono.mp4
```



### Micrófono, monitor y video independientes

Capturar:

1. Audio del *micrófono* en un archivo independiente  llamado `microfono.m4a`.
2. Audio del *monitor* de la tarjeta de sonido interna del sistema, en un archivo independiente llamado `monitor.m4a`.
3. Video sin audio en un archivo independiente llamado `video.mp4`.

**Usando nombres:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i default -map 0:a microfono.m4a -f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 1:a monitor.m4a -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 2:v  video.mp4
```

> - `-vaapi_device /dev/dri/renderD128`: Usa el dispositivo VAAPI especificado en `/dev/dri/renderD128` para acelerar la codificación de video.
> - `-f pulse -thread_queue_size 1024 -i default -map 0:a microfono.m4a`: Captura como primer flujo `-map 0:a` el audio del dispositivo de entrada predeterminado (por ejemplo, un micrófono) a través del servidor de sonido PulseAudio, y lo guarda en el archivo `microfono.m4a`.
> - `-f pulse -thread_queue_size 1024 -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 1:a monitor.m4a`: Captura como segundo flujo `-map 1:a` el audio que se está reproduciendo actualmente en el dispositivo de salida predeterminado (por ejemplo, los altavoces) a través de PulseAudio, y lo guarda en el archivo `monitor.m4a`.
> - `-f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0`: Captura la pantalla `:0` del escritorio a través del servidor de pantalla X11.
> - `-vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17`: Usa la aceleración de hardware de video VAAPI para convertir y codificar el video en el formato H.264 con una tasa de bits constante de 17, lo que significa que la calidad del video se mantiene constante y la cantidad de datos utilizados para representar el video varía.
> - `-map 2:v video.mp4`: Guarda el tercer flujo, es decir, el video capturado en un archivo llamado `video.mp4`.

**Usando números:**

```sh
ffmpeg -vaapi_device /dev/dri/renderD128 -f pulse -thread_queue_size 1024 -i 47 -map 0:a microfono.m4a -f pulse -thread_queue_size 1024 -i 46 -map 1:a monitor.m4a -f x11grab -thread_queue_size 2048 -video_size 1366x768 -i :0 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 17 -map 2:v  video.mp4
```





