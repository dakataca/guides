```shell
setfont ter-i20n
iwctl
  station wlan0 connect Memoria
lsblk -d
lsblk -pso NAME,ROTA /dev/sda
parted /dev/sda print | grep -E "Model|abl|Dis"
ls /sys/firmware/efi/efivars
gdisk /dev/sda
   o
   Y
   p
   n
   Enter
   Enter
   +300M
   ef00
   n
   Enter
   Enter
   +1G
   8200
   n
   Enter
   Enter
   Enter
   8304
   w
   Y
lsblk -p /dev/sda
mkfs.fat -F 32 -n EFI /dev/sda1
mkswap -L swap /dev/sda2
mkfs.btrfs -f -L root -R free-space-tree /dev/sda3
lsblk -po NAME,FSTYPE,LABEL,MOUNTPOINT,SIZE /dev/sda
mount /dev/sda3 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var_log
btrfs subvolume create /mnt/@var_tmp
btrfs subvolume create /mnt/@snapshots
btrfs subvolume list -p /mnt
umount /mnt
swapon /dev/sda2
mount -o noatime,compress=zstd,subvol=@ /dev/sda3 /mnt
mount --mkdir -o noatime,compress=zstd,subvol=@home /dev/sda3 /mnt/home
mount --mkdir -o noatime,compress=zstd,subvol=@var_log /dev/sda3 /mnt/var/log
mount --mkdir -o noatime,compress=zstd,subvol=@var_tmp /dev/sda3 /mnt/var/tmp
mount --mkdir /dev/sda1 /mnt/boot
lsblk -p /dev/sda
ls /mnt
rm -f /etc/pacman.d/mirrorlist && curl -L http://ix.io/4l8N -so /etc/pacman.d/mirrorlist
cat /etc/pacman.d/mirrorlist

#pacstrap /mnt base{,-devel} linux{-{lts,headers,firmware},} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim os-prober terminus-font  efibootmgr
pacstrap /mnt base{,-devel} linux{-{lts,headers,firmware},} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim terminus-font efibootmgr

genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab

mkdir -p /mnt/var/lib/iwd/
cp /var/lib/iwd/*.psk /mnt/var/lib/iwd/
```



### Chroot ###

```shell
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock --systohc
echo 'es_CO.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=es_CO.UTF-8' > /etc/locale.conf
echo 'FONT=ter-i20n' > /etc/vconsole.conf
echo aceres1 > /etc/hostname

nvim /etc/hosts
----------------------------------------------------------------
127.0.0.1				localhost
::1					localhost
127.0.1.1	aceres1.localdomain	aceres1
```





Añadir módulos `i915`, `btrfs` y agregar hook `resume` y eliminar hook `fsck` :

```shell
nvim /etc/mkinitcpio.conf
----------------------------------------------------------------
MODULES=(btrfs zram i915)
...
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems resume)
```

```shell
mkinitcpio -P
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Archlinux
```

Eliminar parámetro del kernel `quiet`, añadir `resume` igualado al dispositivo `swap` y agregar parámetros para recordar el último kernel usado:

```shell
nvim /etc/default/grub
----------------------------------------------------------------
GRUB_TIMEOUT=3
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume=/dev/sda2"
...
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
```

```shell
grub-mkconfig -o /boot/grub/grub.cfg
passwd
#useradd -mG wheel,input,audio,video,optical,scanner,lp -s /bin/bash dakataca
useradd -mG wheel,input,audio,video,optical -s /bin/bash dakataca
passwd dakataca
```

```shell
EDITOR=nvim visudo
----------------------------------------------------------------
%wheel      ALL=(ALL:ALL) ALL
```

```shell
nvim /etc/pacman.conf
----------------------------------------------------------------
# Misc options
Color
ParallelDownloads = 2
ILoveCandy

[multilib]
Include = /etc/pacman.d/mirrorlist
```



**Paquetes**

```shell
pacman -Sy intel-ucode openssh ldns iwd grub-btrfs snapper usbutils htop neofetch arch-install-scripts rsync git --needed --noconfirm
#pacman -Sy intel-ucode openssh iwd grub-btrfs snapper usbutils openresolv htop neofetch arch-install-scripts rsync git --needed --noconfirm


```



**Internet**: *iwd* y *systemd-netowrkd*.

iwctl:

```shell
mkdir -p /etc/iwd/
```

```shell
nvim /etc/iwd/main.conf
----------------------------------------------------------------
[Network]
NameResolvingService=resolvconf

[General]
EnableNetworkConfiguration=true
```



systemd-networkd:

```shell
nvim /etc/systemd/network/20-wired.network
----------------------------------------------------------------
[Match]
Name=enp*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s

[DHCPv4]
RouteMetric=10
```

```shell
nvim /etc/systemd/network/25-wireless.network
----------------------------------------------------------------
[Match]
Name=wl*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s

[DHCPv4]
RouteMetric=20
```

**Nota:** `IgnoreCarrierLoss=3s`  garantiza que systemd-networkd no reconfigurará la interfaz, lo que se traduce en un tiempo de inactividad más corto (3s) cuando se está en roaming.



```shell
systemctl enable --now {iwd,systemd-networkd,sshd}.service

```



DNS

```shell
nvim /etc/resolv.conf
----------------------------------------------------------------
nameserver ::1
nameserver 127.0.0.1
```

```shell
chattr +i /etc/resolv.conf
```



dnsmasq

```shell
nvim /etc/dnsmasq.conf
----------------------------------------------------------------
listen-address=::1,127.0.0.1
cache-size=1000
conf-file=/usr/share/dnsmasq/trust-anchors.conf
dnssec
no-resolv
# Servidores de nombres de Google y Cloudflare.
server=8.8.8.8
server=8.8.4.4
server=1.1.1.1
server=1.0.0.1
```

```shell
systemctl enable dnsmasq.service
```



```shell
exit
swapoff -a
umount -R /mnt
reboot
ip a
```

Iniciar el kernel `linux` manualmente en el `grub`.



Openresolv

```shell
nvim /etc/resolv.conf
--------------------------------------------------------------
# Resolver configuration file.
# See resolv.conf(5) for details.
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
nameserver 1.0.0.1
```



Iniciar sesión con usuario `dakataca` para luego entrar vía `ssh` a `root`:

```shell
#iwctl
#  station wlan0 connect Memoria
ssh dakataca@192.168.1.102
su
chattr +i /etc/resolv.conf
ping -c3 archlinux.org
snapper -c config create-config /
```



```shell
mount --mkdir -o noatime,compress=zstd,subvol=@snapshots /dev/sda3 /mnt/.snapshots
chmod a+rx /.snapshots
chown :wheel /.snapshots
genfstab -U / >> /etc/fstab
sudo systemctl enable snapper-{timeline,cleanup}.timer
sudo systemctl enable grub-btrfsd.service
```





Cambiarse a usuario `dakataca`:

```shell
exit
dakataca
sudo pacman -S xdg-user-dirs
LC_ALL=C xdg-user-dirs-update --force
```





Xorg para Windows Manager

```shell
sudo pacman -S xorg-{server,xinit} --needed --noconfirm
```



Qtile

```shell
sudo pacman -S qtile
```



Configuraciones de Qtile WM:

- Comentar líneas innecesarias de `~/.xinitrc`.
- Insertar línea de ejecución de `qtile`.
- Copiar fichero de configuración.
- Validar fichero de configuración.

```shell
sed -E "/xclock|(exec )?xterm|twm/s/^/#/g" /etc/X11/xinit/xinitrc > ~/.xinitrc

echo 'exec qtile start' >> ~/.xinitrc

install -D /usr/share/doc/qtile/default_config.py ~/.config/qtile/config.py

python -m py_compile ~/.config/qtile/config.py
```



**Touchpad**

```shell
sudo nvim /etc/X11/xorg.conf.d/30-touchpad.conf
----------------------------------------------------------------
Section "InputClass"
	Identifier "touchpad"
	Driver "libinput"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*
    	Option "AccelProfile" "adaptive"
    	Option "AccelSpeed" "0.2"
    	Option "Tapping" "true"
    	Option "DisableWhileTyping" "true"
    	Option "TappingButtonMap" "lrm"
    	Option "TappingDragLock" "true"
    	Option "SendEventsMode" "disabled-on-external-mouse"
    	Option "NaturalScrolling" "true"
EndSection
```



Paquetes necesarios.

```shell
sudo pacman -S ttf-{inconsolata,dejavu,hack,roboto,liberation} hunspell-es_co zip unzip unrar unarj p7zip lzop lzip lrzip lha cpio arj pigz pbzip2 vlc breeze{,-gtk} ttf-nerd-fonts-symbols-2048-em wqy-microhei unicode-character-database noto-fonts-emoji android-{tools,file-transfer} heimdall bc nmap speedtest-cli tldr man-pages-es alacritty tmux lib32-pipewire pipewire{-{alsa,pulse,jack},} wireplumber alsa-utils cmus mediainfo yt-dlp breeze{,-gtk} mousepad mpv atomicparsley mplayer peek telegram-desktop chromium scrot cmus thunar acpi --needed
```



Iniciar Qtile

```shell
startx
```



Alacritty config

```shell
mkdir -p ~/.config/alacritty/
nvim ~/.config/alacritty/alacritty.yml
----------------------------------------------------------------
font:
  normal:
    #family: DejaVu Sans Mono
    family: Inconsolata medium
    style: Regular

  bold:
    family: Inconsolata medium
    style: Bold

  italic:
    family: Inconsolata medium
    style: Italic

  bold_italic:
    family: Inconsolata medium
    style: Bold Italic

  size: 14
```



Tmux config

```shell
nvim ~/.tmux.conf
--------------------------------------------------------------
'set -g mouse on'
# set alacritty terminal colors 24bits and 256colors.
'set -g default-terminal "tmux-256color"'
'set -ga terminal-overrides ", xterm-alacritty: Tc"'
# set first window to index 1 (not 0) to map more to the keyboard layout
'set -g base-index 1'
'set -g pane-base-index 1'
# Rearrange windows.
'bind-key -n S-Left swap-window -t -1 \; select-window -t -1'
'bind-key -n S-Right swap-window -t +1 \; select-window -t +1'
'bind r source-file ${HOME}/.tmux.conf \; display-message "source-file reloaded"'
```



Driver de video Intel i965 modesetting

```shell
su
pacman -S libva{-intel-driver,-utils} vdpauinfo libvdpau-va-gl intel-gpu-tools reflector --needed --noconfirm
```



### Configurar pantalla

Para una pantalla de `14"` pulgadas y resolución `1366x768`:

```shell
echo 'scale=5;sqrt(1366^2+768^2)' | bc
1567.09284

echo 'scale=5;(14/1567.09284)*1366*25.4' | bc
309.83885

echo 'scale=5;(14/1567.09284)*768*25.4' | bc
174.19929
```



Consultar nombre de pantalla

```shell
xrandr | awk -F':' '/[0-9]+: minimum/ {print $1}'
Screen 0
```



No solicite servicio `acpid.service` evitar que un usuario elimine X cuando se está ejecutando:

```shell
nvim /etc/X11/xorg.conf
--------------------------------------------------------------

Section "ServerFlags"
        Option "NoPM" "true" # Do not ask for acpid.service.
		Option "DontZap" "True" # User not kill X server.
EndSection
```



Establecer tamaño de pantalla en milímetros:

```shell
nvim /etc/X11/xorg.conf.d/90-monitor.conf
--------------------------------------------------------------
# Set displaySize for screen 14" and resolution 1366x768.
Section "Monitor"
    Identifier		"LVDS-1"
    DisplaySize		309.83885 174.19929    # In millimeters
    Modes "1366x768"
EndSection
```



**Nota:** Establecer [segunda pantalla](https://wiki.archlinux.org/title/xrandr#Testing_configuration)



[Teclado](https://man.archlinux.org/man/xkeyboard-config.7) Latino, español y EEUU con tilde muerta.

```shell
nvim /etc/X11/xorg.conf.d/00-keyboard.conf
----------------------------------------------------------------
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "us,latam,es"
        Option "XkbModel" "pc86,acer_laptop,acer_laptop"
        Option "XkbVariant" "altgr-intl,deadtilde,deadtilde"
        Option "XkbOptions" "grp:alt_shift_toogle"
EndSection
```



Variables de entorno

```shell
nvim /etc/environment
----------------------------------------------------------------
VISUAL=nvim
LC_ALL=es_CO.UTF-8
XDG_CONFIG_HOME   DEFAULT=@{HOME}/.config
```



**zram-generator**

Instalación

```shell
pacman -S zram-generator --needed --noconfirm
```



Desactivar zwap, activar zram  y tcpfastopen:

```shell
nvim /etc/sysctl.d/99-sysctl.conf
----------------------------------------------------------------
zswap.enabled=0
vm.swappiness=10
net.ipv4.tcp_fastopen=3
vm.vfs_cache_pressure=50
```



```shell
nvim /etc/systemd/zram-generator.conf
----------------------------------------------------------------
[zram0]
swap-priority = 60
compression-algorithm=lz4

[zram1]
mount-point = /run
compression-algorithm = lz4

[zram2]
mount-point = /tmp
compression-algorithm = lz4

[zram3]
mount-point = /var/log
compression-algorithm = lz4

[zram4]
mount-point = /var/compressed
options = X-mount.mode=1777
compression-algorithm = lz4
```



Administración de energía

```shell
sed -i.back "a\HandlePowerKey=suspend\nHandleSuspendKey=suspend\nHandleHibernateKey=suspend" /etc/systemd/logind.conf
```



Habilitar multihilo  en `makepkg.conf`:

```shell
sed -i.back "/MAKEFLAGS=/ s/#\| //g ; /MAKEFLAGS=/ s/[0-9]\+/\$\(nproc\)/ ; /^COMPRESSGZ/ s/gzip/pigz/ ; /^COMPRESSBZ2/ s/(bzip2/(pbzip2/ ; /^COMPRESSXZ\|^COMPRESSZST/ s/-)/--threads=0 -)/"  /etc/makepkg.conf
```



Reflector

```shell
nvim /etc/xdg/reflector/reflector.conf
----------------------------------------------------------------
--save /etc/pacman.d/mirrorlist
--protocol http,https,rsync
--sort rate
--completion-percent 95
--connection-timeout 9
--download-timeout 9
--cache-timeout 9
--threads 2
--latest 15
-f 15
--age 12
```

```shell
systemctl enable reflector.timer
```



Configuración de cargo:

```shell
exit
dakataca
mkdir -p ~/.cargo/
```

```shell
nvim ~/.cargo/config
--------------------------------------------------------------
#Put it in the file '$home/.cargo/config'
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"

#Replace it with your preferred mirror source
replace-with = 'tuna'
#replace-with = 'ustc'
#replace-with = 'sjtu'
#replace-with = 'rustcc'

#Tsinghua University
[source.tuna]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"

#University of science and technology of China
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"

#Shanghai Jiaotong University
[source.sjtu]
registry = "https://mirrors.sjtug.sjtu.edu.cn/git/crates.io-index"

#Rustcc community
[source.rustcc]
registry = "git://crates.rustcc.cn/crates.io-index"
```



Git config

```shell
git config --global user.name dakataca
git config --global user.email danieldakataca@gmail.com
git config --global core.autocrlf input
# nvim git config, solved error vimdiff use vim.
git config --global core.editor nvim
git config --global diff.tool vimdiff3
git config --global difftool.vimdiff3.path nvim
git config --global merge.tool vimdiff3
git config --global mergetool.vimdiff3.path nvim
```



```shell
cd /tmp/
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```



```shell
cd
mkdir -p Git/ && cd Git
git clone https://gitlab.com/dakataca/dotfiles
git clone https://gitlab.com/dakataca/aur
git clone https://gitlab.com/dakataca/guides
paru -S wps-office ttf-wps-fonts wps-office-mui-es-es
sudo rm -r /lib/office6/mui/*_{CN,US,RU}
```



**yt-dlp**

```shell
mkdir -p ~/.config/yt-dlp/
nvim ~/.config/yt-dlp/config
--------------------------------------------------------------
# Save all videos under YouTube directory in your home directory.
-o ~/Videos/yt-dlp/%(title)s.%(ext)s

## Subtitles configuration.
--sub-langs la,es,en

# Embed thumbnail in the video as cover art.
--embed-thumbnail

#-f "bv[height<=2000][ext=mp4][fps<=60][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"
-f "bv[height<=1200][ext=mp4][fps<=30][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"
#-f "bv[height<=800][ext=mp4][fps<=30][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"
```





**mpv**

`yta search term`, introduzca la siguiente fusión en su `.bashrc`:

```shell
nvim .bashrc
--------------------------------------------------------------
function yta() {
    mpv --ytdl-format=bestaudio ytdl://ytsearch:"$*"
}
```



```shell
nvim /etc/mpv/mpv.conf
--------------------------------------------------------------
#vd-lavc-assume-old-x264=yes
#hwdec=vaapi
hwdec=auto
vo=gpu
gpu-hwdec-interop=vaapi
msg-level=vo=fatal
fs=yes
cache=yes
cache-pause=no
vlang=lat,spa,eng
alang=lat,spa,eng
slang=lat,spa,eng
#ytdl-format=bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9]+bestaudio/best
ytdl-format=bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best
#ytdl-format=bestvideo[height<=?480][fps<=?30][vcodec!=?vp9]+bestaudio/best
```



Configuración del driver de video:

```shell
sudo pacman -S xf86-video-intel --needed

nvim /etc/X11/xorg.conf.d/20-intel.conf
-----------------------------------
Section "Device"
Identifier   "Intel Graphics"
    Driver	"intel"
    Option	"TearFree"	"true"
#   Option   "AccelMethod"	"sna" # default.
#	Option   "AccelMethod"	"uxa" # disable acceleration.
#	Option	"DRI"	"2" # disable aceleration 3D.
EndSection
```



```shell
nvim ~/.config/chromium-flags.conf
--------------------------------------------------------------
--force-dark-mode
# Force GPU/vaapi acceleration.
--ignore-gpu-blocklist
--enable-gpu-rasterization
--enable-zero-copy
```



```shell
systemctl disable sshd.service
```

