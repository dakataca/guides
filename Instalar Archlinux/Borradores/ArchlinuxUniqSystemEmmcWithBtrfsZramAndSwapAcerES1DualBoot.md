```shell
iwctl
  station wlan0 connect Memoria
setfont ter-i18n
lsscsi -s
lsblk -pso NAME,ROTA /dev/sda
parted /dev/sda print | grep -E "Model|abl|Dis"
ls /sys/firmware/efi/efivars
gdisk /dev/sda
   p
   n
   Enter
   Enter
   +300M
   ef00
   n
   Enter
   Enter
   +2G
   8200
   n
   Enter
   Enter
   Enter
   8304
   w
   Y
lsblk -p /dev/sda
mkswap -L swap /dev/sda5
swapon /dev/sda5
mkfs.btrfs -f -L root -R free-space-tree /dev/sda6
mount /dev/sda6 /mnt
lsblk -po NAME,FSTYPE,LABEL,MOUNTPOINT,SIZE /dev/sda
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var_log
btrfs subvolume create /mnt/@var_tmp
btrfs subvolume create /mnt/@snapshots
btrfs subvolume list -p /mnt
ls /mnt
umount /mnt
mount -o noatime,compress=zstd,subvol=@ /dev/sda6 /mnt
mount --mkdir -o noatime,compress=zstd,subvol=@home /dev/sda6 /mnt/home
mount --mkdir -o noatime,compress=zstd,subvol=@var_log /dev/sda6 /mnt/var/log
mount --mkdir -o noatime,compress=zstd,subvol=@var_tmp /dev/sda6 /mnt/var/tmp
mkfs.fat -F 32 -n EFI /dev/sda4
mount --mkdir /dev/sda4 /mnt/boot
lsblk -p /dev/sda
ls -R /mnt
curl -L http://ix.io/4l8N > /etc/pacman.d/mirrorlist
cat /etc/pacman.d/mirrorlist

pacstrap /mnt base{,-devel} linux{,-{lts,headers,firmware}} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim terminus-font os-prober efibootmgr

genfstab -U /mnt > /mnt/etc/fstab
cat /mnt/etc/fstab

mkdir -p /mnt/var/lib/iwd/
cp /var/lib/iwd/*.psk /mnt/var/lib/iwd/
```



### Chroot ###

```shell
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock --systohc
echo 'es_CO.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=es_CO.UTF-8' > /etc/locale.conf
echo 'FONT=Lat2-Terminus16' > /etc/vconsole.conf
```



### Host ###

```shell
echo acer411 > /etc/hostname
```

```shell
nvim /etc/hosts
----------------------------------------------------------------
127.0.0.1				localhost
::1					localhost
127.0.1.1	acer411.localdomain	acer411
```



Añadir módulos `btrfs`, `zram` e `i915` , eliminar hook `fsck` y agregar hook `resume`:

```shell
nvim /etc/mkinitcpio.conf
----------------------------------------------------------------
MODULES=(btrfs zram i915)
...
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems resume)
```

```shell
mkinitcpio -P
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Archlinux
```

Eliminar parámetro del kernel `quiet`, añadir `resume` igualado al dispositivo `swap`, agregar parámetros para recordar el último kernel usado y descomentar linea para reconocer otros sistemas operativos:

```shell
nvim /etc/default/grub
----------------------------------------------------------------
GRUB_TIMEOUT=3
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume=/dev/sda2"
...
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true

GRUB_DISABLE_OS_PROBER=false
```

```shell
grub-mkconfig -o /boot/grub/grub.cfg
## Ignore error "ERROR: mkdir /var/lock/dmraid". Es por usar os-prober en arch-chroot.

passwd
#useradd -mG wheel,input,audio,video,optical,scanner,lp -s /bin/bash dakataca
useradd -mG wheel,input,audio,video,optical -s /bin/bash dakataca
passwd dakataca
```

```shell
EDITOR=nvim visudo
----------------------------------------------------------------
%wheel      ALL=(ALL:ALL) ALL
```

```shell
nvim /etc/pacman.conf
----------------------------------------------------------------
# Misc options
Color
ParallelDownloads = 2
ILoveCandy

[multilib]
Include = /etc/pacman.d/mirrorlist
```



**Paquetes**

```shell
pacman -Sy intel-ucode open{ssh,resolv} dnsmasq ldns iwd net-tools grub-btrfs snapper usbutils htop neofetch lsscsi arch-install-scripts rsync git --needed --noconfirm
```



**Internet**: *iwd* y *systemd-netowrkd*.

iwctl:

```shell
mkdir -p /etc/iwd/
```

```shell
nvim /etc/iwd/main.conf
----------------------------------------------------------------
[Network]
# Establecer Openresolv(resolvconf) como administrador de DNS.
NameResolvingService=resolvconf

[General]
# Asignar direcciones IP usando el cliente DHCP incorporado.
EnableNetworkConfiguration=true
```



systemd-networkd:

```shell
nvim /etc/systemd/network/20-wired.network
----------------------------------------------------------------
[Match]
Name=enp*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s

[DHCPv4]
RouteMetric=10
```

```shell
nvim /etc/systemd/network/25-wireless.network
----------------------------------------------------------------
[Match]
Name=wl*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s

[DHCPv4]
RouteMetric=20
```

**Nota:** `IgnoreCarrierLoss=3s` garantiza que systemd-networkd no reconfigurará la interfaz, lo que se traduce en un tiempo de inactividad más corto (3s) cuando se está en roaming.



1dnsmasq

```shell
> /etc/dnsmasq.conf
nvim /etc/dnsmasq.conf
----------------------------------------------------------------
# Utilice el servidor DNS local.
listen-address=127.0.0.1,192.168.1.1

# Establecer cantidad de nombres de dominio almacenados.
cache-size=1000

# Cargue los anclajes de confianza de DNSSEC.
conf-file=/usr/share/dnsmasq/trust-anchors.conf
dnssec

# No lea innecesariamente /etc/resolv.conf.
no-resolv

# Servidores de nombres de dominio de Google.
server=8.8.8.8
server=8.8.4.4

# Servidores de nombres de dominio de Cloudflare.
server=1.1.1.1
server=1.0.0.1

# Leer la configuración generada por openresolv.
conf-file=/etc/dnsmasq-openresolv.conf
resolv-file=/etc/dnsmasq-resolv.conf
```



DNS

```shell
nvim /etc/resolvconf.conf
----------------------------------------------------------------
# Utilizar el servidor de nombres de dominio local.
name_servers=127.0.0.1
resolv_conf_options="trust-ad"

# Escribir los archivos de configuración extendida y de resolución de dnsmasq.
dnsmasq_conf=/etc/dnsmasq-openresolv.conf
dnsmasq_resolv=/etc/dnsmasq-resolv.conf
```



```shell
resolvconf -u
head /etc/dnsmasq-*.conf
echo '192.168.1.1' >> /etc/dnsmasq-resolv.conf
dnsmasq --test
systemctl enable {iwd,systemd-networkd,dnsmasq,sshd}.service
```



```shell
exit
swapoff -a
umount -R /mnt
reboot
ping -c3 archlinux.org #host
rm .ssh/known_host
ip a #host
ssh dakataca@192.168.1.102
```

Seleccionar el kernel `linux` manualmente durante el arranque en el `grub`.



Iniciar sesión con usuario `dakataca` para luego entrar vía `ssh` a `root`:

```shell
su
snapper -c config create-config /
btrfs subvolume list -p /
btrfs subvolume delete /.snapshots
btrfs subvolume list -p /
```



```shell
mount --mkdir -o noatime,compress=zstd,subvol=@snapshots /dev/sda6 /.snapshots
chmod a+rx /.snapshots
chown :wheel /.snapshots
genfstab -U / > /etc/fstab
systemctl enable snapper-{timeline,cleanup}.timer
systemctl enable grub-btrfsd.service
```



[Teclado](https://man.archlinux.org/man/xkeyboard-config.7) Latino, español y EEUU con tilde muerta.

```shell
nvim /etc/X11/xorg.conf.d/00-keyboard.conf
----------------------------------------------------------------
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "us,latam,es"
        Option "XkbModel" "pc86,acer_laptop,acer_laptop"
        Option "XkbVariant" "altgr-intl,deadtilde,deadtilde"
        Option "XkbOptions" "grp:alt_shift_toogle"
EndSection
```



Variables de entorno

```shell
nvim /etc/environment
----------------------------------------------------------------
# Establecer editor de texto completo.
VISUAL=nvim

# XDG Base Directory specification: User directories.
XDG_CONFIG_HOME=${HOME}/.config
```



**zram-generator**

Instalación

```shell
pacman -S zram-generator --needed --noconfirm
```



Desactivar zwap, activar zram y tcpfastopen:

```shell
nvim /etc/sysctl.d/99-sysctl.conf
----------------------------------------------------------------
# Deshabilitar zswap.
zswap.enabled=0

# Controla la tendentencia del núcleo a usar el espacio de intercambio (SWAP/ZRAM).
# 10 (<=4G), 35 (<=8GB), 50 (<=16GB) 60 <<default>> (>=32GB).
vm.swappiness=10

# Habilitar TCP Fast Open.
net.ipv4.tcp_fastopen=3

# Controlar la tendencia del núcleo a reclamar memoria para el almacenamiento en (Caché VFS).
# 50 (<=4G), 75 (<=8GB), 90 (<=16GB) 100 <<default>> (>=32GB).
vm.vfs_cache_pressure=50
```



```shell
nvim /etc/systemd/zram-generator.conf
----------------------------------------------------------------
[zram0]
swap-priority = 60
compression-algorithm=lz4

[zram1]
mount-point = /run
compression-algorithm = lz4

[zram2]
mount-point = /tmp
compression-algorithm = lz4

[zram3]
mount-point = /var/log
compression-algorithm = lz4

[zram4]
mount-point = /var/compressed
options = X-mount.mode=1777
compression-algorithm = lz4
```



Configuración del driver de video (**no recomendado**):

```shell
pacman -S xf86-video-intel --needed

nvim /etc/X11/xorg.conf.d/20-intel.conf
-----------------------------------
Section "Device"
Identifier   "Intel Graphics"
    Driver	"intel"
	Option	"TearFree"	"true"
#	Option   "AccelMethod"	"sna" # default.
#	Option   "AccelMethod"	"uxa" # disable acceleration.
#	Option	"DRI"	"2" # disable aceleration 3D.
EndSection
```



**Touchpad**

```shell
nvim /etc/X11/xorg.conf.d/30-touchpad.conf
----------------------------------------------------------------
Section "InputClass"
	Identifier "touchpad"
	Driver "libinput"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*
    	Option "AccelProfile" "adaptive"
    	Option "AccelSpeed" "0.2"
    	Option "Tapping" "true"
    	Option "DisableWhileTyping" "true"
    	Option "TappingButtonMap" "lrm"
    	Option "TappingDragLock" "true"
    	Option "SendEventsMode" "disabled-on-external-mouse"
    	Option "NaturalScrolling" "true"
EndSection
```



Cambiarse a usuario `dakataca`:

```shell
exit
sudo pacman -S xdg-user-dirs
LC_ALL=C xdg-user-dirs-update --force
```



### Qtile

Xorg para Windows Manager `qtile`, terminal `alacritty`, multiplexor de terminal `tmux`, navegadores y pipewire.

```shell
sudo pacman -S xorg-{server,xinit} qtile alacritty tmux chromium firefox lib32-pipewire pipewire{,-{alsa,pulse,jack}} wireplumber alsa-utils --needed --noconfirm
```



#### Configuraciones:

- Comentar líneas innecesarias de `~/.xinitrc`.
- Insertar línea de ejecución de `qtile`.
- Copiar fichero de configuración.
- Validar fichero de configuración.

```shell
sed -E "/xclock|(exec )?xterm|twm/s/^/#/g" /etc/X11/xinit/xinitrc > ~/.xinitrc

echo 'exec qtile start' >> ~/.xinitrc

install -D /usr/share/doc/qtile/default_config.py ~/.config/qtile/config.py

python -m py_compile ~/.config/qtile/config.py
```



Alacritty config

```shell
mkdir -p ~/.config/alacritty/
nvim ~/.config/alacritty/alacritty.yml
----------------------------------------------------------------
font:
  normal:
    #family: DejaVu Sans Mono
    family: Inconsolata medium
    style: Regular

  bold:
    family: Inconsolata medium
    style: Bold

  italic:
    family: Inconsolata medium
    style: Italic

  bold_italic:
    family: Inconsolata medium
    style: Bold Italic

  size: 14
```



Tmux config

```shell
nvim ~/.tmux.conf
--------------------------------------------------------------
set -g mouse on
# set alacritty terminal colors 24bits and 256colors.
set -g default-terminal "tmux-256color"
set -ga terminal-overrides ", xterm-alacritty: Tc"
# set first window to index 1 (not 0) to map more to the keyboard layout
set -g base-index 1
set -g pane-base-index 1
# Rearrange windows.
bind-key -n S-Left swap-window -t -1 \; select-window -t -1
bind-key -n S-Right swap-window -t +1 \; select-window -t +1
bind r source-file ${HOME}/.tmux.conf \; display-message "source-file reloaded"
```



```shell
nvim ~/.config/chromium-flags.conf
--------------------------------------------------------------
# Forzar modo nocturno.
--force-dark-mode

# Forzar aceleración de GPU.
--ignore-gpu-blocklist
--enable-gpu-rasterization
--enable-zero-copy

# Habilitar aceleración de video VAAPI.
--enable-features=VaapiVideoDecoder
```



Iniciar Qtile

```shell
startx
```



Paquetes necesarios.

```shell
sudo pacman -S acpi ttf-{inconsolata,dejavu,hack,roboto,liberation} hunspell-es_co zip unzip unrar unarj p7zip lzop lzip lrzip lha cpio arj pigz pbzip2 vlc breeze{,-gtk} ttf-nerd-fonts-symbols-2048-em wqy-microhei unicode-character-database noto-fonts-emoji android-{tools,file-transfer} heimdall bc nmap speedtest-cli tldr man-pages-es cmus mediainfo yt-dlp breeze{,-gtk} mousepad mpv atomicparsley mplayer peek telegram-desktop scrot cmus thunar --needed
```



**yt-dlp**

```shell
mkdir -p ~/.config/yt-dlp/
nvim ~/.config/yt-dlp/config
--------------------------------------------------------------
# Guarde sus videos en su directorio de usuario preferido.
-o ~/Videos/yt-dlp/%(title)s.%(ext)s

# Idioma de subtítulos latinos, espaón o inglés.
--sub-langs la,es,en

# Incrustar miniatura de video como portada, requiere el paquete "atomicparsley".
--embed-thumbnail


## Video format:AVC/AAC LC, no codec vp9.

# 4K 60fps.
#-f "bv[height<=2500][ext=mp4][fps<=60][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"

# 2K 60fps.
#-f "bv[height<=2000][ext=mp4][fps<=60][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"

# FHD 30fps.
-f "bv[height<=1200][ext=mp4][fps<=30][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"

# HD 30fps.
#-f "bv[height<=800][ext=mp4][fps<=30][vcodec!=vp9]+ba[ext=m4a][abr<=48000]"
```





### Configurar pantalla

Para una pantalla de `14"` pulgadas y resolución `1366x768`:

```shell
echo 'scale=5;sqrt(1366^2+768^2)' | bc
1567.09284

echo 'scale=5;(14/1567.09284)*1366*25.4' | bc
309.83885

echo 'scale=5;(14/1567.09284)*768*25.4' | bc
174.19929
```



Consultar nombre de pantalla

```shell
xrandr | awk -F':' '/[0-9]+: minimum/ {print $1}'
Screen 0
```



No solicite servicio `acpid.service` evitar que un usuario elimine X cuando se está ejecutando:

```shell
nvim /etc/X11/xorg.conf
--------------------------------------------------------------
Section "ServerFlags"
	Option "NoPM" "true" # Do not ask for acpid.service.
	Option "DontZap" "True" # User not kill X server.
EndSection
```



Establecer tamaño de pantalla en milímetros:

```shell
nvim /etc/X11/xorg.conf.d/90-monitor.conf
--------------------------------------------------------------
# Set displaySize for screen 14" and resolution 1366x768.
Section "Monitor"
    Identifier		"LVDS-1"
    DisplaySize		309.83885 174.19929    # In millimeters
EndSection
```



**Nota:** Establecer [segunda pantalla](https://wiki.archlinux.org/title/xrandr#Testing_configuration)



Administración de energía

```shell
sed -i.back "a\HandlePowerKey=suspend\nHandleSuspendKey=suspend\nHandleHibernateKey=suspend" /etc/systemd/logind.conf
```



Habilitar multihilo en `makepkg.conf`:

```shell
sed -i.back "/MAKEFLAGS=/ s/#\| //g ; /MAKEFLAGS=/ s/[0-9]\+/\$\(nproc\)/ ; /^COMPRESSGZ/ s/gzip/pigz/ ; /^COMPRESSBZ2/ s/(bzip2/(pbzip2/ ; /^COMPRESSXZ\|^COMPRESSZST/ s/-)/--threads=0 -)/" /etc/makepkg.conf
```



VAAPI driver para Intel i965 y reflector:

```shell
pacman -S libva{-intel-driver,-utils} vdpauinfo libvdpau-va-gl intel-gpu-tools reflector --needed --noconfirm
```

```shell
> /etc/xdg/reflector/reflector.conf
nvim /etc/xdg/reflector/reflector.conf
----------------------------------------------------------------
--save /etc/pacman.d/mirrorlist
--protocol http,https,rsync
--sort rate
--completion-percent 95
--connection-timeout 9
--download-timeout 9
--cache-timeout 9
--threads 2
--latest 15
-f 15
--age 12
```

```shell
systemctl enable reflector.timer
```



**mpv**

`yta search term`, introduzca la siguiente fusión en su `.bashrc`:

```shell
nvim ~/.bashrc
--------------------------------------------------------------
function yta() {
    mpv --ytdl-format=bestaudio ytdl://ytsearch:"$*"
}
```

```shell
su
nvim /etc/mpv/mpv.conf
--------------------------------------------------------------
# Usar compilación antigua (<=150) de x264. Usar solo para videos específicos!
#vd-lavc-assume-old-x264=yes

# Driver de video.
hwdec=auto
vo=gpu,vaapi,vdpau,xv

# Suprimir mensaje de error "Cannot load libcuda.so.1" al usar VAAPI.
gpu-hwdec-interop=vaapi

# Inicie a pantalla completa.
fs=yes


## Audio ##

# Inicie con volumen de audio específico.
volume=55

# Idiomas de audio y subtítulos en latino, español o inglés.
vlang=lat,spa,eng


## Streaming Video format:AVC/AAC LC, no codec vp9 ##

# 2K 60fps.
#ytdl-format=bestvideo[height<=?2000][fps<=?60][vcodec!=?vp9]+bestaudio/best

# FHD 60fps.
#ytdl-format=bestvideo[height<=?1200][fps<=?60][vcodec!=?vp9]+bestaudio/best

# FHD 30fps.
#ytdl-format=bestvideo[height<=?1200][fps<=?30][vcodec!=?vp9]+bestaudio/best

# HD 30fps.
ytdl-format=bestvideo[height<=?800][fps<=?30][vcodec!=?vp9]+bestaudio/best

# SD 30fps.
#ytdl-format=bestvideo[height<=?480][fps<=?30][vcodec!=?vp9]+bestaudio/best
```



Configuración de rust/cargo:

```shell
sudo pacman -S rustup --needed --noconfirm
```

```shell
rustup default stable
mkdir -p ~/.cargo/
```

```shell
nvim ~/.cargo/config
--------------------------------------------------------------
#Put it in the file '$home/.cargo/config'.

#Optimize for native CPU platform.
[target.x86_64-unknown-linux-gnu]
rustflags = ["-C", "target-cpu=native"]

[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"

#Replace it with your preferred mirror source.
replace-with = 'tuna'
#replace-with = 'ustc'
#replace-with = 'sjtu'
#replace-with = 'rustcc'

#Tsinghua University.
[source.tuna]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"

#University of science and technology of China.
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"

#Shanghai Jiaotong University.
[source.sjtu]
registry = "https://mirrors.sjtug.sjtu.edu.cn/git/crates.io-index"

#Rustcc community.
[source.rustcc]
registry = "git://crates.rustcc.cn/crates.io-index"
```

```shell
cd /tmp/
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```



Git config

```shell
git config --global user.name dakataca
git config --global user.email danieldakataca@gmail.com
git config --global core.autocrlf input
# nvim git config, solved error vimdiff use vim.
git config --global core.editor nvim
git config --global diff.tool vimdiff3
git config --global difftool.vimdiff3.path nvim
git config --global merge.tool vimdiff3
git config --global mergetool.vimdiff3.path nvim
```

```shell
cd
mkdir -p Git/ && cd Git
git clone https://gitlab.com/dakataca/dotfiles
git clone https://gitlab.com/dakataca/aur
git clone https://gitlab.com/dakataca/guides
paru -S wps-office ttf-wps-fonts wps-office-mui-es-es
sudo rm -r /lib/office6/mui/*_{CN,US,RU}
cd
```



```shell
sudo systemctl disable sshd.service
```



