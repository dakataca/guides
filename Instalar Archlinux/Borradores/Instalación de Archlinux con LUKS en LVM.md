

Instalación de Archlinux con LVM en LUKS dm-crypt

[TOC]







## 1  Distribución de Teclado

[Por defecto es](https://wiki.archlinux.org/title/Installation_guide#Set_the_console_keyboard_layout) `en`.



### 1.1  Latina

```shell
loadkeys la-latin1
```



### 1.2  Español

```shell
loadkeys es
```







## 2  Fuente de Terminal
Tamaño y tipo de [fuente de la terminal](https://man.archlinux.org/man/setfont.8).



### 2.1  Listar fuentes

```shell
ls /usr/share/kbd/consolefonts/
```

>  **Tip:** [Biblioteca de imágenes de fuentes disponibles para consola.](https://adeverteuil.github.io/linux-console-fonts-screenshots/#screenshots)



### 2.2  Fuente latina

```shell
setfont ter-i22b
```







## 3  Comprobar modo de arranque

```shell
ls /sys/firmware/efi/efivars
```

Si el directorio existe, es compatible con el modo de arranque **_UEFI_**, de lo contrario, lo es solo con **_Legacy_**.







## 4  Conectarse a Internet



### 4.1  Vía LAN

Comprobar si las [interfaces de red](https://wiki.archlinux.org/title/Network_interface_(Espa%C3%B1ol)) están listas y activas.

```shell
ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp3s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 9c:b6:54:47:a1:fe brd ff:ff:ff:ff:ff:ff
4: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DORMANT group default qlen 1000
    link/ether a4:db:30:15:6c:5a brd ff:ff:ff:ff:ff:ff
```

> 💡 **Tenga en cuenta:**
>
> -  El `UP` en `<BROADCAST,MULTICAST,UP,LOWER_UP>` indica que la interfaz está activa.
> - `lo` es la [interfaz virtual de bucle invertido](https://en.wikipedia.org/wiki/Loopback#irtual_loopback_interface) y no se utiliza para realizar conexiones de red.

### 4.2  Vía wifi con `iwd`



#### 4.2.1  Entrar al [modo interactivo.](https://wiki.archlinux.org/title/Iwd#iwctl)

```shell
iwctl
```



#### 4.2.2  Listar dispositivos

Verificar el estado de las interfaces inalámbricas:
```shell
[iwd]# device list
                        Devices
-----------------------------------------------------------------------
 Name         Address             Powered  Adapter  Mode
-----------------------------------------------------------------------
 wlan0        80:a5:89:d7:6c:3d   on       phy0     station
```
>**Nota:** En este caso el nombre de la interfaz es `wlan0`.



##### 4.2.2.1  Interfaz de red no aparece

Si la interface inalámbrica no se encuentra, intente manualmente: 
* [Cargar el módulo del driver](https://wiki.archlinux.org/title/Kernel_module#Manual_module_handling).
* [Levantar la interface de red](https://wiki.archlinux.org/title/Kernel_module#Manual_module_handling).



#### 4.2.3 Escanear redes disponibles

**Iniciar escaneo:**

```shell
[iwd]# station wlan0 scan
```

> 💡 **Tenga en cuenta:**
>
> - El nombre de la interfaz es `wlan0`.
>
> - Éste comando no tiene salida 🤐.

**Listar redes disponibles:**

```shell
[iwd]# station wlan0 get-networks
                               Available networks                         *
----------------------------------------------------------------------------
      Network name                      Security            Signal
----------------------------------------------------------------------------
      Memoria                           psk                 ****    
      moto e(7) plus 2011               psk                 ****    
      Adalberto                         psk                 **** 
```
> **Nota:** La red elegida es `Memoria`.



#### 4.2.4  Conectarse a una red inalámbrica

Conectarse a la red `Memoria`:

```shell
[iwd]# station wlan0 connect 'Memoria'
Passphrase:******
```



#### 4.2.5  Verificar si estamos conectados:

```shell
[iwd]# station list
                            Devices in Station Mode                            
----------------------------------------------------------------------------
  Name                  State            Scanning
----------------------------------------------------------------------------
  wlan0                 connected  
```


#### 4.2.6  Salir del modo interactivo:

```shell
[iwd]# exit
```



### 4.3  Verificar conexión de internet

[Comprobamos la salida a Internet](https://wiki.archlinux.org/title/Network_configuration#Ping):

```shell
ping -c3 archlinux.org
```







## 5  Fecha y hora

[Habilite e inicie el servicio de sincronización de fecha y hora](https://man.archlinux.org/man/timedatectl.1#COMMANDS):

```shell
timedatectl set-ntp true
```







## 6  Dispositivos de almacenamiento




### 6.1  Listar dispositivos de almacenamiento:
```shell
lsblk -Sp
NAME     HCTL       TYPE VENDOR   MODEL                 REV SERIAL         TRAN
/dev/sda 0:0:0:0    disk ATA      HGST HTS545050A7E380 AC90 TWA51DL91GHGVS sata
/dev/sr0 1:0:0:0    rom  hp       hp DVD-RAM UJ8D1     H.01 SBD8T03835     sata
```
>**Nota:** El dispositivo de almacenamiento elegido es `/dev/sda`.



>  **Tip:** Algunos posibles [nombres para dispositivos de bloque](https://wiki.archlinux.org/title/Device_file_(Espa%C3%B1ol)#Nombres_de_dispositivos_de_bloque) tipo `hdd`, `eMMc`, [`ssd 2.5"`](https://wiki.archlinux.org/title/Solid_state_drive), [`ssd M.2 msata`](https://wiki.archlinux.org/title/Solid_state_drive) o [`ssd M.2 nvme`](https://wiki.archlinux.org/title/Solid_state_drive), podrían ser:
>
> - `/dev/sdb`, `/dev/sdc`, `/dev/sdx/`
> - `/dev/hda`
> - `/dev/mmcblk0`
> - `/dev/vda`
> - `/dev/nvme0n1`



### 6.2  El dispositivo de almacenamiento es `hdd` o [`ssd`](https://wiki.archlinux.org/title/Solid_state_drive)?

```shell
lsblk  -pso NAME,ROTA /dev/sda
NAME     ROTA
/dev/sda    1
```

> **`hdd`**: Si la columna `ROTA` marca `1`.
>
> [**`ssd`**](https://wiki.archlinux.org/title/Solid_state_drive): Si la columna `ROTA` marca `0`.



### 6.3  Verificar esquema de partición

Consultamos la tabla de particiones del dispositivo de almacenamiento:
```shell
parted /dev/sda print | grep -E "Model|abl|Dis"
```
> **Nota:** El nombre del dispositivo de almacenamiento es `/dev/sda`.







## 7  Diseño de particiones para [LVM en LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS)

> **Nota:** A diferencia de [LUKS en LVM](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_LVM), [éste método](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS) no permite distribuir los volúmenes lógicos en varios discos, así que, con [LVM en LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS) estaremos limitados a distribuir los subvolúmenes lógicos en un solo dispositivo de bloque.



### 7.1  Esquema de particiones para LVM en LUKS

El método sencillo es configurar [LVM](https://wiki.archlinux.org/title/LVM) encima de un gran dispositivo de bloque cifrado. Un diseño básico sería el siguiente:

```shell
+----------------+ +-----------------------------------------------------------------------+ 
| Partición boot | | Volúmen lógico 1      | Volúmen lógico 2      | Volúmen lógico 3      | 
|                | |                       |                       |                       | 
|  /boot         | | [SWAP]                | /                     |  /home                |
|                | |                       |                       |                       |
| (puede estar   | | /dev/MyVolGroup/swap  | /dev/MyVolGroup/root  | /dev/MyVolGroup/home  |
| en  otro       | |_ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ _|
| dispositivo)   | |  (grupo de volúmenes)    /dev/MyVolGroup                              |
|                | |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|                | |   (contenedor LUKS)    /dev/mapper/cryptlvm (volúmen físico)          |
|                | |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|                | |                         Partición cifrada LUKS2                       |
| /dev/sda1      | |                           /dev/sda2                                   | 
+----------------+ +-----------------------------------------------------------------------+
```

> **Tips:**
>
> - Los **[GUID](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs)** y **[ID](https://en.wikipedia.org/wiki/Partition_type)**, son para particiones físicas, no para lógicas.
> - Los volúmenes lógicos son montados sobre un grupo de volúmenes llamado`vg1`, éste a su ves está sobre una partición física encriptada asignada como contenedor LUKS llamada `cryptlvm`  y montada en `/dev/mapper/cryptlvm `.
> - Si en su diseño la partición `/boot` hace parte de los volúmenes lógicos, debe establecer el [módulo del kernel `lvm` precargado en al configuración del *grub*](https://wiki.archlinux.org/title/GRUB#LVM).



### 7.2  UEFI en `gpt` sin [`swap`](https://wiki.archlinux.org/title/Swap)

Recomendado para usuarios que trabajan con correo local en dispositivos [`ssd`](https://wiki.archlinux.org/title/Solid_state_drive).

<table>
  <tr>
      <th rowspan=2>Punto de montaje</th>
      <th colspan=2><p style="text-align:center">Partición</p></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs" target="_blank">GUID de tipo del partición</a></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/Partition_type" target="_blank">ID de tipo de partición</a></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_entries_.28LBA_2.E2.80.9333.29" target="_blank">Atributos de la partición</a></th>  
  	  <th rowspan=2>Tamaño recomendado</th>
  </tr>
  <tr>
      <th style="text-align:center">Física</th>
      <th style="text-align:center">Lógica</th>
  </tr>
  <tr>
      <td><code>/boot</code> o <code>/efi</code></td>
      <td colspan=2 align="center"><code>/dev/sda1</code></td>
      <td><code>C12A7328-F81F-11D2-BA4B-00A0C93EC93B</code> : <a href="https://wiki.archlinux.org/title/EFI_system_partition" target="_blank">EFI system</a></td>
      <td><code>efh</code> : <a href="https://wiki.archlinux.org/title/EFI_system_partition" target="_blank">EFI system</a></td>
      <td></td>
      <td>Al menos <code>300MiB</code></td>
       </tr>
  <tr>
      <td><code>/</code></td>
      <td rowspan=4><br><br><code>/dev/sda2</code></td>
      <td><code>/dev/vg1/root</code></td>
      <td rowspan=4><code>E6D6D379-F507-44C2-A23C-238F2A3DF928</code> : <a href="https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)" target="_blank">Linux [LVM]</a></td>
      <td rowspan=4><br><code>8eh</code>: <a href="https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)" target="_blank">Linux [LVM]</a></td>
      <td rowspan=3></td>
      <td><code>30GiB</code> o más</td>
  </tr>
  <tr>
      <td><code>/var</code></td>
      <td><code>/dev/vg1/var</code></td>
      <td><code>15GiB</code></td>
  </tr>
      <tr>
      <td><code>/home</code></td>
      <td><code>/dev/vg1/home</code></td>
      <td>Resto del dispositivo</td>
  </tr>
</table>


### 7.3  UEFI en `gpt` con [`swap`](https://wiki.archlinux.org/title/Swap), incluyendo `/var`

Recomendado para usuarios que trabajan con correo local en dispositivos `hdd`.

<table>
  <tr>
      <th rowspan=2>Punto de montaje</th>
      <th colspan=2><p style="text-align:center">Partición</p></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs" target="_blank">GUID de tipo del partición</a></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/Partition_type" target="_blank">ID de tipo de partición</a></th>
      <th rowspan=2><a href="https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_entries_.28LBA_2.E2.80.9333.29" target="_blank">Atributos de la partición</a></th>  
  	  <th rowspan=2>Tamaño recomendado</th>
  </tr>
  <tr>
      <th style="text-align:center">Física</th>
      <th style="text-align:center">Lógica</th>
  </tr>
  <tr>
      <td><code>/boot</code> o <code>/efi</code></td>
      <td colspan=2 align="center"><code>/dev/sda1</code></td>
      <td><code>C12A7328-F81F-11D2-BA4B-00A0C93EC93B</code> : <a href="https://wiki.archlinux.org/title/EFI_system_partition" target="_blank">EFI system</a></td>
      <td><code>efh</code> : <a href="https://wiki.archlinux.org/title/EFI_system_partition" target="_blank">EFI system</a></td>
      <td></td>
      <td>Al menos <code>300MiB</code></td>
       </tr>
  <tr>
      <td><code>[SWAP]</code></td>
      <td rowspan=4 align="center"><br><code>/dev/sda2</code></td>
      <td><code>/dev/vg1/swap</code></td>
      <td rowspan=4><br><code>E6D6D379-F507-44C2-A23C-238F2A3DF928</code> : <a href="https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)" target="_blank">Linux [LVM]</a></td>
      <td rowspan=4><br><code>8eh</code>: <a href="https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)" target="_blank">Linux [LVM]</a></td>
      <td rowspan=4></td>
      <td>Más de<code>512MiB</code></td>
  </tr>
  <tr>
      <td><code>/</code></td>
      <td><code>/dev/vg1/root</code></td>
      <td><code>30GiB</code> o más</td>
  </tr>
  <tr>
      <td><code>/var</code></td>
      <td><code>/dev/vg1/var</code></td>
      <td><code>15GiB</code></td>
  </tr>
      <tr>
      <td><code>/home</code></td>
      <td><code>/dev/vg1/home</code></td>
      <td>Resto del dispositivo</td>
  </tr>
</table>






## 8   Gestionar disco con gdisk



### 8.1  [Tabla código de tipos de particiones comunes](https://wiki.archlinux.org/title/GPT_fdisk#Partition_type)

|                      Tipo de partición                       | Punto de montaje | Código de gdisk | [GUID de tipo de partición](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs) |
| :----------------------------------------------------------: | :--------------: | :-------------: | :----------------------------------------------------------: |
| [EFI system partition](https://wiki.archlinux.org/title/EFI_system_partition) |     `/boot`      |     `ef00`      |            `C12A7328-F81F-11D2-BA4B-00A0C93EC93B`            |
| [BIOS boot partition](https://wiki.archlinux.org/title/BIOS_boot_partition) |       None       |     `ef02`      |            `21686148-6449-6E6F-744E-656564454649`            |
| [Linux x86-64 root (/)](https://wiki.archlinux.org/title/Partitioning#/) |       `/`        |     `8304`      |            `4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709`            |
| [Linux swap](https://wiki.archlinux.org/title/Partitioning#Swap) |     `[SWAP]`     |     `8200`      |            `0657FD6D-A4AB-43C4-84E5-0933C84B4F4F`            |
| [Linux /home](https://wiki.archlinux.org/title/Partitioning#/home) |     `/home`      |     `8302`      |            `933AC7E1-2EB4-4F13-B844-0E14E2AEF915`            |
| [Linux /var](https://wiki.archlinux.org/title/Partitioning#/var) |     `/var`1      |     `8310`      |            `4D21B016-B534-45C2-A9FB-5C16E091FD2D`            |
| [Linux LVM](https://wiki.archlinux.org/title/Install_Arch_Linux_on_LVM#Create_partitions) |       Any        |     `8e00`      |            `E6D6D379-F507-44C2-A23C-238F2A3DF928`            |
| [Linux LUKS](https://wiki.archlinux.org/title/Dm-crypt/Drive_preparation#Physical_partitions) |       Any        |     `8309`      |            `CA7D7CCB-63ED-4C53-861C-1742536059CC`            |
| [Linux dm-crypt](https://wiki.archlinux.org/title/Dm-crypt/Drive_preparation#Physical_partitions) |       Any        |     `8308`      |            `7FFEC5C9-2D00-49B7-8941-3EA10A5586B7`            |



### 8.2  [Preparar el disco](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Preparing_the_disk)



#### 8.2.1  Modo interactivo

Entramos al modo interactivo de `gdisk`:

```shell
gdisk /dev/sda
.............................................................................................................................
GPT fdisk (gdisk) version 1.0.9

Partition table scan:
   MBR: protective
   BSD: not present
   APM: not present
   GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help): 
```



#### 8.2.2  Menú de ayuda

Imprimir menú de ayuda usando `?`:

```shell
Command (? for help): ?
b       back up GPT data to a file
c       change a partition's name
d       delete a partition
i       show detailed information on a partition
l       list known partition types
n       add a new partition
o       create a new empty GUID partition table (GPT)
p       print the partition table
q       quit without saving changes
r       recovery and transformation options (experts only)
s       sort partitions
t       change a partition's type code
v       verify disk
w       write table to disk and exit
x       extra functionality (experts only)
?       print this menu
Command (? for help):
```



#### 8.2.3 Crear nueva tabla de particiones vacía (Archlinux como único sistema)

> **Nota:** Si va a hacer *dual boot* con otro sistema operativo, omita éste procedimiento.

Si va a instalar Archlinux como único sistema, se recomienda crear una nueva tabla de partición vacía, usando la opción `o` y confirmamos con `Y`:

```shell
Command (? for help): o
This option deletes all partitions and creates a new protective MBR.
Proceed? (Y/N): Y
Command (? for help):
```



Alternativamente puede usar la herramienta `wipefs` para [borrar las firmas de sistemas de ficheros](https://wiki.archlinux.org/title/Device_file_(Espa%C3%B1ol)#wipefs), pero fuera de la herramienta *gdisk*:

```shell
wipefs --all /dev/sda
```

> 💡 **Advertencia:** 
>
> - Éste comando borra la tabla de particiones del disco.
> - Toda la información del disco será eliminada.
> - Si va a hacer dualboot con otro sistema operativo, omita éste comando.



#### 8.2.4  Gestionar particiones



##### 8.2.4.1  [Preparar partición de arranque](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Preparing_the_boot_partition_2) `efi`

Ésta es la partición donde se va a guardar el [cargador de arranque](https://wiki.archlinux.org/title/Boot_loader).

Tecleamos `n` para crear una nueva partición:

```shell
Command (? for help): n
```



Digitamos el número de partición, por defecto es `1` inicialmente:

```shell
Partition number (1-128, default 1): 1
```



<p>Presionamos <kbd>Enter</kbd> para elegir como el primer sector, el por defecto <code>2048</code>:</p>

```shell
First sector (34-976773134, default = 2048) or {KMGTP}:
```



Tipeamos el tamaño recomendado para la partición `efi`, antecedido del signo `+`:

```shell
Last sector (616448-976773134, default = 976773119) or {+-}size{KMGTP}: +300M
```



Si no sabemos de memoria el código hexadecimal del tipo de partición `efi`, podemos listarlos con `l`:

```shell
Current type is 8300 Linux filesystem)
Hex code or GUID (L to show codes, Enter = 8300): l
```



Escribimos el nombre de la etiqueta `efi`, para que nos arroje el código hexadecimal con el que coincide el tipo de partición,  es decir, `ef00`:

```shell
Type search string, or <Enter> to show all codes: efi
ef00 EFI system partition
```



Definimos el tipo de partición `ef00` para terminar de preparar la partición `efi`:

```shell
Hex code or GUID (L to show codes, Enter = 8300): ef00
Changed type of partition to 'EFI system partition'
```

Partición `efi` lista para escribirse.



##### 8.2.4.2  Preparar partición LVM

Ésta es la partición que va a contener las particiones lógicas `lvm`.

Tecleamos `n` para crear una nueva partición:

```shell
Command (? for help): n
```



<p>Digitamos el número de partición, o también podríamos simplemente presionar <kbd>Enter</kbd> para elegir la siguiente partición por defecto, que en éste caso es la <code>2</code>:</p>

```shell
Partition number (2-128, default 2): 2
```



<p>Presionamos <kbd>Enter</kbd> para elegir como primer sector de la partición, el que sigue por defecto <code>616448</code>:</p>

```shell
First sector (34-976773134, default = 616448) or {KMGTP}:
```



<p>Tipeamos el tamaño recomendado para la partición <code>LVM</code>, antecedido del signo <code>+</code>; o podríamos usar el resto del dispositivo simplemente presionando <kbd>Enter</kbd>. Usaremos para el ejemplo ésta última opción:</p>

```shell
Last sector (616448-976773134, default = 976773119) or {+-}size{KMGTP}:
```



Si no sabemos de memoria el código hexadecimal del tipo de partición `lvm`, podemos listarlos con `l`:

```shell
Current type is 8300 Linux filesystem)
Hex code or GUID (L to show codes, Enter = 8300): l
```



Escribimos el nombre de la etiqueta `lvm`, para que nos arroje el código hexadecimal con el que coincide el tipo de partición,  es decir, `8e00`:

```shell
Type search string, or <Enter> to show all codes: lvm
8e00 Linux LVM
```



Definimos el tipo de partición `8e00` para terminar de perparar la partición `lvm`:

```shell
Hex code or GUID (L to show codes, Enter = 8300): 8e00
Changed type of partition to 'Linux LVM'
```

Partición `efi` lista para escribirse.



##### 8.2.4.3  Escribir particiones `efi` y `lvm`

Escribimos los cambios con `w` y confirmamos con `Y`:

```shell
Command (? for help): w

Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING PARTITIONS!!

Do you want to proceed? (Y/N): Y
OK; writing new GUID partition table (GPT) to /dev/sda.
The operation has completed successfully.
```

En éste punto deberíamos tener una nueva tabla de particiones físicas creada.



##### 8.2.4.4  Listar particiones:

```shell
lsblk -p /dev/sda
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda      8:0    0 465,8G  0 disk
├─sda1   8:1    0   300M  0 part
├─sda2   8:2    0 465.5G  0 part
```







## 9  Gestionar volúmenes para [*dm-crypt*](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system) en modo [LVM en LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS)



### 9.1  Diseño de volúmenes lógicos de la partición LVM

```shell
+-------------------------------------------------------------------------------------------+ 
| Volúmen lógico 1     | Volúmen lógico 2     | Volúmen lógico 3     | Volúmen lógico 4     |
|                      |                      |                      |                      |
| [SWAP]               | /                    | /var                 | /home                |
|                      |                      |                      |                      |
| /dev/MyVolGroup/swap | /dev/MyVolGroup/root | /dev/MyVolGroup/var  | /dev/MyVolGroup/home |
|_ _ _ _ _ _ _ _ _ _ _ | _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ | _ _ _ _ _ _ _ _ _ _ _|
|                      (grupo de volúmenes)   /dev/MyVolGroup                               |
|_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|                     (contenedor LUKS)    /dev/mapper/cryptlvm                             |
|_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|                               Partición física cifrada LUKS2                              |
|                                      /dev/sda2                                            | 
+-------------------------------------------------------------------------------------------+
```



### 9.2  Gestionar contenedor encriptado LUKS

Creamos el contenedor encriptado LUKS en la partición `lvm`. Deberá presionar "YES", luego, asignar una contraseña y validarla:

```shell
cryptsetup luksFormat /dev/sda2

WARNING!
========
This will overwrite data on /dev/sda2 irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for /dev/sda:
Verify passphrase:
crypsetup liksFormat /dev/sda2 10.34s user 0.91s system 60% cpu 18.618 total
```

> **Nota**: Para más información sobre las opciones de cryptsetup, véase [LUKS encryption options](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode).



Abrimos el contenedor encriptado. Escriba la contraseña anterior:

```shell
cryptsetup open /dev/sda2 cryptlvm
Enter passphrase for /dev/sda2:
```

El contenedor descifrado ya está disponible en `/dev/mapper/cryptlvm`.



### 9.3  Alistar volúmenes



#### 9.3.1  Crear volumen físico

Cree un volumen físico encima del contenedor LUKS abierto:

```shell
pvcreate /dev/mapper/cryptlvm
```



#### 9.3.2  Crear grupo de volúmenes

Cree un grupo de volúmenes (en este ejemplo llamado `vglvm`) y agréguele el volumen físico creado anteriormente:

```shell
vgcreate vglvm /dev/mapper/cryptlvm
```



#### 9.3.3  Crear volúmenes lógicos

> **Sugerencia:** Si se formateará un volumen lógico con [ext4](https://wiki.archlinux.org/title/Ext4) , deje al menos 256 MiB de espacio libre en el grupo de volúmenes para permitir el uso de [e2scrub(8)](https://man.archlinux.org/man/e2scrub.8) .

Cree sus volúmenes lógicos `/swap`,`/root`, `/var` y `/home` en el grupo de volúmenes creado anteriormente. para éste ejemplo `vglvm`:

```shell
lvcreate -L 2G vglvm -n swap
lvcreate -L 40G vglvm -n root
lvcreate -L 15G vglvm -n var
lvcreate -l 100%FREE vglvm -n home
```

> **Nota:** Si es necesario también puede [eliminar](https://wiki.archlinux.org/title/LVM#Removing_a_logical_volume), [redimensionar](https://wiki.archlinux.org/title/LVM#Resizing_the_logical_volume_and_file_system_in_one_go) o [renombrar](https://wiki.archlinux.org/title/LVM#Renaming_a_logical_volume) volúmenes lógicos LVM.

#### 9.3.4  Listar volúmenes creados

```shell
lsblk /dev/sda
NAME               MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda                8:0    0 465,8G  0 disk
├─sda1             8:1    0   300M  0 part
├─sda2             8:2    0 465.5G  0 part
  ├─cryptlvm     254:1    0 465.5G  0 crypt
    ├─vglvm-swap 254:2    0     2G  0 lvm
    ├─vglvm-root 254:3    0    40G  0 lvm
    ├─vglvm-var  254:4    0    15G  0 lvm
    ├─vglvm-home 254:5    0 408.4G  0 lvm
```



#### 9.3.5  Formatear volúmenes

Establecer [sistema de ficheros](https://wiki.archlinux.org/title/File_systems_(Espa%C3%B1ol)#Tipos_de_sistemas_de_archivos) y [etiquetas](https://wiki.archlinux.org/title/Persistent_block_device_naming#by-label) a los volúmenes físicos y lógicos creados.



##### 9.3.5.1  Volúmenes lógicos:

```shell
mkswap -L swap /dev/vglvm/swap
mkfs.ext4 -F -L root /dev/vglvm/root
mkfs.ext4 -F -L var /dev/vglvm/var
mkfs.ext4 -F -L home /dev/vglvm/home
```



##### 9.3.5.2  [Partición de arranque EFI](https://wiki.archlinux.org/title/FAT_(Espa%C3%B1ol)#Creaci%C3%B3n_del_sistema_de_archivos):

```shell
mkfs.fat -F 32 -n EFI /dev/sda1
```



##### 9.3.5.3  Listar formato de volúmenes

```shell
lsblk -f /dev/sda
```



#### 9.3.6  Montar sistema de ficheros



##### 9.3.6.1  Montar volúmenes lógicos:

```shell
swapon /dev/vglvm/swap
mount /dev/vglvm/root /mnt/
mount --mkdir /dev/vglvm/var /mnt/var/
mount --mkdir /dev/vglvm/home /mnt/home/
```



##### 9.3.6.2  [Montar partición `efi` "*ESP*", en `/boot`](https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition):

```shell
mount --mkdir /dev/sda1 /mnt/boot/
```

> **Nota:**  `esp`indica el punto de montaje de [la partición EFI](https://wiki.archlinux.org/title/EFI_system_partition)  del sistema.



##### 9.3.6.3  Verificar puntos de montajes y particiones:

```shell
lsblk -f /dev/sda
```

```shell
ls /mnt
boot  home lost+found var
```



## 10  Configuración de pacman

Habilite/descomente [descargas paralelas](https://wiki.archlinux.org/title/Pacman#Enabling_parallel_downloads), el icono de pacman y el [color](https://man.archlinux.org/man/pacman.conf.5), en la sección `# misc options`:

```shell
vim /etc/pacman.conf
----------------------------------------------------------------------------------------------------------------------------
# Misc options
Color
ParallelDownloads = 5
ILoveCandy
```







## 11  Espejos de descarga con reflector

Obtenga en detalle:

- 20 espejos de descarga sincronizados en las últimas 12 horas.
- Con un tiempo de respuesta de no más de 35 segundos.
- Clasifíquelos por velocidad y sobrescriba el fichero  `/etc/pacman.d/mirrorlist `.
- Desde los espejos en países de los continentes América y Europa:



### 11.1  [América](https://telegra.ph/Personal-Archlinux-Installation-Guide-with-btrfs-AND-snapshots-05-17#Content)

Canadá 🇨🇦, Chile 🇨🇱, Brasil 🇧🇷, Ecuador 🇪🇨, Estados Unidos 🇺🇸 y Colombia 🇨🇴:

```
# curl -s ix.io/3V9q | bash
```



### 11.2   [Europa](https://telegra.ph/Personal-Archlinux-Installation-Guide-with-btrfs-AND-snapshots-05-17#Content)

Francia 🇫🇷, Alemania 🇩🇪, España 🇪🇸, Reino unido 🇬🇧, Italia 🇮🇹, Suiza 🇨🇭 y Hungría 🇭🇺:

```
# curl -s ix.io/3YXN | bash
```







## 12  Instalar paquetes esenciales

Use el script [pacstrap(8)](https://man.archlinux.org/man/pacstrap.8) para instalar los paquetes esenciales.



### 12.1  Instalación de paquetes para sistemas `UEFI`:

```shell
pacstrap /mnt base{,-devel} linux{-{lts,headers,firmware},} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim git os-prober arch-install-scripts zram-generator efibootmgr lvm2 cryptsetup
```



### 12.2  Instalación de paquetes para sistemas `BIOS`:

```shell
pacstrap /mnt base{,-devel} linux{-{lts,headers,firmware},} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim git os-prober arch-install-scripts zram-generator lvm2 cryptsetup
```







## 13  Configurar el sistema



### [Cambiar a la raíz](https://wiki.archlinux.org/title/Change_root) del nuevo sistema:

```shell
arch-chroot /mnt
```



### Establezca la [zona horaria](https://wiki.archlinux.org/title/Time_zone) para Colombia:

```shell
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
```



### Use [hwclock(8)](https://man.archlinux.org/man/hwclock.8) para configurar el reloj y generar`/etc/adjtime`:

```shell
hwclock --systohc
```



### Localización

[Edite](https://wiki.archlinux.org/title/Textedit) `/etc/locale.gen` y descomente (quite almohadilla `#`) según su país:

- `es_CO.UTF-8 UTF-8` para Español Colombia.
- `es_PE.UTF-8 UTF-8` para Español Perú.
- `es_AR.UTF-8 UTF-8` para Español Argentina.

#### Locales para Perú:

```shell
nvim /etc/locale.gen
----------------------------------------------------------------------------------------------------------------------------
es_PE.UTF-8 UTF-8
```



#### Generar los locales:

```shell
locale-gen
```



#### Variable LANG

[Cree](https://wiki.archlinux.org/title/Create) el fichero [locale.conf(5)](https://man.archlinux.org/man/locale.conf.5) y [configure la variable LANG](https://wiki.archlinux.org/title/Locale#Setting_the_system_locale) según corresponda:

```shell
nvim /etc/locale.conf
----------------------------------------------------------------------------------------------------------------------------
LANG=en_PE.UTF-8
```



#### Fuente de terminal

Si [configura el diseño del teclado de la consola](https://wiki.archlinux.org/title/installation_guide#Set_the_console_keyboard_layout), haga que los cambios sean persistentes en [vconsole.conf(5)](https://man.archlinux.org/man/vconsole.conf.5):

```shell
nvim /etc/vconsole.conf
----------------------------------------------------------------------------------------------------------------------------
KEYMAP=la-latin1
FONT=Lat2-Terminus16
```







### Configuración de la red



#### Hostname

[Cree](https://wiki.archlinux.org/title/Create) el fichero de nombre del [host](https://wiki.archlinux.org/title/Hostname) (`grupo2` para éste ejemplo):

```shell
nvim /etc/hostname
----------------------------------------------------------------------------------------------------------------------------
grupo2
```



#### Dominio completo

Establecer sistema con un [nombre de dominio completo](https://en.wikipedia.org/wiki/Fully_qualified_domain_name):

```shell
nvim /etc/hosts
----------------------------------------------------------------------------------------------------------------------------
127.0.0.1                      localhost
::1                            localhost
127.0.1.1   grupo2.localdomain   grupo2
```







### Initramfs

Carga varios módulos del kernel y configura las cosas necesarias antes de entregar el control a `init`.



#### Módulos

Los módulos del kernel se cargarán antes de que se ejecuten los *hooks* de arranque.

| Módulos | Descripción                                                  |
| :-----: | :----------------------------------------------------------- |
| `zram`  | Habilita la paginación comprimida en la memoria RAM, hasta que sea necesaria la utilización del espacio compartido `swap`, si existe. |



#### Hooks

Los hooks son scripts que se ejecutan en el ramdisk inicial.

|   Hooks    | Descripción                                                  |
| :--------: | :----------------------------------------------------------- |
|   `udev`   | Inicia el demonio `udev` y procesa `uevents` desde el kernel. |
| `keyboard` | Añade los módulos necesarios para los dispositivos de teclado. Úselo si tiene un teclado USB o serial y lo necesita en un espacio de usuario temprano. |
|  `keymap`  | Agrega los [keymap(s)](https://wiki.archlinux.org/title/Linux_console/Keyboard_configuration#Persistent_configuration) especificados en `/etc/vconsole.conf` a `initramfs`. Si usa el [cifrado del sistema](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system) , especialmente el cifrado de disco completo, asegúrese de agregarlo antes del hook `encrypt`. |
| `encrypt`  | Agrega el módulo kernel `dm_crypt` y la herramienta `cryptsetup` a la imagen. Debe tener instalado [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) para usar ésto. Asegúrese de agregarlo después del hook `keymap`. |
|   `lvm2`   | Agrega el módulo del kernel del mapeador de dispositivos y la herramienta `lvm` a la imagen. Debe tener [lvm2](https://archlinux.org/packages/?name=lvm2) instalado para usar ésto. |
|  `resume`  | Habilitar la hibernación. [Si usa `lvm` debe estar luego `lvm2`](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Configure_the_initramfs), de lo contrario, con [estar luego del hook `udev` sería suficiente](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Configure_the_initramfs). |



#### Agregar módulos y [hooks](https://wiki.archlinux.org/title/mkinitcpio#Common_hooks) a [mkinitcpio.conf](https://man.archlinux.org/man/mkinitcpio.conf.5)

Asegúrese de tener [instalado](https://wiki.archlinux.org/title/Install) el paquete [lvm2](https://archlinux.org/packages/?name=lvm2), verifique que tenga los *hooks* `udev`, `keyboard`y `keymap`, y luego agregue los *hooks* faltantes `encrypt`, `lvm2` y [`resume`](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Configure_the_initramfs) a [mkinitcpio.conf](https://wiki.archlinux.org/title/Mkinitcpio.conf).

```shell
nvim /etc/mkinitcpio.conf
----------------------------------------------------------------------------------------------------------------------------
MODULES=(zram)
...
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 resume filesystems fsck)
...
```



#### [Regenerar imagen initramfs](https://wiki.archlinux.org/title/Mkinitcpio#Manual_generation)

[Generar manualmente todos los presets](https://wiki.archlinux.org/title/Mkinitcpio#Manual_generation):

```shell
mkinitcpio -P
```







### Cargador de arranque [Grub](https://wiki.archlinux.org/title/GRUB)

[Edite manualmente](https://wiki.archlinux.org/title/GRUB#Generated_grub.cfg) `/etc/default/grub`, [eliminando, agregando, o descomentando entradas y argumentos de configuración](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Multiple_entries) para el menú de [*grub*](https://wiki.archlinux.org/title/GRUB).

> **Nota**:  `esp`indica el punto de montaje de [la partición EFI](https://wiki.archlinux.org/title/EFI_system_partition)  del sistema.



#### Instalación

Instalar [*grub*](https://wiki.archlinux.org/title/GRUB) en el [punto de montaje típico](https://wiki.archlinux.org/title/EFI_system_partition#Typical_mount_points) `/boot`, [el método más directo](https://wiki.archlinux.org/title/EFI_system_partition#Mount_the_partition). 

##### [UEFI/GPT](https://wiki.archlinux.org/title/GRUB#Installation)

```shell
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```



##### [GRUB/MBR](https://wiki.archlinux.org/title/GRUB#Installation_2)

```shell
grub-install --target=i386-pc /dev/sda
```



#### [Configuración](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_the_boot_loader_2)

El fichero de configuración general de [*grub*](https://wiki.archlinux.org/title/GRUB) es `/etc/default/grub`.

```shell
nvim /etc/default/grub
```



##### Variable GRUB_CMDLINE_LINUX_DEFAULT

Para [mostrar los mensajes del kernel durante el arranque](https://wiki.archlinux.org/title/GRUB#Additional_arguments) elimine el [argumento del kernel](https://wiki.archlinux.org/title/GRUB#Additional_arguments) `quiet` de la variable `GRUB_CMDLINE_LINUX_DEFAULT`.





 y <a href="https://wiki.archlinux.org/title/GRUB#Additional_arguments" target="_blank">habilitar hibernación en dispositivo <samp>swap</samp>:</a>

```shell
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume=/dev/vglvm/swap"
```



##### Variable GRUB_CMDLINE_LINUX

<p>Agregar <a href="https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_the_boot_loader_2" target="_blank">dispositivo encriptado</a> y <a href="https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_the_boot_loader_2" target="_blank"><samp>root</samp></a>.</p>


```shell
GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:cryptlvm root=/dev/vglvm/root"
```



##### [Detectar otros sistemas operativos](https://wiki.archlinux.org/title/GRUB#Detecting_other_operating_systems)

Agregue o descomente la línea `GRUB_DISABLE_OS_PROBER=false` para mostrar otros sistemas operativos en la entrada de *grub*:

```shell
GRUB_DISABLE_OS_PROBER=false
```

> **Tip:** Otras posibles entradas:
>
> 1. [Deshabilitar submenú](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Disable_submenu).
> 2. [Recuperar entrada anterior](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Disable_submenu)



##### Generar fichero de configuración de `grub`

[Utilice la herramienta *grub-mkconfig* para generar](https://wiki.archlinux.org/title/GRUB#Generate_the_main_configuration_file) `/boot/grub/grub.cfg`:

```shell
grub-mkconfig -o /boot/grub/grub.cfg
```

> **Tip:** Después de instalar o eliminar un [kernel](https://wiki.archlinux.org/title/Kernel) , solo necesita volver a ejecutar el comando *grub-mkconfig* anterior .



### [Usuarios y grupos](https://wiki.archlinux.org/title/users_and_groups)



<h4>Usuario <samp>root</samp></h4>

<p>Establezca la contraseña del usuario <samp>root</samp></p>

```shell
passwd
```



<h4>Nuevo usuario</h4>

<p><a href="https://wiki.archlinux.org/title/Users_and_groups#User_management" target="_target">Añadir un nuevo usuario</a> al <a href="https://wiki.archlinux.org/title/Users_and_groups#User_groups" target="_blank" >grupo de usuarios</a> <samp>wheel</samp>.</p>

```shell
useradd -mG wheel -s /bin/bash rv
```

<blockquote>
<b>Tips:</b> Otros grupos.
	<ul>
		<li><a href="https://wiki.archlinux.org/title/Systemd" target="_blank"> Pre-systemd groups:</a> Grupos antes de <a href="https://wiki.archlinux.org/title/Systemd" target="_blank" >Systemd</a>.</li>
        <li><a href="https://wiki.archlinux.org/title/Users_and_groups#System_groups" target="">System group:</a> Grupos del sistema.</li>
	</ul>
</blockquote>




<p>Establecer <a href="https://wiki.archlinux.org/title/Password" target="_blank">contraseña</a> para el nuevo usuario:</p>

```shell
passwd rv
```



<h5>Permisos <a href="https://wiki.archlinux.org/title/sudo#Example_entries" target="_blank">sudo</a> al nuevo usuario</h5>

<p>Permitir que los usuarios del grupo <samp>wheel</samp> obtengan permisos de administrador, usando <a href="https://wiki.archlinux.org/title/sudo#Using_visudo" target="_blank">visudo</a> y <a hre"https://wiki.archlinux.org/title/Neovim" target="_blank">neovim</a>. Descomente la línea <code>%wheel      ALL=(ALL:ALL) ALL</code> (quite almohadilla <code>#</code>):</p>

```shell
EDITOR=nvim visudo
----------------------------------------------------------------------------------------------------------------------------
...
%wheel      ALL=(ALL:ALL) ALL
...
```



### Configuración de pacman

Habilite/descomente las [descargas paralelas](https://wiki.archlinux.org/title/Pacman#Enabling_parallel_downloads), el [repositorio multilib](https://wiki.archlinux.org/title/official_repositories#Enabling_multilib),  el icono de pacman, [color](https://man.archlinux.org/man/pacman.conf.5)  y la línea [verbosePkgList](https://wiki.archlinux.org/title/pacman#Comparing_versions_before_updating) en la sección `# misc options`:

```shell
nvim /etc/pacman.conf
----------------------------------------------------------------------------------------------------------------------------
# Misc options
Color
VerbosePkgLists
ParallelDownloads = 3
ILoveCandy

[multilib]
Include = /etc/pacman.d/mirrorlist
```



### Actualizaciones de microcódigo

Habilitar las [actualizaciones de microcódigo](https://wiki.archlinux.org/title/Microcode)

#### AMD

```shell
pacman -Sy amd-ucode --noconfirm
```



#### Intel

```shell
pacman -Sy intel-ucode --noconfirm
```



### Paquetes adicionales

Paquetes adicionales para fuentes y wifi:

```shell
pacman -S iwd reflector rsync ttf-{inconsolata,dejavu,hack,roboto,liberation} usbutils openresolv --needed --noconfirm
```



### Administradores de red

#### [Iwd](https://wiki.archlinux.org/title/Iwd)

Administrar redes y adaptadores inalámbricos.



##### Fichero de configuración

Cree el directorio de configuración:

```shell
mkdir -p /etc/iwd/
```



###### Servidores DNS y DHCP

[Seleccione el administrador de DNS](https://wiki.archlinux.org/title/Iwd#Select_DNS_manager) resolv.conf y [configure las rutas usando el cliente DHCP interno](https://wiki.archlinux.org/title/Iwd#Enable_built-in_network_configuration) en el fichero de configuración:

```shell
nvim /etc/iwd/main.conf
----------------------------------------------------------------------------------------------------------------------------
[Network]
NameResolvingService=resolvconf

[General]
EnableNetworkConfiguration=true
```



##### Habilitar la unidad:

```shell
systemctl enable iwd.service
```





#### Systemd-networkd



##### [Configuración cableada con IP estática](https://wiki.archlinux.org/title/Systemd-networkd#Wired_adapter_using_a_static_IP):

```shell
nvim /etc/systemd/network/20-wired.network
----------------------------------------------------------------------------------------------------------------------------
[Match]
Name=enp*

[Network]
DHCP=no
Address=192.168.1.2/24
Gateway=192.168.1.1
```



##### [Configuración inalámbrica con IP dinámica](https://wiki.archlinux.org/title/Systemd-networkd#Wireless_adapter):

```shell
nvim /etc/systemd/network/25-wireless.network
----------------------------------------------------------------------------------------------------------------------------
[Match]
Name=wl*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```



##### Habilitar unidad:

```shell 
 systemctl enable systemd-networkd.service
```



### Habilitar zram

[Deshabilite zswap](https://wiki.archlinux.org/title/Zswap#Toggling_zswap), [habilite TCP Fast Open](https://wiki.archlinux.org/title/sysctl#Enable_TCP_Fast_Open) y decrezca el parámetro de caché [Virtual File System](https://wiki.archlinux.org/title/sysctl#VFS_cache) para mejorar la respuesta del sistema:

```shell
nvim /etc/sysctl.d/99-sysctl.conf
----------------------------------------------------------------------------------------------------------------------------
zswap.enabled=0
net.ipv4.tcp_fastopen=3
vm.vfs_cache_pressure=50
```



Crear fichero de configuración:

```shell
curl ix.io/48cE -so /etc/systemd/zram-generator.conf
```



Genere el fichero [/fstab](/fstab) definido por [UUID](https://wiki.archlinux.org/title/UUID) con la herramienta [genfstab](https://man.archlinux.org/man/genfstab.8)

```shell
genfstab -U / >> /etc/fstab
```



### Desmontar particiones y reiniciar sistema

Salir del entorno chroot:

```shell
exit
```



Desmontar swap:

```shell
swapoff -a
```



Desmontar resto de puntos de montaje

```shell
umount -R /mnt
```



Reiniciar sistema

```shell
reboot
```



### Openresolv

Establecer los servidores de DNS de clouflare y Google con /etc/resolv.conf:

```shell
nvim /etc/resolv.conf
--------------------------------------------------------------------------------------------------------------------------
...
nameserver 1.1.1.1
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.0.0.1
```



Convertir en inmutable el fichero de configuración [/etc/resolv.conf](https://wiki.archlinux.org/title/Openresolv#Installation):

```shell
chattr +i /etc/resolv.conf
```



### Wifi con `iwd`

Entrar al [modo interactivo.](https://wiki.archlinux.org/title/Iwd#iwctl)

```shell
iwctl
```



#### Listar dispositivos

Verificar el estado de las interfaces inalámbricas:

```shell
[iwd]# device list
                        Devices
-----------------------------------------------------------------------
 Name         Address             Powered  Adapter  Mode
-----------------------------------------------------------------------
 wlan0        80:a5:89:d7:6c:3d   on       phy0     station
```

>💡 En este caso el nombre de la interfaz es `wlan0`.



#### Escanear redes disponibles

**Iniciar escaneo:**

```shell
[iwd]# station wlan0 scan
```

> **Tips:**
>
> - El nombre de la interfaz es `wlan0`.
>
> - Éste comando no tiene salida 🤐.

**Listar redes disponibles:**

```shell
[iwd]# station wlan0 get-networks
                               Available networks                         *
----------------------------------------------------------------------------
      Network name                      Security            Signal
----------------------------------------------------------------------------
      Memoria                           psk                 ****    
      moto e(7) plus 2011               psk                 ****    
      Adalberto                         psk                 **** 
```

> **Nota:** La red elegida es `Memoria`.



####  Conectarse a una red inalámbrica

Conectarse a la red visble `Memoria`:

```shell
[iwd]# station wlan0 connect 'Memoria'
Passphrase:******
```



Conectarse a la red oculta `Telnet`:

```shell
[iwd]# station wlan0 connect-hidden 'Telnet'
Passphrase:******
```



####  Verificar si estamos conectados:

```shell
[iwd]# station list
                            Devices in Station Mode                            
----------------------------------------------------------------------------
  Name                  State            Scanning
----------------------------------------------------------------------------
  wlan0                 connected  
```


#### Salir del modo interactivo:

```shell
[iwd]# exit
```



### Verificar conexión de internet

[Comprobamos la salida a Internet](https://wiki.archlinux.org/title/Network_configuration#Ping):

```shell
ping -c3 archlinux.org
```



### Espejos de descarga con reflector

Obtenga en detalle:

- 20 espejos de descarga sincronizados en las últimas 12 horas.
- Con un tiempo de respuesta de no más de 35 segundos.
- Clasifíquelos por velocidad y sobrescriba el fichero  `/etc/pacman.d/mirrorlist `.
- Desde los espejos en países de los continentes América y Europa:

#### [América](https://telegra.ph/Personal-Archlinux-Installation-Guide-with-btrfs-AND-snapshots-05-17#Content)

Canadá 🇨🇦, Chile 🇨🇱, Brasil 🇧🇷, Ecuador 🇪🇨, Estados Unidos 🇺🇸 y Colombia 🇨🇴:

```
# curl -s ix.io/3V9q | bash
```



#### [Europa](https://telegra.ph/Personal-Archlinux-Installation-Guide-with-btrfs-AND-snapshots-05-17#Content)

Francia 🇫🇷, Alemania 🇩🇪, España 🇪🇸, Reino unido 🇬🇧, Italia 🇮🇹, Suiza 🇨🇭 y Hungría 🇭🇺:

```
# curl -s ix.io/3YXN | bash
```



### Configuraciones del usuario



#### Iniciar sesión

Inicie sesión con su usuario:

```shell
Arch Linux 6.0.11-arch1-1 (tty1)
grupo2 login: rv
Password:
[rv@grupo2 ~]$ 
```



#### Administrar directorios conocidos del usuario

Instale el paquete [xdg-user-dirs](https://wiki.archlinux.org/title/XDG_user_directories#Installation):

```shell
$ sudo pacman -S xdg-user-dirs
```



Se recomienda [generar los directorios del usuario en idioma inglés](https://wiki.archlinux.org/title/XDG_user_directories#Creating_default_directories):

```shell
$ LC_ALL=C xdg-user-dirs-update --force
```

> **Tip:** [Directorios conocidos del usuario en el idioma del sistema (es_PE-UTF-8)](https://wiki.archlinux.org/title/XDG_user_directories#Creating_default_directories).



## Postinstalación



### [Ayudador de AUR](https://wiki.archlinux.org/title/AUR_helpers#Pacman_wrappers)

Instalar [ayudador de AUR](https://wiki.archlinux.org/title/AUR_helpers#Pacman_wrappers) [`yay`](https://aur.archlinux.org/packages/yay)o [`paru`](https://aur.archlinux.org/packages/paru)



#### [Yay](https://aur.archlinux.org/packages/yay)

```shell
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```



#### [Paru](https://aur.archlinux.org/packages/paru)

```shell
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```





### Servidor Xorg y de sesión

Instalar Xorg:

```shell
pacman -S xorg-server lightdm-gtk-greeter 
```



Habilitar servicio de sesiones

```shell
systemctl enable lightdm.service
```



#### Controlador de video



##### AMD

Si el procesador es AMD puede configurar el driver de video siguiendo la [wiki](https://wiki.archlinux.org/title/Xorg#AMD), o si además prefiere agregar la aceleración de video por hardware siga ésta completa y [recomendable guía](https://telegra.ph/Hardware-video-acceleration-07-13).



##### Intel

Si el procesador es Intel puede configurar el driver siguiendo la [wiki](https://wiki.archlinux.org/title/Intel_graphics#Installation), la cual recomienda el [driver de controlador de modos](https://man.archlinux.org/man/modesetting.4), sobre el convencional [xf86-video-intel](https://archlinux.org/packages/?name=xf86-video-intel).



### Administrador de ventanas I3 y aplicaciones del usuario



Instalar [i3-wm](https://archlinux.org/packages/?name=i3-wm) y otros paquetes:

```shell
pacman -S i3-wm i3status chromium alacritty htop mpv neofetch telegram-desktop thunar cmus
```











### Bloques de código:

<pre><code class="lang-shell"># lsblk
output
</code></pre>





### Variables:

<var>h</var>

