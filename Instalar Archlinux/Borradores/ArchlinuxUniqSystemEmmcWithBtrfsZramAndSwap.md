```shell
setfont ter-i22b
iwctl
  station wlan0 connect Memoria
ping -c3 archlinux.org
timedatectl set-ntp true
lsblk -d
lsblk -pso NAME,ROTA /dev/mmcblk1
parted /dev/mmcblk1 print | grep -E "Model|abl|Dis"
ls /sys/firmware/efi/efivars
gdisk /dev/mmcblk1
   o
   Y
   p
   n
   Enter
   Enter
   +300M
   ef00
   n
   Enter
   Enter
   +1G
   8200
   n
   Enter
   Enter
   Enter
   8304
   w
   Y
lsblk -p /dev/mmcblk1
mkfs.fat -F 32 -n EFI /dev/mmcblk1p1
mkswap -L swap /dev/mmcblk1p2
mkfs.btrfs -f -L root -R free-space-tree -n 32k /dev/mmcblk1p3
lsblk -po NAME,FSTYPE,LABEL,MOUNTPOINT,SIZE /dev/mmcblk1
mount /dev/mmcblk1p3 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var_log
btrfs subvolume create /mnt/@var_tmp
btrfs subvolume create /mnt/@snapshots
btrfs subvolume list -p /mnt
umount /mnt
swapon /dev/mmcblk1p2
mount -o noatime,compress=zstd,discard=async,subvol=@ /dev/mmcblk1p3 /mnt
mount --mkdir -o noatime,compress=zstd,discard=async,subvol=@home /dev/mmcblk1p3 /mnt/home
mount --mkdir -o noatime,compress=zstd,discard=async,subvol=@var_log /dev/mmcblk1p3 /mnt/var/log
mount --mkdir -o noatime,compress=zstd,discard=async,subvol=@var_tmp /dev/mmcblk1p3 /mnt/var/tmp
mount --mkdir -o noatime,compress=zstd,discard=async,subvol=@snapshots /dev/mmcblk1p3 /mnt/.snapshots
lsblk -p /dev/mmcblk1
mount --mkdir /dev/mmcblk1p1 /mnt/boot
ls /mnt
rm /etc/pacman.d/mirrorlist && curl http://ix.io/4l2b -so /etc/pacman.d/mirrorlist
pacman -Sy archlinux-keyring
pacstrap /mnt base{,-devel} linux{-{lts,headers,firmware},} grub ntfs-3g gvfs-{mtp,afc,gphoto2} neovim git os-prober terminus-font arch-install-scripts zram-generator efibootmgr
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock --systohc
echo 'es_CO.UTF-8 UTF-8' /etc/locale.gen
locale-gen
echo 'LANG=es_CO.UTF-8' nvim /etc/locale.conf
echo 'FONT=ter-i20n' /etc/vconsole.conf
echo chromebook > /etc/hostname
nvim /etc/hosts
----------------------------------------------------------------
127.0.0.1				localhost
::1					localhost
127.0.1.1	chromebook.localdomain	chromebook
```

Eliminar hook `fsck` y añadir hook `resume`:

```shell
nvim /etc/mkinitcpio.conf
----------------------------------------------------------------
MODULES=(btrfs zram)
...
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems resume)
```

```shell
mkinitcpio -P
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```

Eliminar parámetro del kernel `quiet` y añadir `resume` igualado al dispositivo `swap`:

```shell
nvim /etc/default/grub
----------------------------------------------------------------
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume=/dev/mmcblk1p2 nomodeset"
```

```shell
grub-mkconfig -o /boot/grub/grub.cfg
passwd
useradd -mG wheel,audio,video,power,lp,power -s /bin/bash dakataca
passwd dakataca
```

```shell
EDITOR=nvim visudo
----------------------------------------------------------------
%wheel      ALL=(ALL:ALL) ALL
```

```shell
nvim /etc/pacman.conf
----------------------------------------------------------------
# Misc options
Color
VerbosePkgLists
ParallelDownloads = 3
ILoveCandy

[multilib]
Include = /etc/pacman.d/mirrorlist
```



```shell
pacman -Sy amd-ucode iwd grub-btrfs snapper reflector rsync ttf-{inconsolata,dejavu,hack,roboto,liberation} usbutils openresolv lib32-pipewire pipewire{-{alsa,pulse,jack},} wireplumber mpv alsa-util xf86-video-fbdev htop neofetch telegram-desktop chromium scrot cmus tmux thunar acpi xorg-{server,xinit} qtile --needed --noconfirm
mkdir -p /etc/iwd/
```

```shell
nvim /etc/iwd/main.conf
----------------------------------------------------------------
[Network]
NameResolvingService=resolvconf

[General]
EnableNetworkConfiguration=true
```



```shell
nvim /etc/systemd/network/20-wired.network
----------------------------------------------------------------
[Match]
Name=enp*

[Network]
DHCP=no
Address=192.168.1.2/24
Gateway=192.168.1.1
```



```shell
systemctl enable {iwd,systemd-networkd}.service
systemctl enable {fstrim,reflector}.timer
```



```shell
nvim /etc/sysctl.d/99-sysctl.conf
----------------------------------------------------------------
zswap.enabled=0
net.ipv4.tcp_fastopen=3
vm.vfs_cache_pressure=50
```



```shell
curl ix.io/48cE -so /etc/systemd/zram-generator.conf
genfstab -U / >> /etc/fstab
exit
swapoff -a
umount -R /mnt
reboot
```



Iniciar sesión con usuario `root`:

```shell
root
nvim /etc/resolv.conf
----------------------------------------------------------------
# Resolver configuration file.
# See resolv.conf(5) for details.
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
nameserver 1.0.0.1
```



```shell
chattr +i /etc/resolv.conf
iwctl
  station wlan0 connect Memoria
ping -c3 archlinux.org
umount /.snapshots
rm -r /.snapshots
snapper -c config create-config /
```



```shell
nvim /etc/snapper/configs/config
----------------------------------------------------------------
TIMELINE_MIN_AGE="1800"
TIMELINE_LIMIT_HOURLY="5"
TIMELINE_LIMIT_DAILY="7"
TIMELINE_LIMIT_WEEKLY="0"
TIMELINE_LIMIT_MONTHLY="0"
TIMELINE_LIMIT_YEARLY="0"
```



```shell
sudo systemctl enable snapper-{timeline,cleanup}.timer
sudo systemctl enable --now grub-btrfsd.service
sudo sh -c 'rm /etc/xdg/reflector/reflector.conf && curl http://ix.io/4jlI -so /etc/xdg/reflector/reflector.conf'
sudo systemctl enable reflector.timer
```



Driver de video **AMD**:

```shell
sudo pacman -S {lib32-,}mesa{,-vdpau} libva-{mesa-driver,utils} vdpauinfo radeontop --needed
```



Touchpad xorg y config de alacritty

```shell
sudo sh -c 'curl ix.io/4jhZ  -so /etc/X11/xorg.conf.d/30-touchpad.conf'
curl ix.io/4ji0 --create-dirs -so ~/.config/alacritty/alacritty.yml
```



Variables de entorno

```shell
nvim /etc/environment
----------------------------------------------------------------
VISUAL=nvim
LC_ALL=es_CO.UTF-8
XDG_CONFIG_HOME   DEFAULT=@{HOME}/.config
```



Administración de energía

```shell
sudo sed -i.back "a\HandlePowerKey=ignore\nHandleSuspendKey=ignore\nHandleHibernateKey=ignore" /etc/systemd/logind.conf
```



Habilitar multihilo  en `makepkg.conf`:

```shell
sudo sed -i.back "/MAKEFLAGS=/ s/#\| //g ; /MAKEFLAGS=/ s/[0-9]\+/\$\(nproc\)/ ; /^COMPRESSGZ/ s/gzip/pigz/ ; /^COMPRESSBZ2/ s/(bzip2/(pbzip2/ ; /^COMPRESSXZ\|^COMPRESSZST/ s/-)/--threads=0 -)/"  /etc/makepkg.conf
```



[Teclado](https://man.archlinux.org/man/xkeyboard-config.7) Latino, español y EEUU con tilde muerta.

```shell
localectl set-x11-keymap latam,es,us chromebook deadtilde,deadtilde,intl grp:alt_shift_toggle
```



Tmux config

```shell
nvim ~/.tmux.conf
----------------------------------------------------------------
'set -g mouse on'
# set alacritty terminal colors 24bits and 256colors.
'set -g default-terminal "tmux-256color"'
'set -ga terminal-overrides ", xterm-alacritty: Tc"'
# set first window to index 1 (not 0) to map more to the keyboard layout
'set -g base-index 1'
'set -g pane-base-index 1'
# Rearrange windows.
'bind-key -n S-Left swap-window -t -1 \; select-window -t -1'
'bind-key -n S-Right swap-window -t +1 \; select-window -t +1'
'bind r source-file ${HOME}/.tmux.conf \; display-message "source-file reloaded"'
```



Git config

```shell
git config --global user.name dakataca
git config --global user.email danieldakataca@gmail.com
git config --global core.autocrlf input
# nvim git config, solved error vimdiff use vim.
git config --global core.editor nvim
git config --global diff.tool vimdiff3
git config --global difftool.vimdiff3.path nvim
git config --global merge.tool vimdiff3
git config --global mergetool.vimdiff3.path nvim
```



Iniciar sesión con usuario `dakataca`:

```shell
exit
dakataca
sudo pacman -S xdg-user-dirs
LC_ALL=C xdg-user-dirs-update --force
```



Git config

```shell
git config --global user.name dakataca
git config --global user.email danieldakataca@gmail.com
git config --global core.autocrlf input
# nvim git config, solved error vimdiff use vim.
git config --global core.editor nvim
git config --global diff.tool vimdiff3
git config --global difftool.vimdiff3.path nvim
git config --global merge.tool vimdiff3
git config --global mergetool.vimdiff3.path nvim
```



Configuraciones de Qtile WM:

- Comentar líneas innecesarias de `~/.xinitrc`.
- Insertar línea de ejecución de `qtile`.
- Copiar fichero de configuración.
- Validar fichero de configuración.

```shell
sed -E "/xclock|(exec )?xterm|twm/s/^/#/g" /etc/X11/xinit/xinitrc > ~/.xinitrc
echo 'exec qtile start' >> ~/.xinitrc
install -D /usr/share/doc/qtile/default_config.py ~/.config/qtile/config.py
python -m py_compile ~/.config/qtile/config.py
```

