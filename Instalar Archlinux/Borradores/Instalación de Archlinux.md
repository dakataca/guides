# Instalación de Archlinux





[TOC]





## 1  Distribución de Teclado

[Por defecto es](https://wiki.archlinux.org/title/Installation_guide#Set_the_console_keyboard_layout) `en`.



### 1.1  Latina

```bash
# loadkeys la-latin1
```



### 1.2  Español

```bash
# loadkeys es
```





## 2  Fuente de Terminal

Tamaño y tipo de [fuente de la terminal](https://man.archlinux.org/man/setfont.8).



### 2.1  Listar fuentes

```bash
# ls /usr/share/kbd/consolefonts/
```

[Biblioteca de imágenes de fuentes disponibles para consola.](https://adeverteuil.github.io/linux-console-fonts-screenshots/#screenshots)



### 2.2  Fuente latina

```bash
# setfont Lat2-Terminus16
```







## 3  Comprobar modo de arranque

```bash
# ls /sys/firmware/efi/efivars
```

Si el directorio existe, es compatible con el modo de arranque **_UEFI_**, de lo contrario, lo es solo con **_Legacy_**.





## 4  Conectarse a Internet



### 4.1  Vía LAN

Comprobar si las [interfaces de red](https://wiki.archlinux.org/title/Network_interface_(Espa%C3%B1ol)) están listas y activas.

```bash
# ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp3s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 9c:b6:54:47:a1:fe brd ff:ff:ff:ff:ff:ff
4: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DORMANT group default qlen 1000
    link/ether a4:db:30:15:6c:5a brd ff:ff:ff:ff:ff:ff
```

> 💡 **Tenga en cuenta:**
>
> -  El `UP` en `<BROADCAST,MULTICAST,UP,LOWER_UP>` indica que la interfaz está activa.
> - `lo` es la [interfaz virtual de bucle invertido](https://en.wikipedia.org/wiki/Loopback#irtual_loopback_interface) y no se utiliza para realizar conexiones de red.

### 4.2  Vía wifi con `iwd`



#### 4.2.1  Entrar al [modo interactivo.](https://wiki.archlinux.org/title/Iwd#iwctl)

```bash
# iwctl
```





#### 4.2.2  Listar dispositivos

Verificar el estado de las interfaces inalámbricas:

```shell
[iwd]# device list
                        Devices
-----------------------------------------------------------------------
 Name         Address             Powered  Adapter  Mode
-----------------------------------------------------------------------
 wlan0        80:a5:89:d7:6c:3d   on       phy0     station
```

>💡 En este caso el nombre de la interfaz es `wlan0`.



##### 4.2.2.1  Interfaz de red no aparece

Si la interface inalámbrica no se encuentra, intente manualmente: 

* [Cargar el módulo del driver](https://wiki.archlinux.org/title/Kernel_module#Manual_module_handling).
* [Levantar la interface de red](https://wiki.archlinux.org/title/Kernel_module#Manual_module_handling).



#### 4.2.3 Escanear redes disponibles

**Iniciar escaneo:**

```shell
[iwd]# station wlan0 scan
```

> 💡 **Tenga en cuenta:**
>
> - El nombre de la interfaz es `wlan0`.
>
> - Éste comando no tiene salida 🤐.

**Listar redes disponibles:**

```shell
[iwd]# station wlan0 get-networks
                               Available networks                         *
----------------------------------------------------------------------------
      Network name                      Security            Signal
----------------------------------------------------------------------------
      Memoria                           psk                 ****    
      moto e(7) plus 2011               psk                 ****    
      Adalberto                         psk                 **** 
```

> 💡 La red elegida es `Memoria`.



#### 4.2.4  Conectarse a una red inalámbrica

Conectarse a la red `Memoria`:

```shell
[iwd]# station wlan0 connect 'Memoria'
Passphrase:******
```



#### 4.2.5  Verificar si estamos conectados:

```shell
[iwd]# station list
                            Devices in Station Mode                            
----------------------------------------------------------------------------
  Name                  State            Scanning
----------------------------------------------------------------------------
  wlan0                 connected  
```


#### 4.2.6  Salir del modo interactivo:

```shell
[iwd]# exit
```



### 4.3  Verificar conexión de internet

[Comprobamos la salida a Internet](https://wiki.archlinux.org/title/Network_configuration#Ping):

```bash
# ping -c3 archlinux.org
```



## 5  Fecha y hora

[Habilite e inicie el servicio de sincronización de fecha y hora](https://man.archlinux.org/man/timedatectl.1#COMMANDS):

```bash
# timedatectl set-ntp true
```



## 6  Dispositivos de almacenamiento


### 6.1  Listar dispositivos de almacenamiento:

```bash
# lsblk -Sp
NAME     HCTL       TYPE VENDOR   MODEL                 REV SERIAL         TRAN
/dev/sda 0:0:0:0    disk ATA      HGST HTS545050A7E380 AC90 TWA51DL91GHGVS sata
/dev/sr0 1:0:0:0    rom  hp       hp DVD-RAM UJ8D1     H.01 SBD8T03835     sata
```

> - 💡 **Tenga en cuenta:**
>
>   * El dispositivo de almacenamiento elegido es `/dev/sda`.
>
>   
>
>   **NOTA:** Algunos posibles [nombres para dispositivos de bloque](https://wiki.archlinux.org/title/Device_file_(Espa%C3%B1ol)#Nombres_de_dispositivos_de_bloque) tipo `hdd`, `eMMc`, `ssd 2.5"`, `ssd M.2 msata` o `ssd M.2 nvme`, podrían ser:
>
>   1. `/dev/sdb`, `/dev/sdc`, `/dev/sdx/`
>
>   2. `/dev/hda`
>
>   3. `/dev/mmcblk0`
>
>   4. `/dev/vda`
>
>   5. `/dev/nvme0n1`

### 6.2  El dispositivo de almacenamiento es `hdd` o `ssd`?

```bash
# lsblk  -pso NAME,ROTA /dev/sda
NAME     ROTA
/dev/sda    1
```

> 💡 **Tenga en cuenta:**
>
> **`hdd`**: Si la columna `ROTA` marca `1`.
>
> **`ssd`**: Si la columna `ROTA` marca `0`.

### 6.3  Verificar esquema de partición

Consultamos la tabla de particiones en el dispositivo de almacenamiento:

```bash
# parted /dev/sda print | grep -E "Model|arti|Dis"
```

> 💡 Tenga en cuenta que el nombre del dispositivo de almacenamiento es `/dev/sda`.



### 6.4  Particiones





#### 6.4.1  Esquema de particiones para dispositivos de almacenamiento de rotación tipo `hdd`

##### 6.4.1.1  UEFI con `gpt` para `hdd`

| **Punto de montaje** | **Partición** | **[GUID de tipo del partición](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs)** | **[ID de tipo de partición](https://en.wikipedia.org/wiki/Partition_type)** | [Atributos de la partición](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_entries_.28LBA_2.E2.80.9333.29) | **Tamaño recomendado** |
| -------------------- | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------- |
| `/boot` o `/efi`     | `/dev/sda1`   | `C12A7328-F81F-11D2-BA4B-00A0C93EC93B`: [EFI System](https://wiki.archlinux.org/title/EFI_system_partition "EFI system partition") | `efh`: [EFI System](https://wiki.archlinux.org/title/EFI_system_partition "EFI system partition") |                                                              | Al menos 300MiB        |
| `[SWAP]`             | `/dev/sda2`   | `0657FD6D-A4AB-43C4-84E5-0933C84B4F4F`: Linux [swap](https://wiki.archlinux.org/title/Swap "Swap") | `82h`: Linux [swap](https://wiki.archlinux.org/title/Swap "Swap") |                                                              | Más de 512MiB          |
| `/`                  | `/dev/sda3`   | `4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709`: Linux x86-64 root (/) | `83h`: Linux                                                 |                                                              | 30GB o más             |
| `/home`              | `/dev/sda4`   | `933AC7E1-2EB4-4F13-B844-0E14E2AEF915`: Linux home (/home)   | `83h`: Linux                                                 |                                                              | Resto del dispositivo  |



##### 6.4.1.2  BIOS con `mbr` para `hdd`

| **Punto de montaje** | **Partición** | **[ID de tipo de partición](https://en.wikipedia.org/wiki/Partition_type)** | **Bandera de arranque** | **Tamaño recomendado** |
| :------------------- | :------------ | :----------------------------------------------------------- | :---------------------: | :--------------------- |
| `/boot`              | `/dev/sda1`   | `43h`: Linux                                                 |           SÍ            | Al menos 300MiB        |
| `[SWAP]`             | `/dev/sda2`   | `82h`: Linux [swap](https://wiki.archlinux.org/title/Swap "Swap") |           NO            | Más de 512MiB          |
| `/`                  | `/dev/sda3`   | `83h`: Linux                                                 |           NO            | 30GB o más             |
| `/home`              | `/dev/sda4`   | `83h`: Linux                                                 |           NO            | Resto del dispositivo  |

