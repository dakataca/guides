Autor: [@dakataca](https://t.me/dakataca)

## Internet wifi

### iwd

```sh
iwctl #host
  station list #host
  #exit
#rfkill list
#rfkill unblock wifi  #wiki.archlinux.org/title/Network_configuration/Wireless#Rfkill_caveat
  station wlan0 connect Alternativos #host
  station list #host
  exit #host
ip a #host
passwd #host
setfont ter-i18n
ping -c3 archlinux.org
```



### wpa_supplicant

```sh
bash -c 'cat > /etc/wpa_supplicant/wpa_supplicant.conf' << EOF
ctrl_interface=/run/wpa_supplicant
update_config=1
EOF
wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
```

Escanear redes

```sh
wpa_cli scan
wpa_cli scan_results
```

Conectarse a la red `Mamertolandia`

```sh
wpa_passphrase Mamertolandia progresistas >> /etc/wpa_supplicant/wpa_supplicant-nl80211-wlan0.conf
```



## ssh

```sh
rm ~/.ssh/known_hosts
ssh root@192.168.1.102
```

## Validate disk

```sh
lsscsi -s
lsblk -o  NAME,MODEL,SIZE,LABEL,ROTA,TRAN
lsblk -pso NAME,ROTA /dev/sda
parted /dev/sda print | grep -E "Model|abl|Dis"
ls /sys/firmware/efi/efivars
```



## Create partitions for only BTRFS

```sh
gdisk /dev/sda
   o      # Crear nueva tabla de partición GUID vacía GPT (borra todas las particiones).   
   Y      # Confirmar.  
   p      # Imprimir tabla de parciciones (vacía).
   n      # Crear nueva partición para EFI.
   Enter  # Número de la partición por defecto (1).
   Enter  # Primer sector de la partición EFI por defecto.
   +1G    # Tamaño (último sector) de la partición EFI.
   l      # Listar códigos de nombre/PARTLABEL de particiones.
   EFI    # Listar los códigos de nombre/PARTLABEL que coincidan con la regex "EFI".
   q      # Volver al modo comando de gdisk.
   ef00   # Código del tipo de partición (PARTLABEL) "EFI system partition" con LABEL "EFI".
   n      # Crear nueva partición para SWAP.
   Enter  # Número de la partición SWAP por defecto (2).
   Enter  # Primer sector de la partición por defecto.
## Fuente externa: https://help.ubuntu.com/community/SwapFaq#How_much_swap_do_I_need.3F
# ¤==================================================================================¤
# |             Tamaño de la partición SWAP según cantidad de ram y uso              |
# ¤===================:=====:=====:=======:======:=======:======:======:======:======¤
# |    Memoria ram    | 2GB | 4GB |  6GB  |  8GB |  12GB*| 16GB | 24GB | 32GB | 64GB |
# ¤======:=====:======:-----¤-----¤-------¤------¤-------¤------¤------¤------¤------¤
# |      |Hi   |  si* | 2GB | 5GB |  8GB  | 10GB |  14GB | 18GB | 27GB | 37GB | 70GB |
# | swap | ber :======:-----¤-----¤-------¤------¤-------¤------¤------¤------¤------¤
# |      |  nar|  no  | 1GB | 2GB | 2.5GB |  3GB | 3.5GB | 4GB  | 5GB  | 6GB  | 8GB  |
# ¤======:=====:======:-----¤-----¤-------¤------¤-------¤------¤------¤------¤------¤
   +14G   # Tamaño (último sector) de la partición SWAP.
   l      # Listar códigos de nombre/PARTLABEL de particiones.
   Linux  # Listar los códigos de nombre/PARTLABEL que coincidan con la regex "Linux".
   q      # Volver al modo comando de gdisk.
   8200   # Código del tipo de partición (PARTLABEL) "Linux swap", con LABEL "swap".
   n      # Crear nueva partición para ROOT BTRFS.
   Enter  # Número de la partición por defecto (3).
   Enter  # Primer sector de la partición ROOT por defecto.
   80G    # Tamaño (último sector) de la partición ROOT.
   8304   # Código del tipo de partición (PARTLABEL) "Linux x86-64 root(/)" con LABEL "root".
   n      # Crear nueva partición para HOME BTRFS.
   Enter  # Número de la partición por defecto (4).
   Enter  # Primer sector de la partición HOME por defecto.
   Enter  # Utilizar espacio restante (último sector) para partición HOME.
   8302   # Código del tipo de partición (PARTLABEL) "Linux home" con LABEL "home".
   p      # Imprimir tabla de parciciones terminada.
   w      # Guardar los cambios.
   Y      # Confirmar.
lsblk -o  NAME,MODEL,SIZE,ROTA,TRAN
```



## Swap partition (HDD)

```sh
mkswap -L swap /dev/sda2
swapon /dev/sda2
```



## Root partition

```sh
# ¤========================================================================================¤
# |    Opción  | Tamaño del nodo | Intensidad de |             Útil en sistemas            |
# |            |                 |   read/write  |                                         |
# :============|=================|===============|=========================================:
# |            |     (default)   |   Baja        | Para usuarios básicos con uso           |
# |            |       16k       |               | nada intensos.                          |
# |     -n     |-----------------|---------------|-----------------------------------------|
# | --nodesize |       32k       |   Media       | Bases de datos, programadores o         |
# |            |                 |               | servidores de bajo impacto.             |
# |            |-----------------|---------------|-----------------------------------------|
# |            |       64k       |   Alta        | Servidores con altas cargas de trabajo. |
# ¤------------'-----------------'---------------'-----------------------------------------¤
```

### SSD

```sh
mkfs.btrfs -f -L root --nodesize 32k --features free-space-tree /dev/sda3
```

### HDD

```sh
mkfs.btrfs -f -L root -O free-space-tree /dev/sda3
```



### Btrfs subvolumes BTRFS

```sh
mount /dev/sda3 /mnt
lsblk -po NAME,FSTYPE,LABEL,MOUNTPOINT,SIZE /dev/sda
btrfs subvolume create /mnt/@
# Si tiene SSD y HDD mejor crear en HDD.
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var_log
btrfs subvolume create /mnt/@var_tmp
btrfs subvolume create /mnt/@var_cache
btrfs subvolume create /mnt/@snapshots
btrfs subvolume list -p /mnt
ls /mnt
umount /mnt
```



### Mountpoints BTRFS

```sh
#https://wiki.archlinux.org/title/Solid_state_drive
#https://man.archlinux.org/man/btrfs.5#MOUNT_OPTIONS
mount -o noatime,compress=zstd,subvol=@ /dev/sda3 /mnt
# Si usa SSD y HDD se recomienda que los siguientes puntos de montaje se ubiquen en el HDD.
mount --mkdir -o noatime,compress=zstd,subvol=@home /dev/sda3 /mnt/home
mount --mkdir -o noatime,compress=zstd,subvol=@var_log /dev/sda3 /mnt/var/log
mount --mkdir -o noatime,compress=zstd,subvol=@var_tmp /dev/sda3 /mnt/var/tmp
mount --mkdir -o noatime,compress=zstd,subvol=@var_cache /dev/sda3 /mnt/var/cache
```



## EFI partition

```sh
mkfs.fat -F 32 -n EFI /dev/sda1
mount --mkdir /dev/sda1 /mnt/boot
lsblk -p /dev/sda
ls -R /mnt
```



## Mirrorlist

```sh
reflector --save /etc/pacman.d/mirrorlist --protocol http,https --sort rate --completion-percent 98 --connection-timeout 9 --download-timeout 9 --cache-timeout 9 --threads $(nproc) --latest 30 --fastest 30 --age 12
```



## Pacstrap / BTRFS

### Single Boot

```sh
vim /etc/pacman.conf
pacstrap /mnt base{,-devel} linux{,-{lts,headers,firmware,rt{,-lts}}} grub{,-btrfs} inotify-tools ntfs-3g gvfs-{mtp,afc,gphoto2} neovim bat ls{d,hw} procs du{st,f} gptfdisk hwinfo dmidecode inxi terminus-font dosfstools efibootmgr
```

### Dual Boot

```sh
pacstrap /mnt base{,-devel} linux{,-{lts,headers,firmware,rt{,-lts}}} grub{,-btrfs} inotify-tools ntfs-3g gvfs-{mtp,afc,gphoto2} neovim bat ls{d,hw} procs du{st,f} gptfdisk hwinfo terminus-font efibootmgr os-prober
```



## Snapper

```sh
pacman -Sy snapper --needed --noconfirm
snapper -c config create-config /mnt
btrfs subvolume delete /mnt/.snapshots
btrfs subvolume list -p /mnt
mount --mkdir -o noatime,compress=zstd,subvol=@snapshots /dev/sda3 /mnt/.snapshots
chmod a+rx /mnt/.snapshots
chown :wheel /mnt/.snapshots
```



## Fstab

```sh
genfstab -U /mnt > /mnt/etc/fstab
cat /mnt/etc/fstab
```



## Chroot

```sh
arch-chroot /mnt
```

```sh
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock --systohc
echo 'es_CO.UTF-8 UTF-8' > /etc/locale.gen
#nvim /etc/locale.gen
locale-gen
echo 'LANG=es_CO.UTF-8' > /etc/locale.conf
echo 'FONT=Lat2-Terminus16' > /etc/vconsole.conf
```



## SSD Fstrim

```sh
## wiki: https://wiki.archlinux.org/title/Solid_state_drive#Periodic_TRIM
systemctl enable fstrim.timer
```



## Host ##

```sh
echo aspire > /etc/hostname
#echo elitebook > /etc/hostname
#echo hp1000 > /etc/hostname
```

```sh
bash -c 'cat >> /etc/hosts' << EOF
127.0.0.1                        localhost
::1                              localhost
127.0.1.1  aspire.localdomain  aspire
#127.0.1.1  elitebook.localdomain  elitebook
#127.0.1.1  hp1000.localdomain    hp1000
EOF
```

## [Timer btrfs-scrub](https://wiki.archlinux.org/title/btrfs#Start_with_a_service_or_timer)

Mostrar información del dispositivo de almacenamiento con sistemas de ficheros btrfs.

```sh
sudo smartctl -a /dev/sda | grep -B 11 'SATA Version is'
```

> Model Family:     Western Digital Black Mobile
> Device Model:     WDC WD5000LPLX-60ZNTT1
> Serial Number:    WD-WXA1EC464UN2
> LU WWN Device Id: 5 0014ee 606424ae1
> Firmware Version: 02.01A02
> User Capacity:    500.107.862.016 bytes [500 GB]
> Sector Sizes:     512 bytes logical, 4096 bytes physical
> Rotation Rate:    7200 rpm
> Form Factor:      2.5 inches
> Device is:        In smartctl database 7.3/5528
> ATA Version is:   ACS-2, ACS-3 T13/2161-D revision 3b
> SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)

Habilitar el temporizador de escaneos mensuales en busca de errores en el sistema de ficheros btrfs en la raíz del sistema "`/`"

```sh
lsblk -o NAME,TRAN
```

```sh
mkdir -p /etc/systemd/system/btrfs-scrub@-.service.d
# 🌟🌟🌟 Editing /etc/systemd/system/btrfs-scrub@-.service.d/override.conf
# Anything between here and the comment below will become the contents of the drop-in file
#
# Limitar ancho de banda de E/S durante la limpieza mensual en el punto de montaje btrfs.
# Ajustar según necesidad:
# ¤=============:=====================:==============:===========¤
# |   Conexión  |       Opción        | Dispositivo  |   Límite  |
# :=============:=====================:==============:===========:
# |  PCIe NVMe  |                     | /dev/nvme0n1 |    100M   |
# :=============:                     |--------------|-----------|
# |     SATA    | IOReadBandwidthMax  |   /dev/sda   |    30M    |
# :=============:                     |--------------|-----------|
# | Thunderbolt/|                     |   /dev/tnv0  |    300M   |
# |   PCIe 3.0  |                     |              |           |
# ¤=============:=====================:==============:===========¤
bash -c 'cat > /etc/systemd/system/btrfs-scrub@-.service.d/override.conf' << EOF
[Service]
IOReadBandwidthMax='/dev/nvme0n1 100M'
EOF
```

```sh
systemctl enable btrfs-scrub@-.timer
```



## Initramfs and Grub

```properties
nvim /etc/mkinitcpio.conf
----------------------------------------------------------------
# 🌟🌟🌟📦 Edite la configuración de initramfs 🌟🌟🌟
# 📂 /etc/mkinitcpio.conf 👑

MODULES=(btrfs zram amdgpu)             # AMD amdgpu.
#MODULES=(btrfs zram radeon)             # AMD radeon.
#MODULES=(btrfs zram i915)               # Intel graphics.

...

# 🛌 Con hibernación:
# Elimine el hook fcsk (solo para root "/" en btrfs) y agregue resume, para habilitar la hibernación (disk) y grub-btrfs-overlayfs para iniciar instantáneas en modo rw.
HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems resume grub-btrfs-overlayfs)
# 👀 Sin hibernación:  Elimine el hook fcsk (solo si usa root "/" en btrfs).
#HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems)
```

```sh
mkinitcpio -P
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=ArchlinuxQtile
```



### Hibernación

Listar etiqueta o "LABEL" de la partición de respaldo "swap"  (mkswap).

```sh
blkid -s LABEL -o value /dev/sda2
................................................................
swap
```

Eliminar parámetro del kernel `quiet`y agregar parámetros para recordar el último kernel usado:

```properties
nvim /etc/default/grub
----------------------------------------------------------------
# 🌟🌟🌟 Edite/descomente las líneas de configuración de Grub 🌟🌟🌟
# 📂 /etc/default/grub 👑
# 🌐 wiki: https://wiki.archlinux.org/title/GRUB#Additional_arguments

# Comentar entrata numérica para recordar última entrada Grub iniciada como predeterminada.
# 🌐 wiki: https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Recall_previous_entry
#GRUB_DEFAULT=0

# Segundos de espera, antes de iniciar la entrada Grub predeterminada.
GRUB_TIMEOUT=2

# 🛌 Con hibernación.
# 🌐 wiki: https://wiki.archlinux.org/title/silent_boot
# 🌐 wiki: https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation
# ¤==========:================================:==================================¤
# |  Acción  |           Parámetro            |           Descripción            |
# :==========:================================:==================================:
# | Eliminar |             quiet              | Deshabilitar salida silenciosa.  |
# |----------.--------------------------------¤----------------------------------|
# |          |                                | Habilitar hibernación en disposi-|
# | Agregar  |  resume="LABEL=swap"           | tivo con etiqueta "swap" (mkswap)|
# |          |                                | requiere hook resume (initramfs) |
# ¤----------'--------------------------------'----------------------------------¤
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume='LABEL=swap'"
# 👀 Sin hibernación.
#GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"
...
# 🧠 Recordar última entrada Grub iniciada como predeterminada.
# 🌐 wiki: https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Recall_previous_entry
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
...
# 🪟 Detectando otros sistemas operativos.
# 🌐 wiki: https://wiki.archlinux.org/title/GRUB#Detecting_other_operating_systems
GRUB_DISABLE_OS_PROBER=false

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```



## Users

> **Optional groups:**
>
> - `optical,scanner,lp`: Si usa impresora/scanner.

```sh
useradd -mG wheel,input,audio,video,storage,network -s /bin/bash dakataca
useradd -mG nobody,input,audio,video,storage,network -s /bin/bash invitado
```

### Password users

```sh
passwd
passwd dakataca
passwd invitado
```



## Sudoers

```sh
EDITOR=nvim visudo
----------------------------------------------------------------
# Descomentar línea.
%wheel      ALL=(ALL:ALL) ALL
```

```properties
nvim /etc/pacman.conf
----------------------------------------------------------------
# 🌟🌟🌟🕹️ Edite la configuración del administrador de paquetes Pacman 🌟🌟🌟
# 📂 /etc/pacman.conf 👑📝
# 🌐 wiki: https://wiki.archlinux.org/title/Pacman#General_options
# 📜 man-page: https://man.archlinux.org/man/pacman.conf.5#OPTIONS
...
# Misc options
# Habilitar salida de colores en la terminal.
Color
# Habilitar n descargas paralelas.
# 🌐 wiki: https://wiki.archlinux.org/title/Pacman#Enabling_parallel_downloads
ParallelDownloads = 3
# Emoji del juego pacman comiendo dulces en barra de progreso.
ILoveCandy
...
# Habilitar repositorio de paquetes de 32 bits (x86) multilib.
[multilib]
Include = /etc/pacman.d/mirrorlist

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```



## AMD or Intel

```sh
# AMD
pacman -Sy amd-ucode usbutils htop neofetch lsscsi tree --needed --noconfirm

# Intel
#pacman -Sy intel-ucode usbutils htop neofetch lsscsi tree --needed --noconfirm
```



## Internet y Git

```sh
pacman -S net-tools openbsd-netcat rsync git{,-lfs} --needed --noconfirm
```

<table>
<thead>
    <tr>
		<th>Network Administration Tool</th>
        <th>Type tool</th>
        <th>Type conection</th>
    	<th>Mode IP</th>
    	<th>DNSSEC</th>
    </tr>
</thead>
<tbody>
    <tr>
    	<td rowspan=2><code>systemd-networkd</code> or <code>NetworkManager</code></td>
        <td rowspan=2>Complete network</td>
        <td>Wired</td>
    	<td>Static</td>
    	<td rowspan=2>True</td>
    </tr>
    <tr>
        <td>Wireless</td>
        <td>DHCP</td>
    </tr>
    <tr>
    	<td><code>iwd</code> or <code>nmcli</code></td>
        <td>Wireless network</td>
        <td>-</td>
    	<td>False</td>
    	<td>-</td>
    </tr>
    <tr>
        <td><code>unbound</code></td>
        <td>DNS del sistema</td>
        <td>-</td>
    	<td> - </td>
        <td>True</td>
    </tr>
    <tr>
        <td><code>dnscrypt</code></td>
        <td>Encrypt DNS</td>
        <td>-</td>
    	<td> - </td>
        <td>True</td>
    </tr>
</tbody>
</table>


### Modificar nombres de interfaces de red

Mostrar nombre de interfaces de red por defecto

```sh
ls /sys/class/net
```

> eth0  wlan0

#### wired

MacAdress

```sh
#ip address | sed -nE '/^[0-9]{,2}:.*enp.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}'
```

Reemplace la correpondiente MacAddress de su interface cableada.

```bash
bash -c 'cat > /etc/systemd/network/60-enp1s0.link' << EOF
# 🌟🌟🌟 Fichero de configuración del nombre de la red cableada systemd-networkd 🌟🌟🌟
# 📂 /etc/systemd/network/60-enp1s0.link 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Network_configuration#Change_interface_name
# 📜 man-page: https://man.archlinux.org/man/systemd.link.5#%5BMATCH%5D_SECTION_OPTIONS
[Match]
# MAC address.
PermanentMACAddress=$(ip address | sed -nE '/^[0-9]{,2}:.*enp.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}')

[Link]
# Nombre de la interface cableada.
Name=enp1s0

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

#### Wireless

MacAdress

```sh
#ip address | sed -nE '/^[0-9]{,2}:.*wl.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}'
```

Reemplace la correpondiente MacAddress de su interface inalambrica.

```bash
bash -c 'cat > /etc/systemd/network/65-wlp2s0.link' << EOF
# 🌟🌟🌟 Fichero de configuración del nombre de la red inalámbrica systemd-networkd 🌟🌟🌟
# 📂 /etc/systemd/network/65-wlp2s0.link 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Network_configuration#Change_interface_name
# 📜 man-page: https://man.archlinux.org/man/systemd.link.5#%5BMATCH%5D_SECTION_OPTIONS
[Match]
# MAC address.
PermanentMACAddress=$(ip address | sed -nE '/^[0-9]{,2}:.*wl.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}')

[Link]
# Nombre de la interface inalámbrica.
Name=wlp2s0

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

### 

### [Networkmanager](https://wiki.archlinux.org/title/NetworkManager#Installation) y [Android Tethering](https://wiki.archlinux.org/title/android_tethering#USB_tethering)

```sh
pacman -S network{manager,-manager-applet} usb_modeswitch --needed --noconfirm
```

```sh
bash -c 'cat > /etc/polkit-1/rules.d/50-org.freedesktop.NetworkManager.rules' << EOF
# 🌐 wiki: https://wiki.archlinux.org/title/NetworkManager#Set_up_PolicyKit_permissions
# Todos los usuarios del grupo network podrán agregar y eliminar redes sin contraseña.
polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("network")) {
    return polkit.Result.YES;
  }
});

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

#### nm-online

```sh
mkdir -p /etc/systemd/system/NetworkManager-wait-online.service.d/
bash -c 'cat > /etc/systemd/system/NetworkManager-wait-online.service.d/skip-networkmanager.conf' << EOF
# 🌐 wiki: https://wiki.archlinux.org/title/NetworkManager#NetworkManager-wait-online
# 📜 man-page: https://man.archlinux.org/man/nm-online.1.en#OPTIONS
[Service]
ExecStart=
ExecStart=/usr/bin/nm-online --quiet --timeout 8

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



### [Systemd-networkd](https://man.archlinux.org/man/systemd.network.5.de)

#### Iwd

```sh
pacman -S iwd --needed --noconfirm
```

```sh
mkdir -p /etc/iwd/
bash -c 'cat >> /etc/iwd/main.conf' << EOF
# 🌟🌟🌟 Fichero de configuración opcional del gestor de redes inalámbricas IWD, adaptado para el uso junto al administrador de redes completo systemd-netowrkd 🌟🌟🌟
# 📂 /etc/iwd/main.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Iwd#Enable_built-in_network_configuration
# 📜 man-page: https://man.archlinux.org/man/extra/iwd/iwd.config.5.en#Network

[Network]
# ¤===================¤============¤==============================================¤
# |   Servidor DNS    |   Opción   |                IWD +                         |
# ¤===================:============:==============================================¤
# | DNS del sistema*: |    none    | Systemd-networkd*, connman o NetworkManager. |
# | Openresolv:       | resolvconf | Openresolv como implementación Resolvconf.   |
# | systemd-resolv:   |  systemd   | Systemd-networkd + systemd-resolv (Default). |
# ¤===================¤============¤==============================================¤
NameResolvingService=none

[General]
# Asignar direcciones IP y configurar rutas usando el cliente DHCP incorporado.
# 🌐 wiki: https://man.archlinux.org/man/community/iwd/iwd.config.5.en#General_Settings
# ¤=========¤==================¤=================================================¤
# |  Opción |     Opción en    |               Descripción                       |
# |  en IWD | systemd-networkd |                                                 |
# ¤=========:==================:=================================================¤
# |  false  |      DHCP=yes    | Desactivar el cliente DHCP interno de iwd y ac- |
# |         |                  | tivar el de systemd-networkd.*                  |
# ¤---------¤------------------¤-------------------------------------------------¤
# |         |                  | Si usa iwd solo, sin un administrador de red    |
# |         |                  | completo como systemd-networkd. De lo contrario,|
# |         |                  | evite usar DHCP=no en ".../25-wireless.network" |
# |   true  |                  | (systemd-networkd+iwd). La interface inalámcrica|
# |         |                  | se qudará sin IP cuando reinicie únicamente el  |
# |         |                  | servicio systemd-networkd.                      |
# ¤=========¤==================¤=================================================¤
EnableNetworkConfiguration=false

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

#### systemd-networkd-wait-online

```sh
mkdir -p /etc/systemd/system/systemd-networkd-wait-online.service.d/
bash -c 'cat >> /etc/systemd/system/systemd-networkd-wait-online.service.d/wait-for-only-one-interface.conf' << EOF
# 🌟🌟🌟 Iniciar cuando al menos una interface de red (wifi o wired) esté activa, o luego de transcurrir el tiempo de espera establecido en --timeout 🌟🌟🌟
# 🌐 wiki: https://wiki.archlinux.org/title/Systemd-networkd#systemd-networkd-wait-online
# 📜 man-page: https://man.archlinux.org/man/core/systemd/systemd-networkd-wait-online.8.en#OPTIONS
[Service]
ExecStart=
ExecStart=/usr/lib/systemd/systemd-networkd-wait-online --any --timeout 6

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

### Sin administrador completo de red

Paquetes necesarios

```sh
pacman -S iw wpa_supplicant dhcpcd --needed
```



#### Wifi (wpa_supplicant)

Interface de red inalámbrica

```sh
ls /sys/class/net/ | sed -En "s/.+(wl.+)/\1/p"
```

> wlan0

Insertar cabecera de fichero de wpa_supplicant:

```sh
bash -c 'cat > /etc/wpa_supplicant-nl80211-wlp2s0.conf' << EOF
ctrl_interface=/run/wpa_supplicant
ctrl_interface_group=wheel
update_config=1

EOF
```

```sh
read -sp "Introduce tu contraseña wifi: " password
read -sp "Vuelva a introducir su contraseña wifi: " back_password
[ "$password" == "$back_password" ] && wpa_supplicant -B -i wlp2s0 -c <(wpa_passphrase "Mamertolandia" $password >> /etc/wpa_supplicant/wpa_supplicant-nl80211-wlp2s0.conf) || echo "Contraseñas no coinciden."
```

Habilitar servicios

```sh
systemctl enable {wpa_supplicant-nl80211@wlp2s0,dhcpcd}.service
```



#### dhcpcd

Comentar opción de DNS dinámico

```sh
bash -c 'cat >> /etc/dhcpcd.conf' << EOF
# 🌟🌟🌟 Configuración del servidor dns dhcpcd.
# 📂 /etc/dhcpcd.conf

#   Static enp1s0.
interface enp1s0
static ip_address=192.168.1.10/24
static routers=192.168.1.1
static domain_name_servers=127.0.0.1
metric 1002

#   Static wlp2s0.
interface wlp2s0
static ip_address=192.168.1.20/24
static routers=192.168.1.1
static domain_name_servers=127.0.0.1
metric 1004

#   Dinamic net tethering.
interface net0
option domain_name_servers, domain_name, domain_search
metric 1000
EOF
```



### Ficheros de red

```sh
bash -c 'cat > /etc/systemd/network/20-wired.network' << EOF
# 🌟🌟🌟 Fichero de configuración de la red cableada con systemd-networkd 🌟🌟🌟
# 📂 /etc/systemd/network/20-wired.network 👑
# 🌐 wiki: https://wiki.archlinux.org/title/systemd-networkd#Wired_and_wireless_adapters_on_the_same_machine
# 📜 man-page: https://man.archlinux.org/man/systemd.network.5

[Match]
# Nombre de la interface de red cableada.
Name=enp1s0

[Network]
# DHCP.
DHCP=yes
# Validación de soporte para DNSSEC.
DNSSEC=true
# Use servidor DNS interno (unbound).
DNS=127.0.0.1
DNS=::1
# Esperar 3 segundos antes de desconectar la interfaz de red si se pierde la conexión. Solo 3s en modo roaming.
IgnoreCarrierLoss=3s

[DHCPv4]
# Minimizar la divulgación de información de identificación.
Anonymize=true
# Métrica de la ruta ipv4: Entre menor la ruta mayor preferencia (20 < 10 > 25).
RouteMetric=20

[IPv6AcceptRA]
# Métrica de la ruta ipv6: Entre menor la ruta mayor preferencia (20 < 10 > 25).
RouteMetric=20

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
bash -c 'cat > /etc/systemd/network/25-wireless.network' << EOF
# 🌟🌟🌟 Fichero de configuración de la red inalámbrica con systemd-networkd 🌟🌟🌟
# 📂 /etc/systemd/network/25-wireless.network  👑
# 🌐 wiki: https://wiki.archlinux.org/title/systemd-networkd#Wired_and_wireless_adapters_on_the_same_machine
# 📜 man-page: https://man.archlinux.org/man/systemd.network.5#%5BDHCPV4%5D_SECTION_OPTIONS

[Match]
# Nombre de la interface de red inalámbrica.
Name=wlp2s0

[Network]
# Asignar de manera dinámica una IP versión 4 disponible.
# ¤========¤==============¤==========================================================¤
# |    Opción   | DHCP interno |             Requiere                                |
# ¤=============:==============:=====================================================¤
# |   DHCP=yes  | Activado     |                                                     |
# |-------------|--------------|                                                     |
# |   DHCP=ipv4 | Activado*    |           DHCP interno de IWD desactivado           |
# |-------------|--------------|           EnableNetworkConfiguration=false          |
# |   DHCP=ipv6 | Activado     |                                                     |
# |-------------|--------------|-----------------------------------------------------|
# |   DHCP=no   | Desactivado  | Da errores con cliente DHCP interno de IWD activado.|
# ¤=============¤==============¤=====================================================¤
DHCP=yes
# Validación de soporte para DNSSEC.
DNSSEC=true
# Use servidor DNS interno (unbound).
DNS=127.0.0.1
DNS=::1
# Esperar 3 segundos antes de desconectar la interfaz de red si se pierde la conexión. Solo 3s en modo roaming.
IgnoreCarrierLoss=3s

[DHCPv4]
# Minimizar la divulgación de información de identificación.
Anonymize=true
# Métrica de la ruta ipv4: Entre menor la ruta mayor preferencia (25 < 20 < 10).
RouteMetric=25

[IPv6AcceptRA]
# Métrica de la ruta ipv6: Entre menor la ruta mayor preferencia (25 < 20 < 10).
RouteMetric=25
# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



### Unbound y dnscrypt-proxy (DNS)

#### Instalación

```sh
 pacman -S openssh unbound expat dnscrypt-proxy --needed --noconfirm
```

#### Fichero de configuración de unbound

```properties
bash -c 'cat > /etc/unbound/unbound.conf' << EOF
# 🌟🌟🌟 Configuración del servidor DNS unbound 🌟🌟🌟
# 📂 /etc/unbound/unbound.conf 👑📝

server:
    do-ip4: yes
    do-ip6: yes
    do-udp: yes
    do-tcp: yes
    verbosity: 2
    num-threads: $(nproc)
    prefetch: yes
    msg-cache-size: $(awk '/^MemTotal:/{print int((($2/1024)*0.1)/3)}' /proc/meminfo)m
    rrset-cache-size: $(awk '/^MemTotal:/{print int((($2/1024)*0.1)*(2/3))}' /proc/meminfo)m
    # Ocultar información reservada.
    qname-minimisation: yes
    hide-identity: yes
    hide-version: yes
    # Usar la validación de DNSSEC.
    # 🌐 wiki: https://wiki.archlinux.org/title/Unbound#DNSSEC_validation
    trust-anchor-file: "/etc/unbound/trusted-key.key"

    # Fichero de sugerencias de raíz.
    # 🌐 wiki: https://wiki.archlinux.org/title/unbound#Root_hints
    root-hints: "/etc/unbound/root.hints"
    # dnscrypt-proxy + unbound.
    # 🌐 wiki: https://wiki.archlinux.org/title/Dnscrypt-proxy#Unbound
    # Habilitar consultas en localhost.
    # 📜 man-page:https://man.archlinux.org/man/unbound.conf.5.en#do~7
    do-not-query-localhost: no

forward-zone:
    name: "."
    forward-addr: ::1@53000
    forward-addr: ::127.0.0.1@53000

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

#### Fichero de sugerencias de raíz `root.hints`

```sh
curl --output /etc/unbound/root.hints https://www.internic.net/domain/named.cache && \
cp /etc/unbound/root.hints /etc/unbound/root.hints.back
```



Fichero de sobrecarga de `unbound` para que dependa de `systemd-networkd`

```sh
mkdir -p /etc/systemd/system/unbound.service.d/
bash -c 'cat > /etc/systemd/system/unbound.service.d/networkd-restart.conf' << EOF
# 🌟 Sobrecarga del servicio unbound.service para reiniciarlo en caso de fallos. 🌟
# 📂 /etc/systemd/system/unbound.service.d/networkd-restart.conf 👑
# 🌐 https://wiki.archlinux.org/title/systemd#Drop-in_files
# 📖 https://man.archlinux.org/man/systemd.unit.5#SPECIFIERS
# 🛠️ unbound.service
# 🎯 Garantiza máximo 3 reinicios de unbound.service ante cualquier fallo del servicio.
# 🎯  

[Unit]
Wants=systemd-networkd.service
After=systemd-networkd.service suspend.target hibernate.target suspend-then-hibernate.target hybrid-sleep.target
StartLimitIntervalSec=10           # Intérvalo de segundos mientras se intenta reiniciar.
StartLimitBurst=3                  # Número máximo de intentos de reinicio.

[Service]
Restart=on-success                 # Cuando servicio systemd-networkd es exitoso.
RestartSec=300ms
EOF
```

Comprobar la configuración de unbound 

```sh
unbound-checkconf
```

#### Fichero de configuración de dnscrypt-proxy

```sh
sed -Ei "s/^(server_names ?=).*/\1 \
['quad9-dnscrypt-ip4-filter-pri','google','cloudflare','scaleway-fr','yandex']/; \
s/^(listen_addresses ?=).*/\1 ['127.0.0.1:53000','[::1]:53000']/; \
s/^(ipv6_servers ?=).*/\1 true/; s/^(require_dnssec ?=).*/\1 true/" /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```



Fichero de sobrecarga de `dnscrypt-proxy` para que dependa de `systemd-networkd`.

```sh
mkdir -p /etc/systemd/system/dnscrypt-proxy.service.d/
bash -c 'cat > /etc/systemd/system/dnscrypt-proxy.service.d/networkd-restart.conf' << EOF
# 🌟 Sobrecarga del servicio dnscrypt-proxy.service para reiniciarlo en caso de fallos. 🌟
# 📂 /etc/systemd/system/dnscrypt-proxy.service.d/networkd-restart.conf 👑
# 🌐 https://wiki.archlinux.org/title/systemd#Drop-in_files
# 📖 https://man.archlinux.org/man/systemd.unit.5#SPECIFIERS
# 🛠️ dnscrypt-proxy.service
# 🎯 Garantiza máximo 3 reinicios de dnscrypt-proxy.service ante cualquier fallo del servicio.

[Unit]
Wants=systemd-networkd.service
After=systemd-networkd.service suspend.target hibernate.target suspend-then-hibernate.target hybrid-sleep.target
StartLimitIntervalSec=10           # Intérvalo de segundos mientras se intenta reiniciar.
StartLimitBurst=3                  # Número máximo de intentos de reinicio.

[Service]
Restart=on-success                 # Cuando servicio systemd-networkd es exitoso.
RestartSec=300ms
EOF
```



### Systemd-networkd

```sh
systemctl enable {iwd,systemd-networkd,sshd,unbound,dnscrypt-proxy}.service
```

### NetworkManager

```sh
systemctl enable {NetworkManager,sshd,unbound,dnscrypt-proxy}.service
```

Interfaces de red:

```sh
ls /sys/class/net
```

Usar `nmcli` en *modo interactivo*:

```sh
# 🌐 wiki: https://wiki.archlinux.org/title/NetworkManager#nmcli_examples
# 📖 https://man.archlinux.org/man/nmcli-examples.7.en
# 📖 https://man.archlinux.org/man/nmcli.1

# Agregar conexión cableada con IP estática "192.168.1.10" usando interface enp3s0.
# Establecer de DNS interno, DNSSEC y deshabilitar cualquier búsqueda DNS.
nmcli connection add con-name "wired" ifname enp3s0 type ethernet ip4 192.168.1.10/24 gw4 192.168.1.1 ipv4.dns "127.0.0.1" ipv6.dns "::1" ipv4.dns-options "dnssec validate" ipv4.dns-search ""

# Listar conexiones y dispositivos.
nmcli connection show

# Listar redes wifi disponibles y su estado.
nmcli device wifi list

# Conectarse a red wifi.
nmcli --ask device wifi connect "Pacto Historico"

# Nombre de la conexión inalámbrica.
nmcli connection show | grep 'wifi'

# Establecer DNS interno, DNSSEC y deshabilitar cualquier búsqueda DNS.
nmcli connection modify "Pacto Historico" ipv4.dns "127.0.0.1" ipv6.dns "::1" ipv4.dns-options "dnssec validate" ipv4.dns-search ""

# Listar redes wifi disponibles y su estado.
nmcli device wifi list

# Verificar características DNS de una conexión.
nmcli con show "Pacto Historico" | grep dns

## Borrar altigua conexión.
# Listar conexiones y dispositivos.
nmcli connection show
#nmcli connection delete "Wired connection 1"
nmcli connection delete "Conexión cableada 1"
```

 source ~/.bashrcsource ~/.bash_aliasessource ~/.bash_functionssh

## zram-generator y snapper

```sh
pacman -S snapper snap-pac zram-generator --needed --noconfirm
```

```sh
bash -c 'cat > /etc/systemd/zram-generator.conf' << EOF
# 🌟 Fichero de configuración de la utilidad de compresión de ram zram-generator 🌟
# 📂 /etc/systemd/zram-generator.conf 👑
# 🌐 https://wiki.archlinux.org/title/Zram#Automatically
# 📖 https://man.archlinux.org/man/zram-generator.conf.5#OPTIONS

[zram0]
# Tamaño del dispositivo de acuerdo a la ram disponible.
zram-size=max(ram / 3, 1300)
# Prioridad del uso de zram. Por defecto es 100. Directamente proporcional.
swap-priority=110
# Algoritmo de compresión más eficiente (zstd).
compression-algorithm=zstd

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
nvim /etc/updatedb.conf
---------------------------------------------------------------
# 🌟 Edite el fichero de configuación para evitar que updatedb indexe /.snapshots.
PRUNENAMES = ".snapshots"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

```sh
#cp /usr/share/snapper/config-templates/default /etc/snapper/configs/config
```

```sh
# 🌟🌟🌟 Edite la configuración del administrador de instantáneas snapper (btrfs) 🌟🌟🌟
# 📂 /etc/snapper/configs/config 👑📝
# 🌐 https://wiki.archlinux.org/title/snapper#Set_snapshot_limits
# 📖 https://man.archlinux.org/man/snapper-configs.5#VARIABLES
sed -E "s/^(ALLOW_GROUPS ?=).+/\1\"wheel\"/; \
s/^(SYNC_ACL ?=).+/\1\"yes\"/; \
s/^(NUMBER_LIMIT ?=).+/\1\"30\"/; \
s/^(NUMBER_LIMIT_IMPORTANT ?=).+/\1\"5\"/; \
s/^(TIMELINE_MIN_AGE ?=).+/\1\"1800\"/; \
s/^(TIMELINE_LIMIT_HOURLY ?=).+/\1\"5\"/; \
s/^(TIMELINE_LIMIT_DAILY ?=).+/\1\"7\"/; \
s/^(TIMELINE_LIMIT_WEEKLY ?=).+/\1\"10\"/; \
s/^(TIMELINE_LIMIT_MONTHLY ?=).+/\1\"15\"/; \
s/^(TIMELINE_LIMIT_YEARLY ?=).+/\1\"20\"/" \
/usr/share/snapper/config-templates/default | sudo tee /etc/snapper/configs/config > /dev/null
cat /etc/snapper/configs/config
```

```sh
systemctl enable snapper-{timeline,cleanup}.timer
systemctl enable grub-btrfsd.service
```



## Directorios del usuario y pipewire

```sh
pacman -S xdg-user-dirs --needed --noconfirm
```

```sh
su - dakataca -c 'LC_ALL=C xdg-user-dirs-update --force'
su - invitado -c 'LC_ALL=C xdg-user-dirs-update --force'
```



## Editor de texto

```sh
nvim /etc/environment
----------------------------------------------------------------
# 🌟 Establecer editor de texto completo.
VISUAL=nvim
EDITOR=nvim

#MESA_LOADER_DRIVER_OVERRIDE=i965      # Si usa mesa-amber con controlador Intel i965.
```



## sysctl

```sh
bash -c 'cat > /etc/sysctl.d/99-sysctl.conf' << EOF
# 🌟🌟🌟 Fichero de configuración del administrador de parámetros del kernel sysctl 🌟🌟🌟
# 📂 /etc/sysctl.d/99-sysctl.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Sysctl#Configuration

# Deshabilitar zswap por incompatibilidad con zram.
# 🌐 wiki: https://wiki.archlinux.org/title/Zswap#Toggling_zswap
# 🌐 wiki: https://wiki.archlinux.org/title/Improving_performance#zram_or_zswap
zswap.enabled=0

# 🌐 wiki: https://wiki.archlinux.org/title/Swap#Swappiness
# ¤====================================================================================¤
# |                      Tendentencia del núcleo a usar la swap                        |
# ¤============:=====:=====:=======:=====:=======:======:======:======:======:=========¤
# |  Ram       | 2GB | 4GB*|  6GB  | 8GB |  12GB | 16GB | 24GB | 32GB | 64GB | Default |
# ¤============:-----¤-----¤-------¤-----¤-------¤------¤------¤------¤------¤---------¤
# | swappiness |  5  |  10 |  14   | 22  |   31  |  42  |  56  |  67  |  78  |   60    |
# ¤============:-----¤-----¤-------¤-----¤-------¤------¤------¤------¤------¤---------¤
vm.swappiness=22

# Habilitar TCP Fast Open.
# 🌐 wiki: https://wiki.archlinux.org/title/Sysctl#Enable_TCP_Fast_Open
net.ipv4.tcp_fastopen=3

# 🌐 wiki: https://wiki.archlinux.org/title/Swap#VFS_cache_pressure
# 📝 doc: https://web.archive.org/web/20171004100853/http://doc.opensuse.org/documentation/leap/tuning/html/book.sle.tuning/cha.tuning.memory.html#cha.tuning.memory.vm.reclaim
# ¤===================================================================================¤
# |              Tendentencia del núcleo a pedir memoria para Caché VFS               |
# ¤===========:=====:=====:=======:=====:=======:======:======:======:======:=========¤
# |   Ram     | 2GB | 4GB*|  6GB  | 8GB |  12GB | 16GB | 24GB | 32GB | 64GB | Default |
# ¤===========:-----¤-----¤-------¤-----¤-------¤------¤------¤------¤------¤---------¤
# | Cache VFS | 48  |  50 |  53   |  58 |   64  |  74  |  85  |  97  |  110 |  100    |
# ¤===========:-----¤-----¤-------¤-----¤-------¤------¤------¤------¤------¤---------¤
vm.vfs_cache_pressure=58

# Detectar conexiones TCP muertas después de 120 segundos.
# 🌐 wiki: https://wiki.archlinux.org/title/sysctl#Change_TCP_keepalive_parameters
net.ipv4.tcp_keepalive_time = 60 
net.ipv4.tcp_keepalive_intvl = 10 
net.ipv4.tcp_keepalive_probes = 6

# Registrar paquetes marcianos para mayor inspección.
# 🌐 wiki: https://wiki.archlinux.org/title/sysctl#Log_martian_packets
net.ipv4.conf.default.log_martians = 1
net.ipv4.conf.all.log_martians = 1

# Gestión de la memoria caché de escritura de disco en Linux.
# 🌐 wiki: https://wiki.archlinux.org/title/Sysctl#Virtual_memory
# ¤====================================================================================¤
# |                                      Memoria virtual                               |
# ¤========================:=====:=====:======:=====:======:======:======:======:======¤
# |            Ram         | 2GB | 4GB*|  6GB | 8GB | 12GB | 16GB | 24GB | 32GB | 64GB |
# ¤========================:-----¤-----¤------¤-----¤------¤------¤------¤------¤------¤
# |       dirty_ratio      |  9  |  8  |   7  |  6  |  4   |   3  |  2.5 |   2  |   1  |
# ¤========================:-----¤-----¤------¤-----¤------¤------¤------¤------¤------¤
# | dirty_background_ratio | 4.5 |  4  |  3.5 |  3  |  2   |  1.5 |   1  |  0.6 |  0.3 |
# ¤========================:-----¤-----¤------¤-----¤------¤------¤------¤------¤------¤
vm.dirty_ratio = 6               # Porcentaje mínimo de memoria RAM libre que el sistema debe mantener disponible antes de comenzar a escribir datos de la caché de escritura en el disco de fondo.
vm.dirty_background_ratio = 3    # Porcentaje máximo de memoria RAM que se puede usar para la caché de escritura activa antes de comenzar a escribir datos de la caché de escritura en el disco de fondo.

# Aumentar tamaño de la cola de recepción. Solo para tarjetas de red y CPU de alta velocidad.
# 🌐 wiki: https://wiki.archlinux.org/title/sysctl#Increasing_the_size_of_the_receive_queue.
net.core.netdev_max_backlog = 16384

# Máximo de conexiones que aceptará el kernel.
net.core.somaxconn = 8192          # Para procesadores con muchos núcleos.

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
exit
```

```sh
swapoff -a
umount -R /mnt
reboot
```

