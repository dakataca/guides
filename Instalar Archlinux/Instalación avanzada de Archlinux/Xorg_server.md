

[TOC]





# Controladores para GPU's

Antes de instalar el servidor [Xorg](https://wiki.archlinux.org/title/Xorg) debemos establecer como mínimo, la [implementación OpenGL](https://wiki.archlinux.org/title/OpenGL#Installation) y el controlador gráfico adecuado.



## Controladores para GPU's Intel

<table>
<thead>
    <tr>
    	<th align="center" style="vertical-align: middle;" rowspan=2>Controlador (Open Source)</th>
        <!--<th align="center" style="vertical-align: middle;" rowspan=2>Tipo</th>-->
        <th align="center" style="vertical-align: middle;" rowspan=2>Paquete</th>
        <th align="center" style="vertical-align: middle;"rowspan=2>Manual Pages</th>
        <th align="center" style="vertical-align: middle;" colspan=2>OpenGL</th>
        <th align="center" style="vertical-align: middle;" rowspan=2>OpenCL</th>
        <th align="center" style="vertical-align: middle;" colspan=2>Vulkan</th>
        <th align="center" style="vertical-align: middle;"rowspan=2>Docs</th>
    </tr>
    <tr>
        <th align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=mesa" target="_blank">x86_64</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://wiki.archlinux.org/title/Multilib" target="_blank">multilib</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=mesa" target="_blank">x86_64</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://wiki.archlinux.org/title/Multilib" target="_blank">multilib</a></th>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>intel</code></td>
        <!--<td align="center" style="vertical-align: middle;" rowspan=2>Open Source</td>-->
        <td align="center"><a href="https://archlinux.org/packages/extra/x86_64/xf86-video-intel/" target="_blank">xf86-video-intel</a></td>
        <td align="center" style="vertical-align: middle;"><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel.4</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://archlinux.org/packages/extra/x86_64/mesa/" target="_blank">mesa</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://archlinux.org/packages/multilib/x86_64/lib32-mesa/" target="_blank">lib32-mesa</a></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://aur.archlinux.org/packages/intel-opencl/" target="_blank">intel-opencl</a> ó <a href="https://archlinux.org/packages/?name=intel-compute-runtime" target="_blank">intel-compute-runtime</a></td>
        <td alignt="center" style="vertical-align: middle;" rowspan=2><a href="https://archlinux.org/packages/extra/x86_64/vulkan-icd-loader/" target="_blank">vulkan-icd-loader</a> y <a href="https://archlinux.org/packages/extra/x86_64/vulkan-intel/" target="_blank">vulkan-intel</a></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://archlinux.org/packages/multilib/x86_64/lib32-vulkan-icd-loader/" target="_blank">lib32-vulkan-icd-loader</a> y <a href="https://archlinux.org/packages/multilib/x86_64/lib32-vulkan-intel/" target="_blank">lib32-vulkan-intel</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://wiki.archlinux.org/title/Intel_graphics" target="_blank">Intel graphics</a></td>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>modesetting</code></td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/extra/x86_64/xorg-server/" target="_blank">xorg-server</a></td>
        <td align="center" style="vertical-align: middle;"><a href="https://man.archlinux.org/man/modesetting.4" target="_blank">modesetting.4</a></td>
    </tr>
</thead>
<tbody>
</tbody>        
</table>


### Controlador apropiado

<table>
<thead>
    <tr>
        <th align="center">Controlador</th>
        <th align="center">Servidor gráfico</th>
        <th align="center">Generación recomendada</th>
        <th align="center">Arquitectura recomendada</th>
        <th align="center">Paquete</th>
        <th align="center">Fichero de configuración</th>
        <th align="center">Tearing</th>
    </tr>
<thead>
<tbody>
    <tr>
		<td rowspan=2 align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/modesetting.4" target="_blank">modesetting</a></code></td>
        <td align="center" style="vertical-align: middle;">Wayland</td>
        <td align="center" style="vertical-align: middle;"><code>Gen >=4</code></td>
        <td align="center" style="vertical-align: middle;">Lakeport en adelante</td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/testing/x86_64/linux/" target="_blank">linux</a></td>
        <td align="center" style="vertical-align: middle;">Wayland se configura a través de variables de entorno.</td>
        <td align="center" style="vertical-align: middle;">No</td>
    </tr>
    <tr>
        <td rowspan=2 align="center" style="vertical-align: middle;">Xorg</td>
        <td align="center" style="vertical-align: middle;"><code>Desde Gen 1</code></td>
        <td align="center" style="vertical-align: middle;">A partir de Auburn</td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/extra/x86_64/xorg-server/" target="_blank">xorg-server</a></td>
        <td rowspan=2 align="center" style="vertical-align: middle;"><code>/etc/X11/xorg.conf.d/20-gpudriver.conf</code></td>
        <td  rowspan=2  align="center" style="vertical-align: middle;">Probable</td>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td>
		<td align="center" style="vertical-align: middle;"><code>Hasta Gen 7</code></td>
        <td align="center" style="vertical-align: middle;">Hasta Ivy Bridge o Bay Trail</td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://www.archlinux.org/packages/extra/x86_64/xf86-video-intel/" target="_blank">xf86-video-intel</a></td>
    </tr>
</tbody>
</table>

> **Nota**: Para más información sobre los controladores *Intel* y las tecnologías compatibles siga la [guía específica para GPU's *Intel*](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md).



### Validar controlador modesetting en Xorg

La mayoría de procesadores gráficos usados en la actualidad son de cuarta generación o mayor, por eso siempre es prudente iniciar comprobando que nuestra gráfica *Intel* sea compatible con el controlador recomendado `modesetting` y descartar que nuestro servidor *Xorg* no presente problemas de *tearing*. Para eso debemos instalar el servidor *Xorg* junto al entorno gráfico de nuestra preferencia, ya que el controlador `modesetting` viene incorporado en el kernel.



#### Instalar Xorg para un [Windows Manager](https://wiki.archlinux.org/title/window_manager#List_of_window_managers)

```sh
# Para inicio manual con ~/.xinitrc.
pacman -S xorg-{server,xin{put,it},xdpyinfo} --needed --noconfirm
```

# Configurar controlador apropiado

Siga la guía correspondiente al controlador apropiado para configurar correctamente su gráfica *Intel*.

1. [modesetting](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#modesetting)
2. [intel](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#intel)



# Teclados

Configuramos nuestros teclados desde el más genérico hasta el más usado, usando la sección `InputClass` del fichero de configuración *Xorg* para teclados `/etc/X11/xorg.conf.d/00-keyboard.conf`. Si solo tiene un teclado con la sección genérica sería suficiente.

## Teclados USB

Identificamos el nombre de los teclados conectados vía USB.

```sh
lsusb
................................................................
Bus 002 Device 002: ID 04f2:b34f Chicony Electronics Co., Ltd HP Truevision HD
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 004 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 005: ID 258a:002a SINO WEALTH Gaming KB
Bus 003 Device 004: ID 093a:2521 Pixart Imaging, Inc. Optical Mouse
Bus 003 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
```

Ya tenemos  la opción: `Identifier "SINO WEALTH Gaming KB"` del teclado usb.



## Teclado interno

Si su equipo es una portátil y tiene un teclado integrado lo listamos usando `xinput`:

```sh
xinput --list | grep keyboard
................................................................
⎣ Virtual core keyboard                   id=3  [master keyboard (2)]
↳ Virtual core XTEST keyboard             id=5  [slave  keyboard (3)]
↳ Power Button                            id=6  [slave  keyboard (3)]
↳ Video Bus                               id=7  [slave  keyboard (3)]
↳ Power Button                            id=8  [slave  keyboard (3)]
↳ HP Truevision HD: HP Truevision         id=14 [slave  keyboard (3)]
↳ AT Translated Set 2 keyboard            id=15 [slave  keyboard (3)]
↳ Wireless hotkeys                        id=17 [slave  keyboard (3)]
↳ HP WMI hotkeys                          id=18 [slave  keyboard (3)]
↳ SINO WEALTH Gaming KB System Control    id=10 [slave  keyboard (3)]
↳ SINO WEALTH Gaming KB                   id=11 [slave  keyboard (3)]
↳ SINO WEALTH Gaming KB Consumer Control  id=13 [slave  keyboard (3)]
↳ SINO WEALTH Gaming KB Keyboard          id=19 [slave  keyboard (3)]
```

Ya tenemos  la opción: `Identifier "AT Translated Set 2 keyboard"` del teclado de la portátil.



## Fichero de configuración de los teclados en Xorg

```sh
bash -c 'cat > /etc/X11/xorg.conf.d/00-keyboard.conf' << EOF
# 🌟🌟🌟❌⌨️ Fichero de configuración de teclados específicos en Xorg.
# 📂 /etc/X11/xorg.conf.d/00-keyboard.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Xorg/Keyboard_configuration#Using_X_configuration_files
# 📜 man-page: https://man.archlinux.org/man/xkeyboard-config.7#MODELS
# 📜 man-page: https://man.archlinux.org/man/xkeyboard-config.7#LAYOUTS
# 📜 man-page: https://man.archlinux.org/man/xkeyboard-config.7#OPTIONS

# 📜 man-page: https://man.archlinux.org/man/extra/xorg-server/xorg.conf.5.en#INPUTCLASS_SECTION
# Cada "InputClass" puede anular la configuración de una clase anterior (arriba).
# Es conveniente organizar primero las "Section"s con las coincidencias más genéricas.
# Por eso configuramos el teclado de mayor preferencia abajo (KUMARA K552-KR), y los de menor preferencia arriba (del portátil y el por defecto). Tenga en cuenta que el teclado por defecto puede ser cualquier otro teclado distinto a los configurados en el fichero.

# ¤==============================¤==========================================================¤
# |                              |                  Configuración Principal                 |
# |     Teclados ordenados       :========:=============:============:======================¤
# |      por preferencia         | Layout |    Model    |   Variant  |         Option       |
# ¤==============================:========:=============:============:======================¤
# | SINO WEALTH Gaming KB*       |   us   |     pc86    | altgr-intl |                      |
# ¤------------------------------¤--------¤-------------¤------------¤                      |
# | AT Translated Set 2 keyboard | latam  |   presario  | deadtilde  | grp:alt_shift_toogle |
# |                              |        | acer_laptop |            |
# ¤------------------------------¤--------¤-------------¤------------¤                      |
# | system-keyboard              |   us   |     pc86    | altgr-intl |                      |
# ¤==============================¤========¤=============¤============¤======================¤

# ⌨️ Teclado por defecto.
Section  "InputClass"
	Identifier  "system-keyboard"
	MatchIsKeyboard  "on"
		# Distribución estadounidense (default), latina o española.
		Option  "XkbLayout"   "us,latam,es"
		# Modelo Acer Laptop.
		Option	"XkbModel"      "acer_laptop,acer_laptop,acer_laptop"
		# Modelo Compaq Presario/HP1000/HP elitebook Folio 9480m Laptop.
		#Option	"XkbModel"      "presario,presario,presario"
		# Incluir tilde muerta según distribución de teclado.
		Option  "XkbVariant"  "altgr-intl,deadtilde,deadtilde"
		# Alt+Shift para cambiar de distribución de teclado.
		Option  "XkbOptions"  "grp:alt_shift_toogle"
EndSection


# ⌨️ Teclado del portátil.
Section	 "InputClass"
	Identifier  "AT Translated Set 2 keyboard"
	MatchIsKeyboard  "on"
		# Distribución latina (default), española o estadounidense.
		Option	"XkbLayout"     "latam,es,us"
		# Distribución española (default), latina o estadounidense.
#		Option	"XkbLayout"     "es,latam,us"
        # Modelo Acer Laptop.
		Option	"XkbModel"      "acer_laptop,acer_laptop,acer_laptop"
		# Modelo Compaq Presario/HP1000/HP elitebook Folio 9480m Laptop.
		#Option	"XkbModel"      "presario,presario,presario"
		# Incluir tilde muerta según distribución de teclado.
		Option	"XkbVariant"    "deadtilde,deadtilde,altgr-intl"
		# Alt+Shift para cambiar de distribución de teclado.
		Option	"XkbOptions"    "grp:alt_shift_toogle"
EndSection

# ⌨️ Teclado usb KUMARA K552-KR.
Section  "InputClass"
	Identifier  "SINO WEALTH Gaming KB"
	MatchIsKeyboard  "on"
		# Distribución estadounidense (default), latina o española.
		Option	"XkbLayout"     "us,latam,es"		
		# Modelo genérico de 86 teclas.
		Option	"XkbModel"      "pc86,pc86,pc86"		
		# Incluir tilde muerta según distribución de teclado.
		Option	"XkbVariant"    "altgr-intl,deadtilde,deadtilde"		
		# Alt+Shift para cambiar de distribución de teclado.
		Option	"XkbOptions"    "grp:alt_shift_toogle"
EndSection

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

## AMD Graphics

### modesetting

```sh
pacman -S {lib32-,}mesa{,-vdpau} {,lib32-}amdvlk libva-{mesa-driver,utils} vdpauinfo radeontop --needed --noconfirm
```

### amdgpu

```sh
pacman -S xf86-video-amdgpu {lib32-,}mesa{,-vdpau} {,lib32-}amdvlk libva-{mesa-driver,utils} vdpauinfo radeontop --needed --noconfirm
```

```sh
bash -c 'cat > /etc/X11/xorg.conf.d/20-amdgpu.conf' << EOF
# 🌟🌟🌟 Fichero de configuración de gráficas AMDGPU. Aplica desde la familia Volcanic Island en adelante (>= Radeon 300) 🌟🌟🌟
# 🌐 https://es.wikipedia.org/wiki/Serie_Radeon_300
# 📜 https://man.archlinux.org/man/amdgpu.4
# 🌐 https://wiki.archlinux.org/title/AMDGPU#Xorg_configuration

# 📜 https://man.archlinux.org/man/amdgpu.4#Option~6
Section "OutputClass"
    Identifier "AMD"
    MatchDriver "amdgpu"
    Driver "amdgpu"
    Option "TearFree" "true"        
EndSection
EOF
```



### radeon

```sh
pacman -S xf86-video-ati {lib32-,}mesa{,-vdpau} libva-{mesa-driver,utils} vdpauinfo radeontop --needed --needed --noconfirm
```

```sh
bash -c 'cat > /etc/X11/xorg.conf.d/20-radeon.conf' << EOF
# 🌟🌟🌟 Fichero de configuración de gráficas AMD ATI. Aplica desde la familia r100 (2000), hasta la mullins (2014) 🌟🌟🌟
# 📜 man-page: https://man.archlinux.org/man/radeon.4#SUPPORTED_HARDWARE
# 🌐 wiki: https://wiki.archlinux.org/title/ATI#Installation
# 🌐 wikipedia: https://en.wikipedia.org/wiki/Radeon_R100_series
# 🌐 wikipedia: https://es.wikipedia.org/wiki/AMD_Accelerated_Processing_Unit#Beema_&_Mullins_(28nm)
# 🌐 wiki: https://wiki.archlinux.org/title/OpenGL
# 🔗 Fuente externa: https://technical.city/en/video/Radeon-R6-Mullins

# 💻 AMD ATI/Radeon Graphics.
Section "OutputClass"
    Identifier "Radeon"
    MatchDriver "radeon"
    Driver "radeon"
        Option  "AccelMethod"    "glamor"    # Arquitectura de aceleración glamor.
#        Option  "TearFree"       "on"        # Evitar desgarre de pantalla (tearing).
        Option  "DRI"            "3"         # Habilitar DRI3.
        Option  "ColorTiling"    "on"        # Habilitar ColorTiling.
        Option  "ColorTiling2D"  "on"        # Habilitar ColorTiling2D.
EndSection

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
lsmod | grep "kms\|drm"
----------------------------------------------------------------
drm_buddy              20480  1 amdgpu
drm_ttm_helper         16384  2 amdgpu,radeon
ttm                    94208  3 amdgpu,radeon,drm_ttm_helper
drm_display_helper    184320  2 amdgpu,radeon
cec                    81920  1 drm_display_helper
```

`amdgpu` cargado innecesariamente

```sh
#nvim /etc/modprobe.d/amdgpu.conf
----------------------------------------------------------------
blacklist amdgpu
```

`radeon` cargado innecesariamente

```sh
nvim /etc/modprobe.d/radeon.conf
-----------------------------------------------------------------------
blacklist radeon
```



# Touchpad

```sh
bash -c 'cat >> /etc/X11/xorg.conf.d/30-touchpad.conf' << EOF
# 🌟🌟🌟❌🖱️ Fichero de configuración del touchpad en Xorg 🌟🌟🌟
# 📂 /etc/X11/xorg.conf.d/30-touchpad.conf
# 🌐 wiki: https://wiki.archlinux.org/title/Libinput#Tapping_button_re-mapping
# 📜 man-page: https://man.archlinux.org/man/libinput.4#CONFIGURATION_DETAILS

# 🖱️💻 Touchpad laptop.
Section  "InputClass"
	Identifier  "touchpad"
	Driver  "libinput"
	MatchIsTouchpad  "on"
	MatchDevicePath  "/dev/input/event*"
		# Habilitar click al toque.
		Option  "Tapping"             "true"
		# Velocidad táctil.
		Option  "AccelSpeed"          "0.2"
		# Habilitar desplazamiento natural.
		Option  "NaturalScrolling"    "true"
		# Deshabilitar touchpad mientras se digita en el teclado.
#		Option  "DisableWhileTyping"  "true"
		# Habilitar click derecho y medio con dos y tres toques respectivamente.
		Option  "TappingButtonMap"    "lrm"
		# Habilitar tocar y arrastrar.
		Option  "TappingDrag"         "true"
		# Habilitar bloqueo de arrastre (arrastrar sin soltar).
		Option  "TappingDragLock"     "true"
		# Desactivar touchpad mientras esté conectado un mouse externo.
		Option  "SendEventsMode"      "disabled-on-external-mouse"		
EndSection

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Iniciar sesión con el usuario

```sh
exit #host
dakataca #host
```

# Tecnologías de gráficos *Intel*

Si su gráfica *Intel* es compatible, se recomienda implementar también las tecnologías de gráficos de [aceleración de video por hardware&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#aceleraci%C3%B3n-de-video-por-hardware), [OpenGL&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#opengl), [OpenCL&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#opencl), [Vulkan&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#vulkan) y [Gstreamer&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md#gstreamer) para aprovechar al máximo las capacidades de nuestra GPU *Intel* usando nuestra [guía de gráficos *Intel*&#x2197;](https://gitlab.com/dakataca/guides/-/blob/main/Intel%20Graphics/Intel%20Graphics.md).



