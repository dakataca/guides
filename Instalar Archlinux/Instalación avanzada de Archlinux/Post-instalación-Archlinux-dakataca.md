# 2 Post-instalación Archlinux

Autor: [@dakataca](https://t.me/dakataca)



## Pipewire

```sh
sudo pacman -S lib32-pipewire pipewire{,-{alsa,pulse,jack}} rtkit wireplumber libsamplerate alsa-{utils,plugins} --needed
```

```sh
systemctl --user enable pipewire.service
```

### Remuestreo de alta calidad

```sh
su
bash -c 'cat >> /etc/asound.conf' << EOF
# 🌟 Remuestreo de calidad de audio alsa, requiere paquete alsa-plugins.
# 🌐 wiki: https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture#High_quality_resampling
# 💡 Tip: Ideal para procesadores multinucleo (4+) de decente/alto rendimiento.

#defaults.pcm.rate_converter "samplerate_best"     # No recomendable, requiere libsamplerate.
#defaults.pcm.rate_converter "speexrate_medium"
defaults.pcm.rate_converter "speexrate_best"
EOF
```



### Partición Windows (Música windows)

```sh
su
bash -c 'cat >> /etc/fstab' << EOF
# 🌟🪟 Montar automáticamente partición Windows.
# 🌐 wiki: https://wiki.archlinux.org/title/NTFS-3G#Allowing_group/user

/dev/sda6   /home/dakataca/Music/deemix   ntfs-3g   defaults,uid=1000,gid=1000    0   0
EOF
```



## Administrar batería interna (si la tiene)

```sh
pacman -S tlp bash-completion ethtool smartmontools --needed --noconfirm
```

### Procesador

```sh
bash -c 'cat > /etc/tlp.d/01-processor.conf' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía del procesador en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/processor.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/01-processor.conf  👑
#   sudo tlp-stat --processor

#   https://linrunner.de/tlp/settings/processor.html#cpu-scaling-governor-on-ac-bat
#   CPU frequency autoscaling regulator.
#   [es]: Regulador de escalado automático de frecuencia de CPU.
#   conservative, ondemand, userspace, powersave, performance, schedutil
CPU_SCALING_GOVERNOR_ON_AC=performance
CPU_SCALING_GOVERNOR_ON_BAT=powersave

#       📂               

#   https://linrunner.de/tlp/settings/processor.html#cpu-energy-perf-policy-on-ac-bat
#   CPU energy/performance policies (in order of increasing power saving).
#   [es]: Políticas de energía/rendimiento de la CPU (en orden de aumento del ahorro de energía)
#   performance, balance_performance, default, balance_power, power.
CPU_ENERGY_PERF_POLICY_ON_AC=performance
CPU_ENERGY_PERF_POLICY_ON_BAT=power

#   https://linrunner.de/tlp/settings/processor.html#cpu-min-max-perf-on-ac-bat
#   Define the percentaje (0..100%), min/max of P-state for Intel CPUs
#   This setting is intended to limit the power dissipation of the CPU.
#   [es]: Porcentaje (0..100%), del estado P mín/max para CPUs Intel > 2nd gen "Sandy Bridge".
#   [es]: Ésta configuración está destinada a limitar la disipación de energía de la CPU.
#   sudo tlp-stat -p -v | grep "min_perf_pct"
#   0 - 100
CPU_MIN_PERF_ON_AC=0
CPU_MAX_PERF_ON_AC=100
CPU_MIN_PERF_ON_BAT=0
CPU_MAX_PERF_ON_BAT=$([ $(sed -En '/min_perf_pct/s/.+ ([0-9]{1,3}) .+/\1/p' <<<$(sudo tlp-stat -p)) ] ||  echo '75')

#   https://linrunner.de/tlp/settings/processor.html#cpu-boost-on-ac-bat
#   Configure CPU “turbo boost” (Intel) or “turbo core” (AMD) feature (0 = disable / 1 = allow).
#   [es]: Configure la función “turbo boost” (Intel) o “turbo core” (AMD) de la CPU (0 = desactivar / 1 = permitir).
#   A value of 1 does not activate boosting, it just allows it.
#   [es]: Un valor de 1 no activa el impulso, simplemente lo permite.
CPU_BOOST_ON_AC=1
CPU_BOOST_ON_BAT=0
EOF
```

### Disco y controladores

```sh
sudo tlp-stat --disk
```

> `sda`

```sh
bash -c 'cat > /etc/tlp.d/02-disk_and_controllers.conf' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de discos y controladores en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/disks.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/02-disk_and_controllers.conf 👑
#   sudo tlp-stat --disk

#   https://linrunner.de/tlp/settings/disks.html#disk-devices
#   Disks on which the power-saving settings will be applied.
#   [es]: Discos en los que se aplicarán las configuraciones de ahorro de energía.
DISK_DEVICES="sda"

#   https://linrunner.de/tlp/settings/disks.html#disk-iosched
#   Disk I/O Scheduler.
#   [es]: Programador de E/S por disco.
#   Possible values: Multi queue (blk-mq) schedulers: mq-deadline (*), none, kyber, bfq, keep.
DISK_IOSCHED="bfq mq-deadline"
EOF
```

### Gráfica AMDGPU

```sh
bash -c 'cat > /etc/tlp.d/03-graphics.conf' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de la gráfica AMDGPU en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/graphics.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/03-graphics.conf 👑
#   sudo tlp-stat --graphics

#   https://linrunner.de/tlp/settings/graphics.html#radeon-dpm-state-on-ac-bat
#   Controls the power management method of the GPU AMDGPU.
#   [es]: Controla el método de administración de energía de la GPU AMDGPU.
#   Possible values: battery, balanced, performance

RADEON_DPM_STATE_ON_AC=performance
RADEON_DPM_STATE_ON_BAT=battery
EOF
```

### Gráfica AMD ATI

```sh
bash -c 'cat > /etc/tlp.d/03-graphics' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de la gráfica AMD ATI en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/graphics.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/03-graphics.conf 👑
#   sudo tlp-stat --graphics

#   https://linrunner.de/tlp/settings/graphics.html#radeon-power-profile-on-ac-bat
#   Control the clock of the AMD ATI GPU (legacy).
#   [es]: Controla el reloj de la GPU AMD ATI (heredado).
#   Possible values: low, mid, high, auto – high on AC, mid on battery, default – use hardware defaults.

RADEON_POWER_PROFILE_ON_AC=high
RADEON_POWER_PROFILE_ON_BAT=mid
EOF
```



### Grágica Intel

```sh
bash -c 'cat > /etc/tlp.d/03-graphics' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de la gráfica Intel en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/graphics.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/03-graphics.conf 👑
#   sudo tlp-stat --graphics

#   https://linrunner.de/tlp/settings/graphics.html#intel-gpu
#   Min/Max/Turbo Frequency for Intel GPUs.
#   [es]: Frecuencia mínima/máxima/turbo para GPUs intel.
#   Get Minimum and maximum megahertz frequency of the processor:
#   grep -E "GPU m(in|ax)" <<<`sudo tlp-stat --graphics`
#   [es]: Solo debe establecer valores dentro del rango "GPU min" a "GPU max".
INTEL_GPU_MIN_FREQ_ON_AC=$(sed -En "/GPU min/s/.+ ([0-9]+) .+/\1/p" <<<`sudo tlp-stat --graphics`)
INTEL_GPU_MIN_FREQ_ON_BAT=200
INTEL_GPU_MAX_FREQ_ON_AC=$(sed -En "/GPU max/s/.+ ([0-9]+) .+/\1/p" <<<`sudo tlp-stat --graphics`)
INTEL_GPU_MAX_FREQ_ON_BAT=700
INTEL_GPU_BOOST_FREQ_ON_AC=1100
INTEL_GPU_BOOST_FREQ_ON_BAT=1000
EOF
```

### Dispositivos de radio

```sh
bash -c 'cat > /etc/tlp.d/04-radio_device_wizard.conf' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de dispositivos de radio en TLP. 🌟🌟🌟 
#   https://linrunner.de/tlp/settings/rdw.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/04-radio_device_wizard.conf
#   tlp-stat --rfkill

#   https://linrunner.de/tlp/settings/rdw.html#devices-to-enable-on-disconnect
#   Disable on Network Connect.
#   [es]: Deshabilitar bluetooth, wwan o wifi en conexión de red.
#   Possible values: bluetooth, wifi, wwan.
DEVICES_TO_DISABLE_ON_LAN_CONNECT="wifi wwan"
DEVICES_TO_DISABLE_ON_WIFI_CONNECT="wwan"
DEVICES_TO_DISABLE_ON_WWAN_CONNECT="wifi"

DEVICES_TO_ENABLE_ON_LAN_DISCONNECT="wifi wwan"
DEVICES_TO_ENABLE_ON_WIFI_DISCONNECT=""
DEVICES_TO_ENABLE_ON_WWAN_DISCONNECT=""
EOF
```

### Umbrales de carga

```sh
bash -c 'cat > /etc/tlp.d/05-charge_umbral.conf' << EOF
# 🌟🌟🌟🔋 Configuración del ahorro de energía de dispositivos de radio en TLP. 🌟🌟🌟 
# 🌐 https://wiki.archlinux.org/title/TLP#Configuration
#   https://linrunner.de/tlp/settings/rdw.html
#   https://linrunner.de/tlp/usage/tlp-stat.html
# 📂 /etc/tlp.d/05-charge_umbral.conf
#   https://linrunner.de/tlp/settings/battery.html#start-stop-charge-thresh-batx

## Batería principal.
START_CHARGE_THRESH_BAT0=15   # Umbral de inicio de carga.
STOP_CHARGE_THRESH_BAT0=95    # Umbral de parada de carga.

## Batería secundaria.
START_CHARGE_THRESH_BAT1=20   # Umbral de inicio de carga.
STOP_CHARGE_THRESH_BAT1=90    # Umbral de parada de carga.
EOF
```

```sh
#tlp-stat
tlp-stat -b
```

```sh
systemctl mask systemd-rfkill.{socket,service}
systemctl enable --now tlp.service
```



## timesyncd

```sh
bash -c 'cat > /etc/timesync.conf' << EOF
# 🌟🌟🌟 Edite la configuración del servidor NTP de systemd 💻 🌟🌟🌟
# 📂 /etc/timesync.conf 👑📝
# 🌐 wiki: https://wiki.archlinux.org/title/systemd-timesyncd
# 📝 doc: https://man.archlinux.org/man/timesyncd.conf.5

[Time]
NTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org
FallbackNTP=0.pool.ntp.org 1.pool.ntp.org 0.fr.pool.ntp.org

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Habilite e inicie el servicio

```sh
timedatectl set-ntp true
```



Verificar servicios

```sh
systemctl --failed
systemd-analyze blame
#systemctl --user --type=service
systemctl --type=service
```



## [Mouse externo](https://man.archlinux.org/man/xorg.conf.5#INPUTDEVICE_SECTION)

### Listar dispositivos apuntadores:

#### Identifier

```sh
xinput --list | grep 'MOUSE\|TouchPad'
..............................................................
 ↳ SynPS/2 Synaptics TouchPad      id=16   [slave  pointer  (2)]
 ↳ USB OPTICAL MOUSE               id=9    [slave  pointer  (2)]
```

`Identifier "USB OPTICAL MOUSE"`



#### Establecer `MatchProduct` y `MatchVendor`

Listar dispositivos conectados a puertos usb.

```sh
lsusb -v 2>/dev/null | grep -IB1 "idProduct .* Mouse"
..............................................................
idVendor           0x093a Pixart Imaging, Inc.
idProduct          0x2521 Optical Mouse
```

`MatchProduct "Optical Mouse"`
`MatchVendor "Pixart Imaging, Inc."`



### [Aceleración del mouse](https://wiki.archlinux.org/title/Mouse_acceleration#In_Xorg_configuration)

```sh
su
bash -c 'cat > /etc/X11/xorg.conf.d/50-mouse-acceleration.conf' << EOF
# 🌟🌟🌟❌🖱️ Fichero de configuración de la aceleración del mouse externo en Xorg 🌟🌟🌟 
# 📂 /etc/X11/xorg.conf.d/50-mouse-acceleration.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Mouse_acceleration#Setting_mouse_acceleration
# 📜 man-page: https://man.archlinux.org/man/xorg.conf.5#POINTER_ACCELERATION
# Cada "InputClass" puede anular la configuración de una clase anterior (arriba).
# Es conveniente organizar primero las "Section"s con las coincidencias más genéricas.

#🖱️ Configuración genérica de la aceleración de un mouse externo en Xorg.
Section  "InputClass"
    Identifier  "system-mouse"
    MatchIsPointer  "yes"
        # Establezca lo siguiente en 1 1 0 respectivamente, para deshabilitar la aceleración.
        # Numerador y denominador del factor de aceleración. Razonable hasta "5".
        Option  "AccelerationNumerator"   "2"
        Option  "AccelerationDenominator" "1"
        # Umbral de aceleración.
        Option  "AccelerationThreshold"   "4"
EndSection

#🖱️ Configuración de la aceleración del mouse externo DELUX M618 en Xorg.
Section  "InputClass"
    Identifier  "USB OPTICAL MOUSE"
    MatchProduct  "Optical Mouse"
    MatchVendor  "Pixart Imaging, Inc."
    MatchIsPointer "yes"
        # Establezca lo siguiente en 1 1 0 respectivamente, para deshabilitar la aceleración.
        # Numerador y denominador del factor de aceleración. Razonable hasta "5".
        Option  "AccelerationNumerator"   "2"
        Option  "AccelerationDenominator" "1"
        # Umbral de aceleración.
        Option  "AccelerationThreshold"   "4"
EndSection

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
bash -c 'cat > /etc/X11/xorg.conf.d/50-mouse-deceleration.conf' << EOF
# 🌟🌟🌟❌🖱️ Fichero de configuración de la desaceleración del mouse externo en Xorg 🌟🌟🌟 
# 📂 /etc/X11/xorg.conf.d/50-mouse-deceleration.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Mouse_acceleration#Setting_mouse_acceleration
# 📜 man-page: https://man.archlinux.org/man/xorg.conf.5#POINTER_ACCELERATION
# Cada "InputClass" puede anular la configuración de una clase anterior (arriba).
# Es conveniente organizar primero las "Section"s con las coincidencias más genéricas.

#🖱️ Configuración genérica de la desaceleración de un mouse externo en Xorg.
Section  "InputClass"
    Identifier  "system-mouse"
    MatchIsPointer  "yes"
        # Desacelerar puntero de forma adaptativa cuando vaya lento.
        Option  "AdaptiveDeceleration"  "2"
        # Desacelerar puntero de forma constante cuando vaya lento.
#       Option  "ConstantDeceleration"  "2"
EndSection

#🖱️ Configuración de la desaceleración del mouse externo DELUX M618 en Xorg.
Section "InputClass"
    Identifier "USB OPTICAL MOUSE"
    MatchProduct "Optical Mouse"
    MatchVendor "Pixart Imaging, Inc."
    MatchIsPointer "yes"
        # Desacelerar puntero de forma adaptativa cuando vaya lento.
        Option "AdaptiveDeceleration" "2"
        # Desacelerar puntero de forma constante cuando vaya lento.
#       Option "ConstantDeceleration" "2"
EndSection

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Systemd logind

```sh
nvim /etc/systemd/sleep.conf
--------------------------------------------------------------
## Edite el configuración de los etados del administrador de sesión systemd-logind.
# 📂 /etc/systemd/sleep.conf 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Power_management#Hybrid-sleep_on_suspend_or_hibernation_request
# 📜 man-page: https://man.archlinux.org/man/systemd-sleep.conf.5

[Sleep]
#AllowSuspend=no               # Suspensión (ram).
AllowSuspendThenHibernate=no  # Suspender (ram), si llega a batería baja, hibernar (disk).
AllowHybridSleep=yes          # Híbrido (suspender/hibernar).
```



## Makepkg

Habilitar multihilo en `makepkg.conf`:

```sh
sed -Ei.back 's/^#(MAKEFLAGS=\"-j)[0-9]+/\1\$(nproc)/ ; /^COMPRESSGZ=\(gzip -c -f -n\)$/ s/gzip/pigz/ ; s/^(COMPRESSBZ2=\()(bzip2 -c -f\))/\1p\2/ ; s/^(COMPRESSXZ=\(xz -c -z -)\)/\1 --threads=0)/' /etc/makepkg.conf
```



### Aria2 en makepkg y reflector

```sh
pacman -S aria2 wget reflector --needed --noconfirm
```

```sh
sed -Ei.back "s,(/usr/bin/)curl.+ (-o %o %u),\1aria2c -UWget -s4 \2,g" /etc/makepkg.conf
head -20 /etc/makepkg.conf
```

```sh
bash -c 'cat > /etc/xdg/reflector/reflector.conf' << EOF
# 🌟🌟🌟🪞 Fichero de configuración del adminstrador de espejos de descarga reflector 🌟🌟🌟 
# 📂 /etc/xdg/reflector/reflector.conf 👑
# 📝 doc: https://xyne.dev/projects/reflector/
# 📜 man-page: https://man.archlinux.org/man/reflector.1#EXAMPLES

# Guarde la lista de réplicas en el fichero asignado.
--save /etc/pacman.d/mirrorlist

# Use los protocolos de red especificados.
--protocol http,https,rsync

# Ordene la lista de espejos por velocidad.
--sort rate

# Porcentaje mínimo de plenitud de paquetes disponibles en el espejo.
--completion-percent 98

# Número de segundos que se debe esperar, antes de que se agote el tiempo de espera de una conexión.
--connection-timeout 9

# Número de segundos que se debe esperar, antes de que se agote el tiempo de espera de una descarga.
--download-timeout 9

# Tiempo de espera de caché en segundos para los datos recuperados de la API de estado de réplica.
--cache-timeout 9

# Utilice n subprocesos para calificar los espejos.
--threads $(nproc)

# Limite la lista a los n servidores sincronizados más recientemente.
--latest 40

# Devuelve los n espejos más rápidos que cumplan los otros criterios. No utilice ésta opción sin otra opción de filtrado.
--fastest 40

# Solo devuelven réplicas que se hayan sincronizado en las últimas n horas. n puede ser un número entero o un decimal.
--age 12

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
systemctl enable reflector.timer
```



## Paru

```sh
exit
```

```sh
cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd
```





## Pacdiff

Gestionar ficheros .pacnew y .pacsave:

```sh
pacman -S plocate pacman-contrib --needed --noconfirm
```



##### nvim como herramienta de diferenciación de pacdiff

```sh
bash -c 'cat >> /etc/environment' << EOF
# 🌟🌟🌟🐚 Establezca la varibale de entorno global, de la Herramienta de diferenciación de nvim para script pacdiff 🌟🌟🌟 
# 📂 /etc/environment 👑📝

# 🌐 wiki: https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave#pacdiff
# 🌐 wiki: https://wiki.archlinux.org/title/Locate#Usage
# 📜 man-page: https://man.archlinux.org/man/pacdiff.8#ENVIRONMENT.
# 📝 doc: https://neovim.io/doc/user/diff.html
DIFFPROG="nvim -d"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
updatedb  # https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave#Locating_.pac*_files
#pacdiff -l
```



## Fuentes y herramientas necesarias

```sh
pacman -S acpi unicode-character-database gnu-free-fonts wqy-microhei noto-fonts-emoji ttf-{font-awesome,inconsolata,dejavu,hack,roboto,liberation,fira-{sans,code},cascadia-code,nerd-fonts-symbols} hunspell-es_co zip un{zip,rar,arj} arj cpio p{7zip,bzip2,igz} l{zop,zip,rzip,ha} xarchiver breeze{,-gtk} android-{tools,file-transfer} heimdall bc nmap speedtest-cli tldr man-pages-es cmus mediainfo yt-dlp breeze{,-gtk} mousepad mplayer mpv{,-mpris} atomicparsley vlc peek telegram-desktop scrot cmus gpick imagemagick expac --needed --noconfirm
```



### Administrador de ficheros y Visualizador de imágenes y miniaturas TUI

#### Yazi

Instalas yazi y todas sus dependencias opcionales:

```sh
pacman -S ueberzugpp --needed --asdeps $(expac -1S "%n\n%o" yazi)
```



### Administrador de ficheros y Visualizador de imágenes y miniaturas GUI

#### Visualizador y miniaturas

```sh
pacman -S ffmpegthumbnailer libgsf ristretto --needed --noconfirm
```



##### PcmanFm

```sh
pacman -S pcmanfm-gtk3 --needed --noconfirm
```



##### Thunar

```sh
pacman -S thunar{,-{volman,{archive,media-tags}-plugin}} tumbler --needed --noconfirm
```

Terminal thunar

```sh
mkdir -p ~/.config/xfce4/
cat > ~/.config/xfce4/helpers.rc << EOF
# 🌟 Terminal y navegador por defecto de Thunar.
# 📂 ~/.config/xfce4/helpers.rc 👤
# 🌐 wiki: https://wiki.archlinux.org/title/thunar#Open_Terminal_Here

#TerminalEmulator=foot
TerminalEmulator=alacritty
#WebBrowser=chromium

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
xdg-mime default thunar.desktop inode/directory
```



### mpv

```sh
bash -c 'cat > /etc/mpv/mpv.conf' << EOF
# 🌟🌟🌟 Fichero de configuración del reproductor mpv con VAAPI/VDPAU y streaming usando yt-dlp 🌟🌟🌟
# 📂 /etc/mpv/mpv.conf 👑

# 🌟🌟  References 🌟🌟
# 🌐 wiki: https://wiki.archlinux.org/title/Mpv#Hardware_video_acceleration
# 📜 man-page: https://man.archlinux.org/man/mpv.1#CONFIGURATION_FILES
# 📝 doc: https://mpv.io/manual/stable/
# 🔗 Fuente externa: https://github.com/hl2guide/better-mpv-config/blob/master/mpv_v2/mpv.conf
# 🔗 Fuente externa: https://github.com/Katzenwerfer/mpv-config/blob/c7f9817be45b3f23fe5608d2bacaf0172b602f8b/mpv.conf#L134


# 🌟🌟 General Configurations 🌟🌟

# Para listar opciones por defecto use por ejemplo una regex:
#  mpv --list-options | grep  '\--demuxer-max'

# 🌟 Wayland 🇼:
#gpu-context=wayland

# 🌟 Cache options 📦:
--demuxer-max-back-bytes=200MiB  # Cantidad de datos pasados que el demuxer puede conservar.
--demuxer-max-bytes=600MiB       # Cantidad de memoria máxima para almacenar en bufer por delante.

# 🌟 Subtitles options 📝:
vlang=lat,spa,eng                # Idiomas de audio y subtítulos en latino, español o inglés.

# 🌟 Audio volume, resampler, downmix and normalize 🔊:
volume=70                        # Inicie con nivel de volumen de audio específico.
audio-pitch-correction=yes       # Si no hay ningún dispositivo de audio, aplique --ao=null.
audio-resample-filter-size=32    # Longitud del filtro con respecto a la frecuencia de muestreo más baja.
audio-normalize-downmix=yes      # Habilita la normalización si el audio envolvente se mezcla a estéreo.
#af=lavfi=[loudnorm=I=-16:TP=-3:LRA=4]   # Filtro de audio Loudnorm.
#af=lavfi=[dynaudnorm=f=75:g=25:p=0.55] # Filtro de audio dynaudnorm.

# 🌟 Terminal options 💻:
term-osd-bar=yes                 # Habilita barra de progreso debajo de la línea de estado.


# 🌟 Video options 🎬:
hwdec=auto                       # Aceleración de video automática.
vo=gpu,xv,vdpau                  # Driver de video según compatibilidad.
# 📜 man-page: https://man.archlinux.org/man/mpv.1#gpu
gpu-hwdec-interop=vaapi          # Evitar error "Cannot load libcuda.so.1" al usar VAAPI.
fs=yes                           # Inicie videos a pantalla completa.


# 🌟🌟 Video Filters 🌟🌟

# 📝 doc: https://mpv.io/manual/stable/#options-deband-iterations
# 🌟 Debanding:
deband=yes                       # Algoritmo de eliminación de bloqueos y bandas visibles.
deband-iterations=4              # Número de pasos de eliminación de banda por muestra.
deband-threshold=30              # Umbral de corte del filtro.
deband-range=13                  # Radio inicial del filtro de disolución.
deband-grain=45                  # Añade un poco de ruido para cubrir artefactos restantes.

# 🌟🌟  Profiles 🌟🌟
# 👽 Custom profiles for streaming mpv with yt-dlp 🎥.
# Use: mpv --profile=name-profile

# 🌟 Default profile:
# 🌐 wiki: https://wiki.archlinux.org/title/mpv#Custom_profiles
profile=hd                       # Perfil personalizado por defecto.
#profile=hq

# 🍿 Hight Quality. Solo para gráficas de alto rendimiento:
[hq]
profile-desc='Alta calidad, solo con --vo=gpu'
profile=gpu-hq
scale=ewa_lanczossharp
cscale=ewa_lanczossharp

# ¤--------------------------------------------------------¤
# | Formato de perfiles para streaming mpv con yt-dlp 🎥 : |
# :--------------------------------------------------------:
# | - Codev de video avc1 preferido.                       |
# | - No soportado codec vp9.                              |
# | - Desde: VGA 📺 (640 × 360).                           |
# | - Hasta: 8K 🎦 (7680 × 4320 60fps).                    |
# ¤--------------------------------------------------------¤

# 🎦 8K 60fps:
[8k60]
profile-desc='Resolución 7680 × 4320 60fps (8K).'
## 🌐 wiki: https://wiki.archlinux.org/title/mpv#youtube-dl_and_choosing_formats
ytdl-format=bestvideo[height<=?4500][fps<=?60][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎦 8K 30fps:
[8k]
profile-desc='Resolución 7680 × 4320 30fps (8K).'
ytdl-format=bestvideo[height<=?4500][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎫 4K 60fps:
[4k60]
profile-desc='Resolución 3840 × 2160 60fps (4K).'
ytdl-format=bestvideo[height<=?2500][fps<=?60][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎫 4K 30fps:
[4k]
profile-desc='Resolución 3840 × 2160 30fps (4K).'
ytdl-format=bestvideo[height<=?2500][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 📽️ 2K 60fps:
[2k60]
profile-desc='Resolución 2560 × 1440 60fps (2K).'
ytdl-format=bestvideo[height<=?1800][fps<=?60][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 📽️ 2K 30fps:
[2k]
profile-desc='Resolución 2560 × 1440 30fps (2K).'
ytdl-format=bestvideo[height<=?1800][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎬 FHD 60fps:
[fhd60]
profile-desc='Resolución 1920 x 1080 60fps (FHD).'
ytdl-format=bestvideo[height<=?1200][fps<=?60][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎬 FHD 30fps:
[fhd]
profile-desc='Resolución 1920 x 1080 30fps (FHD).'
ytdl-format=bestvideo[height<=?1200][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎥 HD 60fps:
[hd60]
profile-desc='Resolución 1280 x 800 60fps (HD).'
ytdl-format=bestvideo[height<=?800][fps<=?60][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 🎥 HD 30fps:
[hd]
profile-desc='Resolución 1280 x 800 30fps (HD).'
#profile-restore=copy-equal # Sets the profile restore method to "copy if equal"
ytdl-format=bestvideo[height<=?800][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 📹 SD 30fps:
[sd]
profile-desc='Resolución 854 x 480 30fps.'
ytdl-format=bestvideo[height<=?500][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 📺 VGA 30fps:
[vga]
profile-desc='Resolución 640 × 360 30fps (VGA).'
ytdl-format=bestvideo[height<=?400][fps<=?30][vcodec^=avc1][vcodec!=?vp9]+bestaudio/best

# 📼 Usar compilación antigua (<=150) de x264. Solo para videos específicos:
[old]
profile-desc='Perfil para videos locales renderizados con formato antiguo de x264.'
vd-lavc-assume-old-x264=yes


# 👤 Autor: https://t.me/dakataca 💻 🐬 ###
EOF
```

`yta search term`, introduzca la siguiente fusión en su fichero configuración de usuario `~/.bashrc`:



## yt-dlp

```
exit
```

```sh
mkdir -p ~/{.config/yt-dlp/,Downloads/yt-dlp/}
cat > ~/.config/yt-dlp/config << EOF
# 🌟🌟🌟 Fichero de configuración de la herramienta de descarga de video/audio yt-dlp 🌟🌟🌟 
# 📂 ~/.config/yt-dlp/config 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Yt-dlp#Configuration
# 📜 man-page: https://man.archlinux.org/man/yt-dlp.1#Filesystem_Options:

# 🌟🌟 General Configurations 🌟🌟
--paths $HOME/Downloads/yt-dlp/    # Directorio de descarga.
--output %(title)s.%(ext)s         # Formato de nombre de fichero multimedia.
--sub-langs la,es,en               # Idioma de subtítulos latino, español o inglés.
--progress                         # Mostrar barra de progreso.
#--concurrent-fragments 2           # Descargar N fragmentos de video en paralelo.
--restrict-filenames               # Nombres de archivo a solo ASCII, evite "&" y espacios.
--no-overwrites                    # No sobreescriba ningún fichero multimedia.

# 🌟  Faster downloads
# 🌐 wiki: https://wiki.archlinux.org/title/yt-dlp#Faster_downloads
# Require package: aria2
--downloader aria2c
--downloader-args "aria2c:-x 4 -s 4 -j 4 -k 1M --log-level=info"

# 🌟 Playlist.
#--no-playlist                      # Descargar solo video, aunque URL sea una playlist.
#--yes-playlist                     # Descargar playlist si la URL es una playlist.

# 🌟 Requieren el paquete "atomicparsley":
--embed-subs                       # Incrustar subtítulos.
--embed-thumbnail                  # Incrustar miniatura como portada.
--embed-metadata                   # Incrustar metadatos.


# 🌟🌟 Formatos de descarga de audio y video 🌟🌟

# 🌟🔊 Solo audio: AAC LC.
# Para descargar solo audio debe descomentar manualmente las siguientes dos opciones, y comentar la opción de descarga de video/audio (-f "bv...").
#--extract-audio                   # Extraiga solo el audio.
#-f bestaudio[ext=m4a][abr<=48000] # Mejor calidad (m4a) y abr menor o igual a 48000.


# 🌟🎬 Codecs de Video:
# - avc1: Prefedido ✔️.
# - vp9: No soportado ❌.

# 🎦 8K 60fps, 7680 × 4320:
#-f "bv[height<=4500][ext=mp4][fps<=60][vcodec^=avc1][vcodec!=vp9][abr<=192000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎦 8K 30fps, 7680 × 4320:
#-f "bv[height<=4500][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=192000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎫 4K 60fps, 3840 × 2160:
#-f "bv[height<=2500][ext=mp4][fps<=60][vcodec^=avc1][vcodec!=vp9][abr<=192000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎫 4K 30fps, 3840 × 2160:
#-f "bv[height<=2500][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=192000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 📽️ 2K 60fps, 2560 × 1440:
#-f "bv[height<=1800][ext=mp4][fps<=60][vcodec^=avc1][vcodec!=vp9][abr<=96000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 📽️ 2K 30fps, 2560 × 1440:
#-f "bv[height<=1800][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=96000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎬 FHD 60fps, 1920 x 1080:
#-f "bv[height<=1200][ext=mp4][fps<=60][vcodec^=avc1][vcodec!=vp9][abr<=48000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎬 FHD 30fps, 1920 x 1080:
#-f "bv[height<=1200][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=48000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎥 HD 60fps, 1280 x 800:
#-f "bv[height<=1200][ext=mp4][fps<=60][vcodec^=avc1][vcodec!=vp9][abr<=44000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 🎥 HD 30fps, 1280 x 800 (default):
-f "bv[height<=800][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=44000]+ba[ext=m4a]/hd+ba[ext=m4a]"

# 📹 SD 30fps, 854 x 480:
#-f "bv[height<=500][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=44000]+ba[ext=m4a]/sd+ba[ext=m4a]"

# 📺 VGA 30fps, 640 × 360:
#-f "bv[height<=400][ext=mp4][fps<=30][vcodec^=avc1][vcodec!=vp9][abr<=44000]+ba[ext=m4a]/sd+ba[ext=m4a]"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```





## [Profile-sync-daemon](https://wiki.archlinux.org/title/Profile-sync-daemon)

```sh
sudo pacman -S profile-sync-daemon --needed --noconfirm
```

```sh
mkdir -p ~/.config/systemd/user/psd-resync.timer.d/
cat > ~/.config/systemd/user/psd-resync.timer.d/frequency.conf << EOF
# 🌟🌟🌟 Fichero de configuración a nivel de usuario del temporizador de Profile-sync-daemon 🌟🌟🌟
# 📂 ~/.config/systemd/user/psd-resync.timer.d/frequency.conf 👤
# 🌐 wiki: https://wiki.archlinux.org/title/Profile-sync-daemon#Sync_at_more_frequent_intervals

[Unit]
Description=Timer for Profile-sync-daemon - 10min

[Timer]
# Temporizador para sincronizar cada 10 minutos. Default << 60min >>.
OnUnitActiveSec=
OnUnitActiveSec=10min

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Descomentar y establecer en `yes` la variable `USE_OVERLAYFS="yes"` y los navegadores a usar en `BROSWERS=(chromium firefox opera)`:

```sh
psd
nvim ~/.config/psd/psd.conf
------------------------------------------------------------------
# 🌟🌟🌟 Edite la configuración a nivel de usuario de Profile-sync-daemon (psd) 🌟🌟🌟 
# 📂 ~/.config/psd/psd.conf 👤📝
# 🌐 wiki: https://wiki.archlinux.org/title/Profile-sync-daemon#Configuration
# Reducir la huella de memoria del espacio tmpfs de psd y acelerar las operaciones de sincronización y desincronización.
USE_OVERLAYFS="yes"

# Aplicar tmpfs en los navegadores establecidos.
BROSWERS=(chromium firefox)
...

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
```

```sh
sudo bash -c 'cat >> /etc/sudoers' << EOF

# 📂 /etc/sudoers 👑📝
# 💼 Miembros del grupo wheel no requieren password para overlayfs.
%wheel ALL=(ALL) NOPASSWD: /usr/bin/psd-overlay-helper

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
systemctl --user enable psd.service
```





## [Git config](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Configurando-Git-por-primera-vez)

```sh
git config --global user.name dakataca
git config --global user.email danieldakataca@gmail.com
git config --global core.autocrlf input
# nvim git config, solved error vimdiff use vim.
git config --global core.editor nvim
git config --global diff.tool vimdiff3
git config --global difftool.vimdiff3.path nvim
git config --global merge.tool vimdiff3
git config --global mergetool.vimdiff3.path nvim
git config --global init.defaultBranch main
```

### SSH keys

```sh
mkdir -p ~/.ssh/authorized_keys/{aur,git{lab,hub},codeberg}/
tree ~/.ssh/
```



#### SSH keys generate for Authentication & signin

```sh
ssh-keygen -t ed25519 -C "$(whoami)@$(uname -n)-$(date -I)" -f ~/.ssh/authorized_keys/aur/aur_ssh_key
ssh-keygen -t ed25519 -C "$(whoami)@$(uname -n)-$(date -I)" -f ~/.ssh/authorized_keys/gitlab/gitlab_ssh_key
ssh-keygen -t ed25519 -C "$(whoami)@$(uname -n)-$(date -I)" -f ~/.ssh/authorized_keys/github/github_ssh_key
ssh-keygen -t ed25519 -C "$(whoami)@$(uname -n)-$(date -I)" -f ~/.ssh/authorized_keys/codeberg/codeberg_ssh_key
```

```sh
cat > ~/.ssh/config << EOF
# 🌟🌟🌟 Fichero de configuración a nivel de usuario de llaves ssh, para autenticación 🌟🌟🌟 
# 📂 ~/.ssh/config 👤
# 🌐 wiki: https://wiki.archlinux.org/title/SSH_keys#Managing_multiple_keys

#🔒🌀 Llave de acceso para el servidor aur.archlinux.org.
Host aur.archlinux.org
   IdentitiesOnly yes
   IdentityFile ~/.ssh/authorized_keys/aur/aur_ssh_key

#🔒🌀 Llave de acceso para el servidor gitlab.com.
Host gitlab.com
   IdentitiesOnly yes
   IdentityFile ~/.ssh/authorized_keys/gitlab/gitlab_ssh_key

#🔒🌀 Llave de acceso para el servidor github.com.
Host github.com
   IdentitiesOnly yes
   IdentityFile ~/.ssh/authorized_keys/github/github_ssh_key

#🔒🌀 Llave de acceso para el servidor codeberg.org.
Host codeberg.org
   IdentitiesOnly yes
   IdentityFile ~/.ssh/authorized_keys/codeberg/codeberg_ssh_key

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

```sh
cat ~/.ssh/authorized_keys/aur/aur_ssh_key.pub
cat ~/.ssh/authorized_keys/gitlab/gitlab_ssh_key.pub
cat ~/.ssh/authorized_keys/github/github_ssh_key.pub
cat ~/.ssh/authorized_keys/codeberg/codeberg_ssh_key.pub
```

Inicie sesión en [aur](aur.archlinux.org/login), [gitlab](https://gitlab.com/users/sign_in) y [github](https://github.com/login), luego copie la anterior clave pública en el campo `clave pública ssh` de las configuraciones de claves ssh de su perfil respectivo ([aur](https://aur.archlinux.org/account/dakataca/edit), [gitlab](https://gitlab.com/-/profile/keys) y [github](https://github.com/settings/ssh/new)).



#### Plataformas

```sh
mkdir -p ~/Git/{Aur,Git{Hub,Lab},codeberg}/
tree ~/Git/
```



Repositorios aur

```sh
cd ~/Git/Aur/
git clone ssh://aur@aur.archlinux.org/pseint.git
git clone ssh://aur@aur.archlinux.org/pseint-bin.git
git clone ssh://aur@aur.archlinux.org/deemix-py.git
cd
```



Repositorios gitlab

```sh
cd ~/Git/GitLab/
git clone git@gitlab.com:dakataca/guides.git
git clone git@gitlab.com:dakataca/scripts.git
git clone git@gitlab.com:dakataca/dotfiles.git
git clone git@gitlab.com:dakataca/aur.git
cd
```



Repositorios de github

Por ser de microsoft lo uso solo en situaciones en que me veo forzado.



### AUR packages

##### WPS Office

```sh
yay -S wps-office ttf-wps-fonts wps-office-mui-es-es
sudo rm -r /lib/office6/mui/*_{CN,US,RU}
```

Otros paquetes aur.

```sh
paru -S cpu-x glfw-x11 fastfetch fetchit-git typora webtorrent-cli localsend-bin pseint-bin unbound-roothints
#yay -S cpu-x glfw-x11 fastfetch fetchit-git typora alacritty-themes stremio
```

```sh
mkdir -p ~/Downloads/WebTorrent/
```



### Bluetooth

```sh
sudo pacman -S bluez{,-utils} --needed --noconfirm
```

#### Bluetoothctl

```sh
bluetoothctl
[bluetooth]# power on
[bluetooth]# scan on
[bluetooth]# pair BC:2D:EF:3E:D4:A8
[bluetooth]# agent on
[bluetooth]# default-agent
[bluetooth]# connect BC:2D:EF:3E:D4:A8
[bluetooth]# scan off
[bluetooth]# discoverable off
[bluetooth]# exit
```





### Programación

```sh
sudo pacman -S code --needed --noconfirm
```



#### Java



##### NetBeans

```sh
su
pacman -S netbeans --needed --noconfirm
```


Consultar si están configuradas las fuentes:

```sh
grep -e 'Dswing.aatext=' -e 'Dawt.useSystemAAFontSettings=' /usr/etc/netbeans.conf
tail -n1 /usr/lib/netbeans/enterprise/VERSION.txt         # 16
install -D /usr/etc/netbeans.conf ~/.netbeans/16/etc/netbeans.conf
```

**Nota**: Si no hay salida debe aplicar el siguiente comando para configurar las fuentes, de lo contrario, omítalo.

###### Configurar fuentes en el fichero de configuración del usuario:

```sh
sed -Ei 's/(-J-XX\:\+IgnoreUnrecognizedVMOptions)"/\1 -J-Dawt.useSystemAAFontSettings=on -J-Dswing.aatext=true --fontsize 15"/'' ~/.netbeans/16/etc/netbeans.conf
#sed -Ei 's/^(netbeans_default_options=".*)"$/\1 -J-Dswing.aatext=TRUE -J-Dawt.useSystemAAFontSettings=on --fontsize 15"/' ~/.netbeans/16/etc/netbeans.conf
```



Sobreescribir fichero de configuración de netbeans `/usr/etc/netbeans.conf` en cada actualización:

```sh
mkdir -p /etc/pacman.d/hooks/
nvim /etc/pacman.d/hooks/10-netbeans.hook
---------------------------------------------------------------
# 🌐 wiki: https://wiki.archlinux.org/title/Netbeans#Preserving_configuration_changes

[Trigger]
Type = Path
Operation = Install
Operation = Upgrade
Target = usr/etc/netbeans.conf

[Action]
Description = Updating netbeans.conf
When = PostTransaction
Exec = /usr/bin/sed -Ei 's/(-J-XX\:\+IgnoreUnrecognizedVMOptions)"/\1 -J-Dawt.useSystemAAFontSettings=on -J-Dswing.aatext=true --fontsize 15"/' /usr/etc/netbeans.conf
#Exec = /usr/bin/sed -Ei 's/^(netbeans_default_options=".*)"$/\1 -J-Dswing.aatext=TRUE -J-Dawt.useSystemAAFontSettings=on --fontsize 15"/' /usr/etc/netbeans.conf
```



### Funciones

```sh
cat <<'EOF' > ~/.bash_functions
#cat > ~/.bash_functions << EOF
# 🐚 Buscar y reproducir desde la terminal audios de youtube usando yta search "terms".
# 📂 ~/.bashrc 👤📝
# 🌐 wiki: https://wiki.archlinux.org/title/Mpv#youtube-dl_audio_with_search
function yta() {
    mpv --ytdl-format=bestaudio ytdl://ytsearch:"$*"
}

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



### Aliases

Fichero independiente para los alias del usuario.

```sh
cat <<'EOF' > ~/.bash_aliases
# 🌟🌟🌟👤 Fichero de configuración de los alias del usuario 🌟🌟🌟
# ~/.bash_aliases 👤
alias grep='grep --color=auto'
alias lt='lsd --tree'
alias ll='lsd -lh'
alias la='lsd -a'
alias lla='lsd -lha'
alias ls='lsd'
alias webtorrent='webtorrent --out "$HOME/Downloads/WebTorrent/"'
alias ip='ip -c'
alias ncm='ncmpcpp'

# 👤 Autor: https://t.me/dakataca 💻 🐬 ###
EOF
```

```bash
cat <<'EOF' >> ~/.bashrc
# 🌟🌟👤 Edite la configuración de ejecuciones automáticas a nivel de usuario.

files=('aliases' 'functions')

for file in "${files[@]}"; do
    [ -f ~/.bash_$file ] && . ~/.bash_$file
done
EOF
```

Aplicar configuración a la sesión actual

```sh
source ~/.bashrc
source ~/.bash_aliases
source ~/.bash_functions
```
