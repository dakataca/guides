

[toc]

# PostgreSQL

## Limpiar instalación previa

Si tiene una instalación fallida previa o simplemente quiere asegurarse de hacer una instalación limpia ejecute:

```sh
sudo rm -rf /var/{lib/{pgadmin,postgres},log/pgadmin}
```

```sh
sudo pacman -Rnsc postgresql --noconfirm
```

xxxxxxxxxx resolvconf -uexitsh

## Instalar PostgreSQL

```sh
sudo pacman -S postgresql --needed --noconfirm
```



## Configuración

### Cluster de base de datos

Cambiarse al usuario de `PostgreSQL`:

```sh
$ sudo -iu postgres
```

> **Nota**: Si falla use en su lugar:
>
> 1. `su`
> 2. `su -l postgres`

Inicializar cluster de base de datos:

```sh
[postgres]$ initdb -D /var/lib/postgres/data --data-checksums
```

Salir del usuario `postgres` y regresar al usuario normal:

```sh
exit
```



### PostGres en sistema de ficheros BTRFS

Si la base de datos reside en un sistema de ficheros [Btrfs](https://wiki.archlinux.org/title/Btrfs), debe considerar deshabilitar [Copiar en escritura](https://wiki.archlinux.org/title/Btrfs#Copy-on-Write_(CoW)) para el directorio `/var/lib/postgres/data` antes de crear cualquier base de datos.

```sh
sudo chattr +C /var/lib/postgres/data
```



### Autenticación de usuarios

Permitir el acceso solo a roles/usuarios de la base de datos `postgres`:

```sh
sudo nvim /var/lib/postgres/data/pg_hba.conf
-------------------------------------------------------------------------------
# 🌟🌟🌟🗃️ Editar la configuración del Servidor de Base de Datos PostGreSQL 🌟🌟🌟
# 📂 /var/lib/postgres/data/pg_hba.conf 👑📝
# 🌐 wiki: https://wiki.archlinux.org/title/PostgreSQL#Restricts_access_rights_to_the_database_superuser_by_default
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only.
# "peer" Usuario que está intentando conectarse al servidor de PostgreSQL debe tener el mismo nombre que el usuario del sistema operativo que está ejecutando el servidor.
#¤===============:================================================================¤
#|   Método de   |                                                                |
#| autenticación |                       Descripción                              |
#:===============:================================================================:
#|      md5      | Usando contraseña previamente establecida.                     |
#|---------------:----------------------------------------------------------------|
#|     peer      | Utiliza autenticación del sistema operativo. Usuario que se    |
#|               | conecta al servidor PostgreSQL debe existir en linux.          |
#|---------------:----------------------------------------------------------------|
#|     trust     | No requiere autenticación.                                     |
#|---------------:----------------------------------------------------------------|
#|               | Rechaza cualquier conexión sin autenticar al usuario. Útil para|
#|    reject     | para denegar explícitamente el acceso a ciertos usuarios       |
#|               | o direcciones IP.                                              |
#¤---------------'----------------------------------------------------------------¤

# Comentar línea.
#local   all             all                                     trust

# Allow "dakataca" to connect using peer authentication.
local   all     dakataca        peer

# Allow "postgres" to connect using peer authentication.
local   all     postgres        peer
```



### Accesible solo desde localhost

```sh
sudo nvim /var/lib/postgres/data/postgresql.conf
--------------------------------------------------------------------------
...
# 🌟🌟🌟 Editar fichero para el acceso a PostGreSQL solo desde localhost.
# 📂 /var/lib/postgres/data/postgresql.conf 👑📝
# 🌐 wiki: https://wiki.archlinux.org/title/PostgreSQL#Configure_PostgreSQL_to_be_accessible_exclusively_through_UNIX_Sockets
listen_addresses = 'localhost'
...
```

> **Tip**: Si desea que sea accesible desde hosts remotos [Configure PostgreSQL para que sea accesible desde hosts remotos](https://wiki.archlinux.org/title/PostgreSQL#Configure_PostgreSQL_to_be_accessible_from_remote_hosts).



### Iniciar servicio de `postgres`

```sh
sudo systemctl start postgresql.service
```



### Superusuario del sistema también en la base de datos

Si crea un rol/usuario de PostgreSQL con el mismo nombre que su usuario de Linux, le permite acceder al *shell de la base de datos de PostgreSQL* sin tener que especificar un usuario "`-U postgres`" para iniciar sesión (lo que lo hace muy conveniente).

Entrar como usuario `postgres`:

```sh
$ sudo -iu postgres
```

[Crear usuario de *Linux* `dakataca` como rol/usuario de servidor de base datos](https://www.postgresql.org/docs/current/app-createuser.html)  con permisos administrativos, esto es conveniente porque permite acceder al shell de la base de datos de PostgreSQL desde el usuario Linux.

```sh
[postgres]$ createuser --password --interactive --createrole --superuser --createdb
Ingrese el nombre del rol a agregar: dakataca
Contraseña:
```

Acceder a la *shell de la base de datos* `psql`:

```sh
[postgres]psql
psql (15.3)
Digite «help» para obtener ayuda.

postgres=#
```

Asignar contraseña de manera interactiva al usuario `postgres` a nivel del servidor de base de datos `\password`:

```sh
postgres=# \password
Ingrese nueva contraseña para usuario «postgres»:
Ingrésela nuevamente:
```

Salimos de la la *shell de la base de datos* y del usuario `postgres`

```sh
postgres=# exit
no se pudo guardar historial a archivo «/var/lib/postgres/.psql_history»: No existe el fichero o el directorio
[postgres]exit
```

> **Nota**: Podemos ignorar el error anterior sin problema.

Asignar contraseña del usuario `postgres` a nivel de sistema operativo Linux :

```sh
sudo passwd postgres
```



# Pgadmin4



## Limpiar instalación previa

Si hizo una instalación fallida previa o simplemente desea asegurarse de hacer una instalación limpia ejecute:

```sh
sudo rm -rf ~/envs/pgadmin4/ /var/{lib,log}/pgadmin
```



## Instalar pgadmin4

Usaremos un [entorno virtual Python](https://wiki.archlinux.org/title/Python/Virtual_environment), ya que al momento de realizar la guía, el paquete de los repositorios oficiales no anda bien.

Crear directorios:

```sh
sudo mkdir -p /var/{lib,log}/pgadmin
```

Cambiar propietario de directorios:

```sh
sudo chown $USER /var/{lib,log}/pgadmin
```

Crear directorio a nivel de usuario para entornos virtuales de *Python*:

```sh
mkdir -p ~/venvs/ ; cd ~/venvs
```

Crear entorno virtual:

```sh
python3 -m venv pgadmin4
```

> Si desea limpiar la caché de pip use: `pip cache purge`.

Activar entorno virtual:

```sh
source pgadmin4/bin/activate
```

> **Tip**: Si desea salir y desactivar el entorno virtual use `deactivate`.

Instalar `pgadmin4` en el entorno virtual:

```sh
(pgadmin4)$ pip install pgadmin4
...
```

Iniciar `pgadmin4`(Debe haber previamente inicializado el servicio de `postgresql.service`):

```sh
(pgadmin4)$ pgadmin4
NOTE: Configuring authentication for SERVER mode.

Enter the email address and password to use for the initial pgAdmin user account:

Email address: danieldakataca@gmail.com
Password: 
Retype password:
```

> **Nota**: Solicitará un correo y contraseña de 6 o más caracteres como credenciales:

```sh
Starting pgAdmin 4. Please navigate to http://127.0.0.1:5050 in your browser.
 * Serving Flask app "pgadmin" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
```

En su navegador ingrese a `http://127.0.0.1:5050` usando las credenciales establecidas como muestran la siguiente imagen.

![](imgs/postgresql/loginpgadmin.png)



## Registrar nuevo servidor

![](imgs/postgresql/newserver.png)

![](imgs/postgresql//registerServer.png)

![](imgs/postgresql//registerserverconexion.png)

Salvamos la información.

Deberíamos obtener algo como ésto si desplegamos el Tablero.

![](imgs/postgresql//tableroPgadmin.png)



# Próximos inicios

## Manual

La próxima ves que desee iniciar `pgadmin4`debe:

1. Iniciar el servicio `PostgreSQL`:

   ```sh
   sudo systemctl start postgresql.service
   ```

2. Acceder al directorio de entornos virtuales de `Python`:

   ```
   cd ~/envs/
   ```

3. Activar el entorno virtual `pgadmin4`:

   ```sh
   source pgadmin4/bin/activate
   ```

4. Ejecutar la aplicación:

   ```sh
   (pgadmin4)$ pgadmin4
   ```

5. En su navegador favorito acceder a http://127.0.0.1:5050 e ingresar sus credenciales de usuario administrador de la base de datos `postgres`, es decir, correo y contraseña.

> **Nota**: Recuerda que un entorno virtual es como un espacio de trabajo aislado y dedicado para una aplicación específica.  Al crear un entorno virtual para cada aplicación, aseguramos que las versiones de los paquetes utilizados sean específicas para esa aplicación en particular  sin afectar a otras aplicaciones.

6. Si desea salir del entorno virtual use:

   ```sh
   (pgadmin4)$ deactivate
   ```

   

## Automático

Puede establecer un alias en el espacio de usuario para automatizar el inicio del servicio de `postgresql.service` y `pgadmin4`. Requiere de [`openbsd-netcat`](https://archlinux.org/packages/extra/x86_64/openbsd-netcat/):

```sh
nvim ~/.bashrc
-----------------------------------------------------------------------------
...
# 🌟🌟🌟 Editar la configuración del alias para ejecutar el servidor PostGreSQL+PgAdmin 🌟🌟🌟
# 📂 ~/.bashrc 👤📝
...
alias pgadmin='[[ `systemctl is-active --quiet postgresql.service` ]] || sudo systemctl start postgresql.service ; source ~/envs/pgadmin4/bin/activate ; pgadmin4 & \
while ! nc -z 127.0.0.1 5050; do sleep 1; done ; \
xdg-open http://127.0.0.1:5050 & \
deactivate'
...
```

Ahora para iniciar `pgadmin4` solo proporcione el comando `pgadmin`:

```sh
pgadmin
[sudo] contraseña para dakataca:
[1] 9692
Starting pgAdmin 4. Please navigate to http://127.0.0.1:5050 in your browser.
2023-08-05 11:30:59,867: WARNING        werkzeug:       WebSocket transport not available. Install simple-websocket for improved performance.
 * Serving Flask app 'pgadmin'
 * Debug mode: off
Connection to 127.0.0.1 5050 port [tcp/mmcc] succeeded!
[2] 9748
[dakataca@elitebook ~]$ Abriendo en una sesión existente del navegador
MESA-INTEL: warning: Haswell Vulkan support is incomplete
```

Ésto abrirá `pgadmin4` en su navegador predeterminado.
