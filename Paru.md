# Paru

## Configuración de rust/cargo

```sh
sudo pacman -S rustup --needed --noconfirm
```

```sh
mkdir -p ~/.cargo/
```

```sh
nvim ~/.cargo/config
--------------------------------------------------------------
## Fichero de configuración a nivel de usuario del compilador de rust cargo:
#--> ~/.cargo/config

## wiki: https://wiki.archlinux.org/title/Rust#Optimizing_for_native_CPU_platform
#Optimize for native CPU platform.
[target.x86_64-unknown-linux-gnu]
rustflags = ["-C", "target-cpu=native"]

[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"

#Replace it with your preferred mirror source.
replace-with = 'ustc'
#replace-with = 'tuna'
#replace-with = 'sjtu'
#replace-with = 'rustcc'

#University of science and technology of China.
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"

#Tsinghua University.
#[source.tuna]
#registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"

#Shanghai Jiaotong University.
#[source.sjtu]
#registry = "https://mirrors.sjtug.sjtu.edu.cn/git/crates.io-index"

#Rustcc community.
#[source.rustcc]
#registry = "git://crates.rustcc.cn/crates.io-index"

### Autor: https://t.me/dakataca ###
```

```sh
rustup default stable
```

#### Paru(AUR helper)

```sh
cd /tmp/
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd
```

