

[toc]



# Introducción

Ésta guía está enfocada en la configuración con una sola tarjeta gráfica *Intel*, es decir, que si posee más de una tarjeta gráfica debe configurar su tarjeta gráfica *Intel* usando la siguiente guía y a demás, implementar [gráficos híbridos](https://wiki.archlinux.org/title/Hybrid_graphics) según corresponda:

**AMD e Intel**
https://wiki.archlinux.org/title/Supergfxctl

**Nvidia y AMD/Intel**
https://wiki.archlinux.org/title/PRIME
https://wiki.archlinux.org/title/NVIDIA_Optimus


# [Intel Quick Sync Video](https://trac.ffmpeg.org/wiki/Hardware/QuickSync)

[Intel Quick Sync Video ](https://trac.ffmpeg.org/wiki/Hardware/QuickSync) es el nombre comercial de un conjunto de características de hardware disponibles dentro de muchas GPU Intel.



# Conociendo nuestro hardware Intel

Inicialmente debemos conocer nuestro hardware gráfico.



## Gráfica y módulo del kernel usado

```sh
lspci -vnn | grep -E 'VGA|3D|Display' -A 9
.....................................................
00:02.0 VGA compatible controller [0300]: Intel Corporation Atom Processor Z36xxx/Z37xxx Series Graphics & Display [8086:0f31] (rev 0e) (prog-if 00 [VGA controller])
        Subsystem: Acer Incorporated [ALI] Device [1025:0934]
        Flags: bus master, fast devsel, latency 0, IRQ 91
        Memory at 90000000 (32-bit, non-prefetchable) [size=4M]
        Memory at 80000000 (32-bit, prefetchable) [size=256M]
        I/O ports at 2050 [size=8]
        Expansion ROM at 000c0000 [virtual] [disabled] [size=128K]
        Capabilities: <access denied>
        Kernel driver in use: i915
        Kernel modules: i915
```

> **Nombre**: Procesador [`Intel® Celeron® Processor N2840`](https://ark.intel.com/content/www/us/en/ark/products/82103/intel-celeron-processor-n2840-1m-cache-up-to-2-58-ghz.html)  con gráfica integrada [`Intel® HD Graphics for Intel Atom® Processor Z3700 Series`](https://ark.intel.com/content/www/us/en/ark/products/82103/intel-celeron-processor-n2840-1m-cache-up-to-2-58-ghz.html#tab-blade-1-0-4).
> **Módulo del kernel disponible y en uso**: `i915`.



## Arquitectura, generación y codecs soportados

Identifique la generación de su gráfica fácilmente de acuerdo al nombre de la plataforma o tecnología de fabricación, en la [wiki de ffmpeg sobre Intel Quick Sync](https://trac.ffmpeg.org/wiki/Hardware/QuickSync).

<table>
  <thead>
    <tr>
      <th>Nombre de la plataforma</th>
      <th>Gráficos</th>
      <th>Agrega soporte para...</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Ironlake</td>
      <td>gen5</td>
      <td>MPEG-2, H.264 decode.</td>
    </tr>
    <tr>
      <td>Sandy Bridge</td>
      <td>gen6</td>
      <td>VC-1 decode; H.264 encode.</td>
    </tr>
    <tr>
      <td>Ivy Bridge</td>
      <td>gen7</td>
      <td>JPEG decode; MPEG-2 encode.</td>
    </tr>
    <tr>
      <td>Bay Trail</td>
      <td>gen7</td>
      <td>-</td>
    </tr>
    <tr>
      <td>Haswell</td>
      <td>gen7.5</td>
      <td>-</td>
    </tr>
    <tr>
      <td>Broadwell</td>
      <td>gen8</td>
      <td>VP8 decode.</td>
    </tr>
    <tr>
      <td>Braswell</td>
      <td>gen8</td>
      <td>H.265 decode; JPEG, VP8 encode.</td>
    </tr>
    <tr>
      <td>Skylake</td>
      <td>gen9</td>
      <td>H.265 encode.</td>
    </tr>
    <tr>
      <td>Apollo Lake</td>
      <td>gen9</td>
      <td>VP9, H.265 Main10 decode.</td>
    </tr>
    <tr>
      <td>Kaby Lake</td>
      <td>gen9.5</td>
      <td>VP9 profile 2 decode; VP9, H.265 Main10 encode.</td>
    </tr>
    <tr>
      <td>Coffee Lake</td>
      <td>gen9.5</td>
      <td>-</td>
    </tr>
    <tr>
      <td>Gemini Lake</td>
      <td>gen9.5</td>
      <td>-</td>
    </tr>
    <tr>
      <td>Cannon Lake</td>
      <td>gen10</td>
      <td>-</td>
    </tr>
    <tr>
      <td>Ice Lake</td>
      <td>gen11</td>
      <td>H.265 8 bits 422/444 decode,encode; vp9 8/10 bits 444 decode,encode</td>
    </tr>
    <tr>
      <td>Tiger Lake</td>
      <td>gen12</td>
      <td>av1 8,10 bits; H.265 12 bits decode;vp9 12 bits decode</td>
    </tr>
    <tr>
      <td>Rocket Lake</td>
      <td>gen12</td>
      <td>-</td>
    </tr>
    <tr>
      <td>DG1</td>
      <td>gen12</td>
      <td>vp8 encode removed</td>
    </tr>
    <tr>
      <td>SG1</td>
      <td>gen12</td>
      <td>vp8 encode removed</td>
    </tr>
    <tr>
      <td>Alder Lake</td>
      <td>gen12</td>
      <td>-</td>
    </tr>
    <tr>
      <td>DG12</td>
      <td>gen12</td>
      <td>AV1 encode</td>
    </tr>
    <tr>
      <td>Raptor Lake</td>
      <td>gen12</td>
      <td>-</td>
    </tr>
  </tbody>
</table>





## Características y soporte de gráficas Intel

Ubique su gráfica en la siguiente [lista de unidades de procesamiento de gráficos Intel](https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units), para conocer sus características y soporte. También puede consultar su gráfica *Intel* en la [página oficial Intel](https://www.intel.com/content/www/us/en/products/details/processors.html).



## Gráfica integrada al procesador?

Si nuestra gráfica está integrada al procesador, entonces es prudente conocer la información de dicho procesador.

### Fabricante y modelo del procesador

```sh
lscpu | grep 'ID\|model'
..................................................
ID de fabricante:                    GenuineIntel
Nombre del modelo:                   Intel(R) Celeron(R) CPU  N2840  @ 2.16GHz
```

```sh
cat /proc/cpuinfo | grep "model name" | uniq
.....................................................
model name      : Intel(R) Celeron(R) CPU  N2840  @ 2.16GHz
```

### Arquitectura del procesador

```sh
cat /sys/devices/cpu/caps/pmu_name
....................................................
silvermont
```



Las siguientes capturas sirven de guía para localizar su gráfica:

https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units

![](imgs/z37.png)


![](imgs/baytrailm.png)

> Las características de la gráfica del ejemplo son:
>
> - Gráfica integrada [Intel® HD Graphics for Intel Atom® Processor Z3700 Series](https://ark.intel.com/content/www/us/en/ark/products/82103/intel-celeron-processor-n2840-1m-cache-up-to-2-58-ghz.html#tab-blade-1-0-4).
> - Procesador [Intel® Celeron® Processor N2840](https://ark.intel.com/content/www/us/en/ark/products/82103/intel-celeron-processor-n2840-1m-cache-up-to-2-58-ghz.html) de la [serie  Intel® Celeron® Processors N series](https://www.intel.com/content/www/us/en/products/details/processors/celeron/products.html).
> - Generación 7.
> - Familia de procesador [Bay Trail M](https://en.wikipedia.org/wiki/Silvermont#Mobile_processors_(Bay_Trail-M)).
> - Utiliza la tecnología de [séptima generación](https://en.wikipedia.org/wiki/Intel_Graphics_Technology#Gen7_architecture) [Ivy Bridge](https://en.wikipedia.org/wiki/Intel_Graphics_Technology#Ivy_Bridge).
> - Microarquitectura de procesador [Valleyview o silvermont](https://en.wikipedia.org/wiki/Silvermont_(microarchitecture)).
> - Familia de gráficas [Intel HD graphics](https://es.wikipedia.org/wiki/Intel_HD_Graphics), éste es valor que usaremos para identificar nuestra gráfica en la opción `Identifier "Intel HD Graphics"`.
> - Lanzado en 2013.
> - Soporta [OpenGL](https://wiki.archlinux.org/title/OpenGL), [OpenCL](https://wiki.archlinux.org/title/GPGPU#OpenCL) y [Vulkan](https://wiki.archlinux.org/title/Vulkan).



# Tecnologías para GPU's Intel

<table>
<thead>
    <tr>
    	<th align="center" style="vertical-align: middle;" rowspan=2>Controlador (Open Source)</th>
        <!--<th align="center" style="vertical-align: middle;" rowspan=2>Tipo</th>-->
        <th align="center" style="vertical-align: middle;" rowspan=2>Paquete</th>
        <th align="center" style="vertical-align: middle;"rowspan=2>Manual Pages</th>
        <th align="center" style="vertical-align: middle;" colspan=2>OpenGL</th>
        <th align="center" style="vertical-align: middle;" rowspan=2>OpenCL</th>
        <th align="center" style="vertical-align: middle;" colspan=2>Vulkan</th>
        <th align="center" style="vertical-align: middle;"rowspan=2>Docs</th>
    </tr>
    <tr>
        <th align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=mesa" target="_blank">x86_64</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://wiki.archlinux.org/title/Multilib" target="_blank">multilib</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=mesa" target="_blank">x86_64</a></th>
        <th align="center" style="vertical-align: middle;"><a href="https://wiki.archlinux.org/title/Multilib" target="_blank">multilib</a></th>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>intel</code></td>
        <!--<td align="center" style="vertical-align: middle;" rowspan=2>Open Source</td>-->
        <td align="center"><a href="https://archlinux.org/packages/extra/x86_64/xf86-video-intel/" target="_blank">xf86-video-intel</a></td>
        <td align="center" style="vertical-align: middle;"><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel.4</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://archlinux.org/packages/extra/x86_64/mesa/" target="_blank">mesa</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://archlinux.org/packages/multilib/x86_64/lib32-mesa/" target="_blank">lib32-mesa</a></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://aur.archlinux.org/packages/intel-opencl/" target="_blank">intel-opencl</a> ó <a href="https://archlinux.org/packages/?name=intel-compute-runtime" target="_blank">intel-compute-runtime</a></td>
        <td alignt="center" style="vertical-align: middle;" rowspan=2><a href="https://archlinux.org/packages/extra/x86_64/vulkan-icd-loader/" target="_blank">vulkan-icd-loader</a> y <a href="https://archlinux.org/packages/extra/x86_64/vulkan-intel/" target="_blank">vulkan-intel</a></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://archlinux.org/packages/multilib/x86_64/lib32-vulkan-icd-loader/" target="_blank">lib32-vulkan-icd-loader</a> y <a href="https://archlinux.org/packages/multilib/x86_64/lib32-vulkan-intel/" target="_blank">lib32-vulkan-intel</a></td>
        <td align="center" style="vertical-align: middle;" rowspan="2"><a href="https://wiki.archlinux.org/title/Intel_graphics" target="_blank">Intel graphics</a></td>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>modesetting</code></td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/extra/x86_64/xorg-server/" target="_blank">xorg-server</a></td>
        <td align="center" style="vertical-align: middle;"><a href="https://man.archlinux.org/man/modesetting.4" target="_blank">modesetting.4</a></td>
    </tr>
</thead>
<tbody>
</tbody>        
</table>


# Módulo del kernel `i915`

Algunas configuraciones aplicables al módulo del kernel encargado de las GPU's *Intel* son:

## Framebuffer compression

<table>
  <thead>
    <tr>
      <th align="center">Opción</th>
      <th align="center">Generación</th>
      <th align="center">Micro-arquitectura</th>
      <th align="center">Descripción</th>
      <th align="center">Wiki</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="center"  style="vertical-align: middle;"><code>enable_fbc=1</code></td>
        <td align="center"  style="vertical-align: middle;"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen7" target="_blank">Gen >= 7</a></code></td>
        <td align="center"  style="vertical-align: middle;"><a href="https://en.wikipedia.org/wiki/Ivy_Bridge_(microarchitecture)" target="_blank">Ivy Bridge</a> en adelante</td>
        <td align="center"  style="vertical-align: middle;">Reducir el consumo de energía y el ancho de banda de memoria necesario para las actualizaciones de pantalla</td>
        <td align="center"  style="vertical-align: middle;"><a href="https://wiki.archlinux.org/title/intel_graphics#Framebuffer_compression_(enable_fbc)" target="_blank">Compresión del buffer de fotogramas(enable_fbc)</a></td>
    </tr>
  </tbody>
</table>

Si su gráfica es de la generación 7 use:

```sh
bash -c 'cat > /etc/modprobe.d/i915.conf' << EOF
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Framebuffer_compression_(enable_fbc)
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Enable_GuC_/_HuC_firmware_loading

options i915 enable_fbc=1                   # Framebuffer compression.
EOF
```





## GuC/HuC

GuC (Graphics microcode) es un microcódigo propietario utilizado por *Intel* para mejorar el rendimiento y la eficiencia energética de sus controladores gráficos integrados. HuC (H.264/AVC undecoded bitstream) es un firmware utilizado para acelerar la decodificación de video H.264/AVC. Si su gráfica es compatible use el valor correspondiente a la arquitectura de su gráfica:

<table>
<thead>
    <tr>
        <th style="text-align:center">Valor de <code>i915.enable_guc</code></th>
        <th style="text-align:center">Envío GuC</th>
        <th style="text-align:center">Carga de firmware HuC</th>
        <th style="text-align:center">Plataformas predeterminadas</th>
        <th style="text-align:center">Plataformas compatibles</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td style="text-align:center">0</td>
        <td style="text-align:center">No</td>
        <td style="text-align:center">No</td>
        <td style="text-align:center"><a href="https://en.wikipedia.org/wiki/Tiger_Lake" target="_blank">Tiger Lake</a>, <a href="https://es.wikipedia.org/wiki/Rocket_Lake" target="_blank">Rocket Lake</a>, y Pre-Gen12</td>
        <td style="text-align:center">Todas las plataformas</td>
    </tr>
    <tr>
        <td style="text-align:center">1</td>
        <td style="text-align:center">Sí</td>
        <td style="text-align:center">No</td>
        <td style="text-align:center">-</td>
        <td style="text-align:center"><a href="https://en.wikipedia.org/wiki/Alder_Lake#Alder_Lake-P" target="_blank">Alder Lake-P</a> (móvil) y posteriores</td>
    </tr>
    <tr>
        <td style="text-align:center">2</td>
        <td style="text-align:center">No</td>
        <td style="text-align:center">Sí</td>
        <td style="text-align:center"><a href="https://en.wikipedia.org/wiki/Alder_Lake#Processors_for_Internet_of_Things_(IoT)_devices_and_embedded_systems_(Alder_Lake-PS)" target="_blank">Alder Lake-S</a> (escritorio) [6] [7]</td>
    	<td style="text-align:center">Gen9 y posteriores</td>
    </tr>
    <tr>
        <td style="text-align:center">3</td>
        <td style="text-align:center">Sí</td>
        <td style="text-align:center">Sí</td>
        <td style="text-align:center" colspan=2><a href="https://en.wikipedia.org/wiki/Alder_Lake#Alder_Lake-P" target="_blank">Alder Lake-P</a> (móvil) y posteriores</td>
    </tr>
</tbody>
</table>

> **Nota**: Como la gráfica del ejemplo es de generación 7 ésta configuración no se debe aplicar.

Si su gráfica es de la arquitectura [Alder Lake-S](https://en.wikipedia.org/wiki/Alder_Lake#Processors_for_Internet_of_Things_(IoT)_devices_and_embedded_systems_(Alder_Lake-PS)) de 2021, aplique ambas configuraciones, `enable_fbc=1` y `enable_guc=2` :

```sh
nvim /etc/modprobe.d/i915.conf
----------------------------------------------------------------
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Framebuffer_compression_(enable_fbc)
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Enable_GuC_/_HuC_firmware_loading

options i915 enable_fbc=1 enable_guc=2     # Requiere linux-firmware e intel-media-driver.
```



## Aplicar configuración del módulo del kernel `i915`

Vuelva a generar [initramfs](https://wiki.archlinux.org/title/Mkinitcpio#Manual_generation):

```sh
mkinitcpio -P
```



## Verificar configuración

En el próximo arranque verifique si se activó correctamente la configuración del módulo:

```sh
dmesg | grep "[GH]uC firmware"
.......................................................
[30130.586970] i915 0000:00:02.0: [drm] GuC firmware i915/icl_guc_33.0.0.bin version 33.0 submission:disabled
[30130.586973] i915 0000:00:02.0: [drm] HuC firmware i915/icl_huc_9.0.0.bin version 9.0 authenticated:yes
```

```sh
modprobe -c | grep "i915 enable"
```



# OpenGL

Es la API para aceleración 2D y 3D. Existen dos implementaciones de [OpenGL](https://wiki.archlinux.org/title/OpenGL):

- [**mesa**](https://archlinux.org/packages/?name=mesa): Implementación **recomendada** para GPU's *Intel*.

- [**mesa-amber**](https://archlinux.org/packages/?name=mesa-amber): **No recomendada** para GPU's *Intel*, ya que se enfoca en proporcionar soporte para hardware antiguo de AMD y no tiene las actualizaciones y mejoras más recientes que [Mesa](https://wiki.archlinux.org/title/Mesa) tiene para ofrecer en términos de soporte para [OpenGL](https://wiki.archlinux.org/title/OpenGL) y [Vulkan](https://wiki.archlinux.org/title/Vulkan).

  > Si quiere hacer caso omiso a la recomendación e instalar [Mesa-amber](https://archlinux.org/packages/?name=mesa-amber) en ves de [Mesa](https://archlinux.org/packages/?name=mesa), entonces también debe asignar el controlador a la variable de entorno `MESA_LOADER_DRIVER_OVERRIDE`. Por ejemplo si implementará el controlador `i965`, entonces quedaría así:
  >
  > ```sh
  > nvim /etc/environment
  > --------------------------------------------------
  > ...
  > MESA_LOADER_DRIVER_OVERRIDE=i965
  > ```
  >
  > Con ésto anulamos la detección automática del controlador de gráficos y seleccionamos un controlador de gráficos específico para todo el sistema. **Tenga cuidado**, porque si implementa un controlador inadecuado puede tener efectos secundarios no deseados en el sistema.



## Instalación

Siguiendo las recomendaciones anteriores instalamos la implementación de [OpenGL](https://wiki.archlinux.org/title/OpenGL) [Mesa](https://wiki.archlinux.org/title/Mesa) para gráficas *Intel* y las utilidades de [Mesa](https://wiki.archlinux.org/title/Mesa) para su verificación [`mesa-utils`](https://archlinux.org/packages/?name=mesa-utils).

```sh
pacman -S mesa{,-utils} --needed --noconfirm
```



## Verificación

Si implementó correctamente [OpenGL](https://wiki.archlinux.org/title/OpenGL) debería obtener una salida parecida a la siguiente:

![](imgs/glxinfo.png)



# Controladores genéricos

Existen dos controladores genéricos que pueden haberse instalado automáticamente y dar imagen, pero no van a sacar el mayor provecho a nuestra gráfica:

1. [xf86-video-fbdev](https://archlinux.org/packages/?name=xf86-video-fbdev)
2. [xf86-video-vesa](https://archlinux.org/packages/?name=xf86-video-vesa)

Lo ideal es no tenerlos instalados y en dado caso que ya los tenga, desinstálelos:

```sh
pacman -R xf86-video-{fbdev,vesa}
```

> **Nota**: Alguno de éstos controladores solo sería necesarios si se han descartado los controladores descritos a continuación.

# Controlador gráfico apropiado

Existen solo dos drivers apropiados para GPU's *Intel*, [`intel`](https://man.archlinux.org/man/intel.4) y [`modesetting`](https://man.archlinux.org/man/modesetting.4). El controlador adecuado depende principalmente de dos factores, la generación de la gráfica y el servidor gráfico que va a usar ([Xorg](https://wiki.archlinux.org/title/Xorg) o [Wayland](https://wiki.archlinux.org/title/Wayland)).

<table>
<thead>
    <tr>
        <th align="center">Controlador</th>
        <th align="center">Servidor gráfico</th>
        <th align="center">Generación</th>
        <th align="center">Arquitectura</th>
        <th align="center">Paquete</th>
        <th align="center">File config</th>
        <th align="center">Tearing</th>
    </tr>
<thead>
<tbody>
    <tr>
		<td rowspan=2 align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/modesetting.4" target="_blank">modesetting</a></code></td>
        <td align="center" style="vertical-align: middle;">Wayland</td>
        <td align="center" style="vertical-align: middle;"><code>Gen >=4</code></td>
        <td align="center" style="vertical-align: middle;">Lakeport en adelante</td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/testing/x86_64/linux/" target="_blank">linux</a></td>
        <td align="center" style="vertical-align: middle;">Wayland se configura a través de variables de entorno.</td>
        <td align="center" style="vertical-align: middle;">No</td>
    </tr>
    <tr>
        <td rowspan=2 align="center" style="vertical-align: middle;">Xorg</td>
        <td align="center" style="vertical-align: middle;"><code>Desde Gen 1</code></td>
        <td align="center" style="vertical-align: middle;">A partir de Auburn</td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/extra/x86_64/xorg-server/" target="_blank">xorg-server</a></td>
        <td rowspan=2 align="center" style="vertical-align: middle;"><code>/etc/X11/xorg.conf.d/20-gpudriver.conf</code></td>
        <td  rowspan=2  align="center" style="vertical-align: middle;">Probable</td>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td>
		<td align="center" style="vertical-align: middle;"><code>Hasta Gen 7</code></td>
        <td align="center" style="vertical-align: middle;">Hasta Ivy Bridge o Bay Trail</td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://www.archlinux.org/packages/extra/x86_64/xf86-video-intel/" target="_blank">xf86-video-intel</a></td>
    </tr>
</tbody>
</table>


> **Recomendación personal**: Usar en lo posible *modesetting*, a menos, que posea una gráfica de generación menor o igual a la séptima, presenta [tearing en Xorg](https://es.wikipedia.org/wiki/Tearing), o ha comprobado que su gráfica saca el máximo rendimiento solo cuando usa el controlador *Intel* ([xf86-video-intel](https://man.archlinux.org/man/intel.4)) con un benchmark OpenGL/Vulkan.



## modesetting

El controlador de modos es un controlador para dispositivos KMS (Kernel Mode Setting).

### Características

- En *Wayland* no requiere ningún paquete extra, debido a que utiliza la funcionalidad KMS (Kernel Mode Setting) proporcionada por el kernel de Linux.
- En *Wayland* no requiere fichero de configuración.
- Recomendado para gráficas *Intel* más recientes y a partir de la cuarta generación.
- No recomendado en *Xorg* si presenta [tearing](https://es.wikipedia.org/wiki/Tearing) en la pantalla.
- En *Xorg* requiere el paquete [xorg-server](https://archlinux.org/packages/extra/x86_64/xorg-server/).
- A pesar de proporcionar gráficos genéricos, se recomienda antes que el controlador *intel*, a menos que presente problemas de rendimiento o [tearing](https://es.wikipedia.org/wiki/Tearing).
- No requiere configuración en la mayoría de los casos.



### Configuración

Si usa *Xorg* en la [página del manual de Archlinux](https://man.archlinux.org/man/modesetting.4) hay varias configuraciones que podría experimentar. Un [ejemplo básico de configuración](https://wiki.archlinux.org/title/Intel_GMA_3600#Xorg_driver) comentando algunas opciones que podría experimentar sería:

```sh
nvim /etc/X11/xorg.conf.d/20-gpudriver.conf
--------------------------------------------------------
Section "Device"
    Identifier "Intel HD Graphics"
    #Identifier "Intel Graphics"      # Identificador genérico para gráficas Intel.
    Driver     "modesetting"
#     Option     "AccelMethod"   "none"        # Desactivar la aceleración.
#     Option     "PageFlip"      "false"       # Desactivar la aceleración 3D.
EndSection
```

> **Nota**: En [wayland](https://wiki.archlinux.org/title/Wayland) el controlador *modesetting* no requiere fichero de configuración, pero podría implementar variables de entorno para configurar su servidor dependiendo de su entorno visual, por ejemplo en [Sway](https://wiki.archlinux.org/title/Sway) podría implementar las recomendaciones de la [wiki](https://wiki.archlinux.org/title/Sway).



#### Deshabilitar DRI3 (no recomendado)

Con el controlador `modesetting` usar la opción `Option "DRI" "2"` no funciona, en su lugar utilice la variable de entorno `LIBGL_DRI3_DISABLE=1`.

```sh
nvim /etc/environment
-------------------------------------------------
...
LIBGL_DRI3_DISABLE=1
```



## intel

Es un controlador *Xorg* para conjuntos de chips de gráficos integrados de Intel.

### Características

- Compatible únicamente con *Xorg*.
- Requiere el paquete [xf86-video-intel](https://man.archlinux.org/man/intel.4).
- Recomendado para gráficas *Intel* de cuarta o menor generación.
- Corrige el [tearing](https://es.wikipedia.org/wiki/Tearing) en la pantalla.
- Es recomendable establecer una configuración así sea muy básica, sobre todo si presenta [tearing](https://es.wikipedia.org/wiki/Tearing).
- Aprovechar las características específicas de *Intel*.

En [*Xorg*](https://wiki.archlinux.org/title/Xorg#Driver_installation) requiere la instalación del siguiente paquete:

```sh
pacman -S xf86-video-intel --needed --noconfirm
```

> **Nota**: Recuerde que el controlador `intel` no es posible implementarlo en *Wayland*, solo `modesetting` es posible implementarlo tanto en *Wayland* como en *Xorg*.



### Configuración

A diferencia de `modesetting`, el controlador `intel` requiere configuración manual si ha experimentado [tearing](https://es.wikipedia.org/wiki/Tearing), desea sacar el mejor provecho a su GPU o las configuraciones por defecto superan las capacidades [OpenGL](https://wiki.archlinux.org/title/OpenGL), [Vulkan](https://wiki.archlinux.org/title/Vulkan) y [OpenCL](https://wiki.archlinux.org/title/GPGPU#OpenCL). En la [wiki de Archlinux](https://wiki.archlinux.org/title/intel_graphics#Xorg_configuration) se establecen varias opciones de configuración de acuerdo a las características de nuestra gráfica.



#### Controlador [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure)

La biblioteca libGL carga el controlador de gráficos [Direct Rendering Infrastructure (DRI)](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure)  desde la ruta "`/usr/lib/dri/`" para proporcionar aceleración de hardware a la implementación [Mesa](https://wiki.archlinux.org/title/Mesa).

Por defecto *Archlinux* usa [DRI3](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure#DRI3) , pero usted puede configurarlo manualmente de acuerdo a su hardware *Intel*:



##### Deshabilitar [DRI3](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure#DRI3) (no recomendado)

Permite deshabilitar [DRI3](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure#DRI3) y en su lugar usar [DRI2](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure#DRI2).

```sh
Option "DRI" "2"
```



##### Deshabilitar aceleración (no recomendado)

Permite deshabilitar por completo la aceleración de video.

```sh
  Option "NoAccel" "True"
```

Alternativamente use:

```sh
 Option "DRI" "False"
```

> **Nota**: Usar solo cuando presente congelamientos o bloqueos con el controlador [intel](https://man.archlinux.org/man/intel.4).

 

##### Configurar el controlador [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) apropiado (**recomendado**)

Si usará el controlador gráfico [intel](https://man.archlinux.org/man/intel.4) en [Xorg](https://wiki.archlinux.org/title/Xorg), dispone de varios controladores [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) para hardware gráfico específico definidos en la siguiente tabla.

<table>
<thead>
	<tr>
    	<th rowspan=2 align="center" style="vertical-align: middle;">Controlador <a href="https://wiki.archlinux.org/title/Mesa" target="_blank">mesa</a></th>
        <th rowspan=2 align="center" style="vertical-align: middle;">Generación de hardware</th>
        <th colspan=2 align="center" style="vertical-align: middle;">Arquitectura de hardware</th>
        <th rowspan=2 align="center" style="vertical-align: middle;">Controlador gráfico</th>
    </tr>
    <tr>
    	<th align="center" style="vertical-align: middle;">Desde</th>
        <th align="center" style="vertical-align: middle;">Hasta</th>
    </tr>
</thead>
<tbody>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>i915</code></td>
        <td align="center" style="vertical-align: middle;"><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Third_generation" target="_blank"><code>Gen 3</code></a></td>
        <td align="center" style="vertical-align: middle;"><code>Grantsdale</code></td>
        <td align="center" style="vertical-align: middle;"><code>Pineview</code></td>
        <td rowspan=4 align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td>
	</tr>
        <tr>
    	<td align="center" style="vertical-align: middle;"><code>crocus</code></td>
        <td align="center" style="vertical-align: middle;"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen4" terget="_blank">Gen 4</a> - <a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen7" target="_blank">Gen 7</a></code></td>
        <td align="center" style="vertical-align: middle;"><code>Lakeport</code></td>
        <td align="center" style="vertical-align: middle;"><code><a href="https://en.wikipedia.org/wiki/Haswell_(microarchitecture)" target="_blank">Haswell</a></code></td>
    <!--    <td align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td> -->
	</tr>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><code>iris</code></td>
        <td align="center" style="vertical-align: middle;"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8" terget="_blank">Gen >=8</a></td>
        <td align="center" style="vertical-align: middle;"><a href="https://en.wikipedia.org/wiki/Airmont_microarchitecture" target=""><code>Cherryview
Braswell (Gen8LP)(silvermont)</code></a></td>
        <td align="center" style="vertical-align: middle;"><code>∞</code></td>
      <!--  <td align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td> -->
	</tr>
<tr>
    	<td align="center" style="vertical-align: middle;"><code>i965</code></td>
    <td align="center" style="vertical-align: middle;"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen4" terget="_blank">Gen <=4 </a></code></td>
        <td align="center" style="vertical-align: middle;"><code>Lakeport y anteriores</code></td>
        <td align="center" style="vertical-align: middle;"><code>standalone</code></td>
      <!--  <td align="center" style="vertical-align: middle;"><code><a href="https://man.archlinux.org/man/intel.4" target="_blank">intel</a></code></td> -->
	</tr>
</tbody>
</table>

> **Nota**: Si dispone de una gráfica con tecnología [silvermont](https://en.wikipedia.org/wiki/Silvermont) o de generación igual o mayor a ocho, debería usar el controlador [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) `iris`, ya que ofrece un mejor rendimiento y compatibilidad con las últimas versiones de [OpenGL](https://wiki.archlinux.org/title/OpenGL) y [Vulkan](https://wiki.archlinux.org/title/Vulkan).



#### Establecer controlador gráfico [intel](https://man.archlinux.org/man/intel.4), controlador [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) y [Tearing](https://wiki.archlinux.org/title/intel_graphics#Tearing) para la gráfica del ejemplo

La gráfica *Intel* del ejemplo es de generación 7, por lo que en teoría debería implementar el controlador  [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) `crocus`, pero como posee a la arquitectura [silvermont](https://en.wikipedia.org/wiki/Silvermont_(microarchitecture)), también es posible implementar el controlador [DRI](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) `iris` para aprovechar las últimas tecnologías aplicadas al controlador recomendado por el mismo fabricante. También se aplican la opciones `TearFree` y `SwapbuffersWait` en `true`, para evitar el desgarro de pantalla  al llamar a [`glx`](https://es.wikipedia.org/wiki/GLX).

```sh
bash -c 'cat > /etc/X11/xorg.conf.d/20-intel.conf' << EOF
## Fichero de configuración del controlador intel del paquete "xf86-video-intel":
#--> /etc/X11/xorg.conf.d/20-intel.conf
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Xorg_configuration
## wiki: https://wiki.archlinux.org/title/Intel_graphics#Tearing
## wiki: https://wiki.archlinux.org/title/OpenGL
## wiki: https://wiki.archlinux.org/title/Vulkan
## man: https://man.archlinux.org/man/intel.4#CONFIGURATION_DETAILS
## man-page: https://man.archlinux.org/man/extra/xf86-video-intel/intel.4.en

Section "Device"
#    Identifier "Intel Graphics"  # Identificador genérico para gráficas Intel.
	Identifier "Intel HD Graphics"
    Driver "intel"
        ## Si usa compositor (xfc4, plasma, gnome o picom) deshabilite para mejorar rendimiento.
        Option "TearFree"        "false"  # No evitar desgarre de pantalla (tearing)
        Option "TripleBuffer"    "false"  # No usar un tercer buffer para cambiar de página.
        Option "SwapbuffersWait" "false"  # No evitar desgarro al llamar a glX.

#        Option "AccelMethod"     "uxa"   # Antiguo método de aceleración.
#        Option "DRI" "iris"    # Hardware Intel silvermont, Gen 8 y posteriores.
        Option "DRI" "crocus"  # Hardware Intel Gen 4 a Gen 7.
#        Option "DRI" "i915"    # Hardware Intel Gen 3.
#        Option "DRI" "zink"    # Controlador Gallium para ejecutar OpenGL sobre Vulkan.
#        Option "DRI"             "2"     # Deshabilitar DRI3.
#        Option "TearFree" "true"         # Evitar desgarro de pantalla.
#        Option "SwapbuffersWait" "true"  # Evitar desgarro al llamar a glX.
EndSection
EOF
```

> Otras [posibles configuraciones son](https://wiki.archlinux.org/title/intel_graphics#Troubleshooting):
>
> - [Antiguo método de aceleración `uxa`](https://wiki.archlinux.org/title/intel_graphics#AccelMethod).
> - [Deshabilitar sincronización vertical](https://wiki.archlinux.org/title/intel_graphics#Disable_Vertical_Synchronization_(VSYNC)).



# Tearing con ambos controladores

Si con la configuración de los anteriores controladores usted aun presenta *tearing*, entonces se recomienda [implementar el compositor](https://gitlab.com/dakataca/guides/-/blob/main/Qtile.md?ref_type=heads#picom) [picom](https://archlinux.org/packages/extra/x86_64/picom/).

# Aceleración de video por hardware



El soporte de *Intel* para *VA-API* está definido por los paquetes listados en la siguiente tabla:

<table>
<thead>
    <tr>
    	<th align="center">Arquitectura de gráficos intel</th>
        <th align="center">Generación</th>
        <th align="center">Soporte de aceleración de video</th>
        <th align="center">Herramientas de verificación</th>
    </tr>
</thead>
<tbody>
    <tr>
    	<td align="center"><a href="https://es.wikipedia.org/wiki/Broadwell_(microarquitectura)" target="_blank">Broadwell</a> y más recientes</td>
        <td align="center"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8" target="_blank">Gen >=8</a></code></td>
        <td align="center"><a href="https://archlinux.org/packages/?name=intel-media-driver" target="_blank">intel-media-driver</a></td>
        <td align="center" rowspan=4  style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=libva-utils" target="_blank">livba-utils</a> y <a href="https://archlinux.org/packages/?name=vdpauinfo" target="_blank">vdpauinfo</a></td>
    </tr>
    <tr>
    	<td align="center">Desde <a href="https://en.wikipedia.org/wiki/Intel_P45" target="_blank">Eaglelake</a> hasta <a href="https://en.wikipedia.org/wiki/Airmont_microarchitecture" target="_blank">Cherryview
Braswell (Gen8LP)(silvermont)</a></td>
        <td align="center">desde  <code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen4" target="_blank">Gen 4</a></code> hasta <code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8" target="_blank">Gen 8</a></code></td>
        <td align="center"><a href="https://archlinux.org/packages/extra/x86_64/libva-intel-driver/" target="_blank">libva-intel-driver</a></td>
    </tr>
    <tr>
    	<td align="center">Decodificación H.264 de <a href="https://en.wikipedia.org/wiki/Intel_P45" target="_blank">GMA 4500 </a></td>
        <td align="center"><code><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen4" target="_blank">Gen 4</a></code></td>
        <td align="center"><a href="https://aur.archlinux.org/packages/libva-intel-driver-g45-h264/" tergat="_blank">libva-intel-driver-g45-h264</a></td>
    </tr>
    <tr>
    	<td align="center"><a href="https://en.wikipedia.org/wiki/Haswell_(microarchitecture)#Haswell_Refresh" target="_blank">Haswell Refresh</a> y más nuevas (admite VP8/VP9)</td>
        <td align="center"><a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen7" target="_blank"><code>Gen >=7.5</code></a></td>
        <td align="center"><a href="https://aur.archlinux.org/packages/intel-hybrid-codec-driver/" target="_blank">intel-hybrid-codec-driver</a></td>
    </tr>
</tbody>
</table>




## Instalación

Para establecer la aceleración de video por hardware VA-API y VDPAU en el equipo de ejemplo, usamos las implementaciones compatibles con la arquitectura [silvermont](https://en.wikipedia.org/wiki/Silvermont) o anteriores, es decir, [libva-intel-driver](https://archlinux.org/packages/extra/x86_64/libva-intel-driver/) y [libvdpau-va-gl](https://archlinux.org/packages/extra/x86_64/libvdpau-va-gl/). También instalamos las utilidades [`intel-gpu-tools`](https://archlinux.org/packages/community/x86_64/intel-gpu-tools/), [`libva-utils`](https://archlinux.org/packages/?name=libva-utils) y [`vdpauinfo`](https://archlinux.org/packages/?name=vdpauinfo):

```sh
pacman -S libv{a-{intel-driver,utils},dpau-va-gl} vdpauinfo intel-gpu-tools --needed --noconfirm
```



## Verificación

### VA-API

```sh
vainfo
```

![](imgs/vainfo.png)

### VDPAU

```sh
vdpauinfo
```

![](imgs/vdpauinfo.png)

> **Nota**: En caso de no obtener salida proceda con la configuración manual de `VA-API` y `VDPAU`.



## Configuración manual de VA-API y VDPAU

La compatibilidad con la aceleración de video por hardware debería habilitarse automáticamente, pero en caso de haber obtenido errores en la verificación, es posible que sea necesario configurar `VA-API` y `VDPAU` manualmente.

Asigne a la variable de entorno el controlador correspondiente a la implementación instalada:

<table>
<thead>
    <tr>
        <th align="center">Implementación</th>
        <th align="center">Variable</th>
    	<th align="center">Controlador</th>
        <th align="center">Paquete</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td align="center" style="vertical-align: middle;" rowspan=2><b>VA-API</b></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><code>LIBVA_DRIVER_NAME</code></td>
        <td align="center" style="vertical-align: middle;"><code>iHD</code></td>
        <td align="center"><a href="https://archlinux.org/packages/?name=intel-media-driver" target="_blank"><code>intel-media-driver</code></a></td>
    </tr>
    <tr>
        <td align="center" style="vertical-align: middle;"><code>i965</code></td>
        <td align="center"><a href="https://archlinux.org/packages/extra/x86_64/libva-intel-driver/" target="_blank"><code>libva-intel-driver</code></a></td>
    </tr>
    <tr>
    	<td align="center" style="vertical-align: middle;"><b>VDPAU</b></td>
        <td align="center" style="vertical-align: middle;"><code>VDPAU_DRIVER</code></td>
        <td align="center" style="vertical-align: middle;"><code>va_gl</code></td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/extra/x86_64/libva-intel-driver/" target="_blank"><code>libva-intel-driver</code></a> ó <a href="https://archlinux.org/packages/?name=intel-media-driver" target="_blank"><code>intel-media-driver</code></a></td>
    </tr>
</tbody>
</table>



Establecemos las variables de entorno de `VA-API` y `VDPAU` respectivamente, en el fichero de configuración `/etc/environment` para el equipo del ejemplo:

```sh
nvim /etc/environment
-----------------------------------------------------
...
LIBVA_DRIVER_NAME=i965
VDPAU_DRIVER=va_gl
```

> **Nota**: Tenga en cuenta que el fichero de configuración `/etc/environment` solo acepta variables del tipo `clave=valor`, no permite comentarios ni campos adicionales delante de las variables de entorno.




# OpenCL

Es una especificación de programación paralela abierta y libre de regalías.



## Implementación OpenCL para gráficas Intel

Existen en [Archlinux](https://archlinux.org/) cuatro implementaciones [OpenCL](https://wiki.archlinux.org/title/GPGPU#OpenCL) disponibles, cada una orientada a cierto tipo de gráficas:

1. [intel-compute-runtime](https://archlinux.org/packages/?name=intel-compute-runtime)
2. [opencl-mesa](https://archlinux.org/packages/?name=opencl-mesa)
3. [beignet](https://aur.archlinux.org/packages/beignet/)
4. [intel-opencl](https://aur.archlinux.org/packages/intel-opencl/)
5. [intel-opencl-runtime](https://aur.archlinux.org/packages/intel-opencl-runtime/)

El fabricante *intel* recomienda elegir entre dos de ellas de acuerdo a las características de su gráfica, [intel-opencl](https://aur.archlinux.org/packages/intel-opencl/) o [intel-compute-runtime](https://archlinux.org/packages/?name=intel-compute-runtime). En la siguiente tabla se describe la implementación [OpenCL](https://wiki.archlinux.org/title/GPGPU#OpenCL) apropiada según las características de su gráfica.

<table>
<thead>
	<tr>
        <th align="center" style="vertical-align: middle;">Gráfica Intel</th>
        <th align="center" style="vertical-align: middle;">Fecha de lanzamiento</th>
		<th align="center" style="vertical-align: middle;">Arquitectura</th>
        <th align="center" style="vertical-align: middle;">Generación</th>
        <th align="center" style="vertical-align: middle;">Tecnología</th>
        <th align="center" style="vertical-align: middle;">Implementación OpenCL</th>
        <th align="center" style="vertical-align: middle;">Herramienta de verificación</th>
    </tr>
</thead>
<tbody>
	<tr>
        <td>Intel HD Graphics 2000/3000</td>
        <td align="center" style="vertical-align: middle;">2011</td>
        <td align="center" style="vertical-align: middle;">Sandy Bridge</td>
        <td align="center" style="vertical-align: middle;"><code>Gen 6</code></td>
        <td align="center" style="vertical-align: middle;"><code>32nm</code></td>
        <td>No se recomienda usar OpenCL en éstas gráficas ni en anteriores.</td>
        <td align="center" style="vertical-align: middle;" rowspan=4><a href="https://archlinux.org/packages/community/x86_64/clinfo/" target="_blank">clinfo</a></td>
    </tr>
    <tr>
    	<td>Intel HD Graphics 4000/2500</td>
        <td align="center" style="vertical-align: middle;">2012</td>
        <td align="center" style="vertical-align: middle;">Ivy Bridge y Bay Trail</td>
        <td align="center" style="vertical-align: middle;"><code>Gen 7</code></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><code>22nm</code></td>
        <td align="center" style="vertical-align: middle;" rowspan=2><a href="https://aur.archlinux.org/packages/intel-opencl/" target="_blank">intel-opencl</a></td>
    </tr>
    <tr>
    	<td>Intel HD Graphics 5000/5100/5200</td>
        <td align="center" style="vertical-align: middle;">2013</td>
        <td align="center" style="vertical-align: middle;">Haswell</td>
        <td align="center" style="vertical-align: middle;"><code>Gen 7.5</code></td>
    </tr>
    <tr>
    	<td>A partir de Intel HD Graphics 5300/5500/6000</td>
        <td align="center" style="vertical-align: middle;"> desde 2014</td>
        <td align="center" style="vertical-align: middle;">Broadwell en adelante</td>
        <td align="center" style="vertical-align: middle;"><code>Gen >=8</code></td>
        <td align="center" style="vertical-align: middle;"><code>22nm</code></td>
        <td align="center" style="vertical-align: middle;"><a href="https://archlinux.org/packages/?name=intel-compute-runtime" target="_blank">intel-compute-runtime</a></td>
</tbody>
</table>




## Instalación

Para la gráfica del ejemplo de generación 7 ([Bay Trail M](https://en.wikipedia.org/wiki/Silvermont#Mobile_processors_(Bay_Trail-M))) debemos instalar [`intel-opencl`](https://aur.archlinux.org/packages/intel-opencl). También instalamos la utilidad [clinfo](https://archlinux.org/packages/community/x86_64/clinfo/) para enumerar todas las propiedades de dispositivos y plataformas disponibles.

```sh
paru -S intel-opencl clinfo
```



## Verificación

Si su gráfica es compatible con `OpenCL` debería obtener un resultado parecido al siguiente:

```sh
clinfo
```

![](imgs/clinfo.png)

> Comprobamos que nuestra gráfica [`Intel® HD Graphics for Intel Atom® Processor Z3700 Series`](https://ark.intel.com/content/www/us/en/ark/products/82103/intel-celeron-processor-n2840-1m-cache-up-to-2-58-ghz.html#tab-blade-1-0-4) es compatible con `OpenCL 1.2`.

# Vulkan

Es una API de cómputo y gráficos 3D multiplataforma de bajo costo. Es un sucesor de [OpenGL](https://wiki.archlinux.org/title/OpenGL).



## Instalación

[Instalamos los paquetes de 64 y 32 bits](https://wiki.archlinux.org/title/Vulkan#Installation), y las herramientas de verificación [`vulkan-tools`](https://archlinux.org/packages/?name=vulkan-tools):

```sh
pacman -S {lib32-,}vulkan{-icd-loader,-intel} vulkan-tools --needed --noconfirm
```

> **Nota**: Use el comando `vulkaninfo` para obtener información relevante sobre su sistema.



## Verificación

Si su gráfica es compatible con `vulkan` debería obtener una salida parecida a la siguiente:

```sh
vulkaninfo --summary
```

![](imgs/vulkaninfoo1.png)

![](imgs/vulkaninfoo2.png)

> **Nota**: Para la gráfica del ejemplo de generación [Bay Trail M](https://en.wikipedia.org/wiki/Silvermont#Mobile_processors_(Bay_Trail-M)) la implementación `vulkan` está incompleta. Aun así, es mejor una implementación incompleta que incompatible.



# Gstreamer

[GStreamer](https://en.wikipedia.org/wiki/GStreamer) es un marco multimedia basado en canalización escrito en el lenguaje de programación C, sobre el cual se basan aplicaciones como [Rhythmbox](https://es.wikipedia.org/wiki/Rhythmbox), [Opera](https://es.wikipedia.org/wiki/Opera_(navegador)) (solo en Linux, Unix), [Clementine](https://es.wikipedia.org/wiki/Clementine_(software)), [Totem](https://es.wikipedia.org/wiki/GNOME_Videos), entre otros.



## Instalación

```sh
pacman -S gstreamer{,-vaapi} gst-{plugins-{base,good,ugly},libav} --needed --noconfirm
```



## Verificación

```sh
gst-inspect-1.0 vaapi
```

![](imgs/gstreamerVaapi.png)



# Benchmark con basemark

El Benchmark se hizo en una máquina distinta, ésta posee un procesador Intel i7 de cuarta generación cuatro núcleos, una gráfica de séptima generación de familia 4400 Haswell, y memoria ram de 8GB.

## Información del hardware

![](imgs/basemarkHardwareInfo.png)





## Configuración de la prueba



### OpenGL

![](imgs/pruebaBasemarkOpengl.png)



### Vulkan

![](imgs/pruebaBasemarkVulkan.png)





## Resultados



### OpenGL

![](imgs/Benchmark/intelOpenGLSimpleFHD.png)

![](imgs/Benchmark/modesettingOpenGLSimpleFHD.png)



### Vulkan

![](imgs/Benchmark/intelVulkanSimpleFHD.png)

![](imgs/Benchmark/modesettingVulkanSimpleFHD.png)



## Comparación

<table>
<thead>
    <tr>
        <th>Driver</th>
        <th>Graphics API</th>
        <th>Version</th>
        <th>Average</th>
        <th>Minimum</th>
        <th>Maximum</th>
        <th>Overall Score</th>
    </tr>
</thead>
<tbody>
	<tr>
        <td rowspan=2><code>intel</code></td>
    	<td>OpenGL</td>
        <td>4.5</td>
        <td>105 fps</td>
        <td>47 fps</td>
        <td>1938 fps</td>
        <td rowspan>C10484</td>
    </tr>
    <tr>
    	<td>Vulkan</td>
        <td>1.0</td>
        <td>105 fps</td>
        <td>40 fps</td>
        <td>2229 fps</td>
        <td rowspan>C9740</td>
    </tr>
    <tr>
        <td rowspan=2><code>modesetting</code></td>
    	<td>OpenGL</td>
        <td>4.5</td>
        <td>97 fps</td>
        <td>45 fps</td>
        <td>1071 fps</td>
        <td rowspan>C10450</td>
    </tr>
    <tr>
    	<td>Vulkan</td>
        <td>1.0</td>
        <td>96 fps</td>
        <td>45 fps</td>
        <td>1062 fps</td>
        <td rowspan>C9645</td>
    </tr>
</tbody>
</table>



## Conclusión

El controlador `intel` siempre va a rendir mejor en gráficas *Intel* de generaciones anterior o igual a la séptima, y `modesetting` va a ser más apropiado en gráficas de generación igual o mayor a la octava. Como siempre habrán casos muy puntuales en los que ésta regla no se cumpla, pero lo que sí es seguro es que de manera general sí se da.

Recuerda que si el *tearing* se da en ambos controladores, aun usando `intel` con la opción `Option TearFree "true"`, entonces debes implementar un compositor como picom y aplicar efectos de desvanecimiento, opacidad y transparencias a su gusto, y la configuración de OpenGL (glx) para mejorar su rendimiento.

La mejor manera de lograr el mejor rendimiento es haciendo una prueba como la anterior, aplicada a ambos controladores.
