# Android Tethering

Es una forma de tener acceso a Internet en su PC a través de su teléfono inteligente usando su conexión de red.

[TOC]

# Anclaje USB

Limpiar instalación previa

```sh
pacman -Rnsc usb_modeswitch --noconfirm
```

```sh
rm -f /etc/{systemd/network/{10-*.link,50-*.network},udev/rules.d/90-tethering-net0.rules}
```

```sh
ls /etc/{systemd/network/,udev/rules.d}
```

Instalar

```sh
pacman -S usb_modeswitch --needed --noconfirm
```



Conecte el cable USB y habilítelo usando uno de los siguientes:

- *Ajustes > Conexión y compartir > Modem USB*
- *Ajustes > Conexiones inalámbricas y redes > Anclaje a Internet > Anclaje USB*



## [Nombre único de la interface](https://wiki.archlinux.org/title/android_tethering#USB_tethering)

Debido a que el nombre de la interface puede cambiar según el puerto USB que utilice y la MAC puede establecerse de forma dinámica, se recomienda establecer un nombre único para el dispositivo independientemente del puerto USB y de la MAC dinámica generada.

Nombre actual de la interface:

```sh
ls /sys/class/net/
enp0s20u3u3  enp0s25  lo  wlan0
```

```sh
ls /sys/class/net/ | grep -E "enp[0-9]s[0-9]{2}([a-z][0-9]){2}|net[0-9]+"
enp0s20u3u3
```

### [Cambiar nombre de la interface](https://wiki.archlinux.org/title/Network_configuration#Change_interface_name)

Obtener identificadores del dispositivo.

`ID_MODEL`:

```sh
udevadm info /sys/class/net/enp0s20u3u3 | awk -F "=" '/ID_MODEL=/ {print $2}'
RMX2001
```

`ID_VENDOR_ID`:

```sh
udevadm info /sys/class/net/enp0s20u3u3 | awk -F "=" '/ID_VENDOR_ID/ {print $2}'
22d9
```

`ID_MODEL_ID`:

```sh
udevadm info /sys/class/net/enp0s20u3u3 | awk -F "=" '/ID_MODEL_ID/ {print $2}'
276a
```

```sh
bash -c 'cat > /etc/systemd/network/10-net0.link' << EOF
# 🌟🌟🌟 Fichero de la interface modem/anclaje USB del realme 6 de dakataca 🌟🌟🌟
# 📂 /etc/systemd/network/10-net0.link 👑
# 🌐 wiki: https://wiki.archlinux.org/title/Network_configuration#Change_interface_name
# 📜 man-page: https://man.archlinux.org/man/systemd.link.5.en
[Match]
Property=ID_VENDOR_ID=22d9 ID_MODEL_ID=276a

[Link]
# Nuevo nombre de la interface!
# No usar nombres de interfaces del tipo ethX, wlanX, enpX; en su lugar usar net0, net1, wifi0, wifi1.
# 🔗 Fuente externa: https://systemd.io/PREDICTABLE_INTERFACE_NAMES/
Name=net0

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Usando systemd-networkd con udev

Ajustar automáticamente la red para usar el teléfono como puerta de enlace cuando está enchufado. Reemplace por el `idVendor` de su interface.

Obtener `ID_VENDOR_ID` :

```sh
udevadm info /sys/class/net/enp0s20u3u3 | awk -F "=" '/ID_VENDOR_ID/ {print $2}'
22d9
```

```sh
bash -c 'cat > /etc/udev/rules.d/90-tethering-net0.rules' << EOF
# 🌟🌟🌟 Reconfigurar la red de manera automática y aprovechar el teléfono android como punto de acceso USB 🌟🌟🌟
# 📂 /etc/udev/rules.d/90-tethering-net$index.rules' 👑
# 🌐 wiki: https://wiki.archlinux.org/title/android_tethering#Using_systemd-networkd_with_udev
# 📜 man-page: https://man.archlinux.org/man/extra/man-pages-de/udev.7.de
# Execute pairing program when appropriate.
#ACTION=="add|remove", SUBSYSTEM=="net", ATTR{idVendor}=="22d9" ENV{ID_USB_DRIVER}=="rndis_host", SYMLINK+="android"
ACTION=="add|remove", SUBSYSTEM=="net", ATTR{idVendor}=="22d9", ATTR{idModel}=="276a", ENV{ID_USB_DRIVER}=="rndis_host", SYMLINK+="android"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

 Éstas reglas se aplicarán en el siguiente inicio. Si desea aplicar las reglas *udev* al subsistema `net` inmediatamente utilice:

```sh
udevadm trigger --verbose --subsystem-match=net --action=add
```



## Fichero de red `systemd-networkd`



Crear el fichero de red `systemd-networkd` correspondiente:

```sh
bash -c 'cat > /etc/systemd/network/50-net0.network' << EOF
# 🌟🌟🌟 Fichero de red systemd-networkd del punto de acceso móvil Android realme 6 de dakataca 🌟🌟🌟
# 📂 /etc/systemd/network/50-net0.network' 👑
# 🌐 wiki: https://wiki.archlinux.org/title/android_tethering#Using_systemd-networkd_with_udev
# 📜 man-page: https://man.archlinux.org/man/systemd.network.5
[Match]
# Nombre del punto de acceso móvil android tethering en 10-net0.link.
Name=net0

[Network]
# Asignar de manera dinámica una ip versión 4.
DHCP=ipv4
# Validación de soporte para DNSSEC.
DNSSEC=true
# Use servidor DNS interno (unbound).
DNS=127.0.0.1

# Configura el cliente DHCPv4, si habilitó con la configuración DHCP=ipv4 o DHCP=yes.
[DHCPv4]
# Minimizar la divulgación de información de identificación.
Anonymize=true
# Métrica de la ruta: Entre menor la ruta mayor preferencia (10 > 20 > 25).
RouteMetric=10

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Reinicie el servicio `systemd-netowrkd` para aplicar los cambios:

```sh
systemctl restart systemd-networkd.service
```



## Comprobar conexión

### Información de la interface:

```sh
ip a show net0
5: net0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 36:16:0e:1e:f3:5b brd ff:ff:ff:ff:ff:ff
    inet 192.168.208.198/24 metric 1024 brd 192.168.208.255 scope global dynamic net0
       valid_lft 2766sec preferred_lft 2766sec
    inet6 fe80::3416:eff:fe1e:f35b/64 scope link proto kernel_ll
       valid_lft forever preferred_lft forever
```

### Salida a internet:

```sh
ping -c3 archlinux.org
PING archlinux.org (95.217.163.246) 56(84) bytes of data.
64 bytes from archlinux.org (95.217.163.246): icmp_seq=1 ttl=40 time=295 ms
64 bytes from archlinux.org (95.217.163.246): icmp_seq=2 ttl=40 time=255 ms
64 bytes from archlinux.org (95.217.163.246): icmp_seq=3 ttl=40 time=228 ms

--- archlinux.org ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 228.493/259.440/295.117/27.403 ms
```

### Tabla de enrutamiento:

```sh
ip route
default via 192.168.208.11 dev net0 proto dhcp src 192.168.208.198 metric 1024
192.168.208.0/24 dev net0 proto kernel scope link src 192.168.208.198 metric 1024
192.168.208.11 dev net0 proto dhcp scope link src 192.168.208.198 metric 1024
```



Si desea verificar los 

# Automatizar Android Tethering



```sh
bash -c 'cat > /etc/udev/rules.d/80-admin-android-tethering.rules' << EOF
# 🌟🌟🌟 Regla udev para administrar interfaces de red Android Tethering conectados vía USB 🌟🌟🌟
# 🌐 wiki: https://wiki.archlinux.org/title/Udev#udev_rule_example
# 📜 man-page: https://man.archlinux.org/man/udevadm.8.en
# 📜 man-page: https://man.archlinux.org/man/core/systemd/udev.7.en
# Detectar cuando se conecta un dispositivo de red Tethering Android.
ACTION=="add", SUBSYSTEM=="net", ENV{ID_USB_DRIVER}=="rndis_host", SYMLINK+="android", RUN+="/usr/bin/android-tethering"

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```

Conecte cable usb del teléfono en modo Android Modem USB (Tethering) y simule la ejecución de un evento udev para el dispositivo dado (`net0`) e imprima la salida de depuración:

```sh
udevadm test /sys/class/net/net0
...
ID_NET_LINK_FILE=/etc/systemd/network/10-net0.link
ID_NET_NAME=net0
CURRENT_TAGS=:systemd:
SYSTEMD_ALIAS=/sys/subsystem/net/devices/net0
USEC_INITIALIZED=15361513014
run: '/home/dakataca/Templates/androidTethering.sh'
run: '/usr/lib/systemd/systemd-sysctl --prefix=/net/ipv4/conf/net0 --prefix=/net/ipv4/neigh/net0 --prefix=/net/ipv6/conf/net0 --prefix=/net/ipv6/neigh/net0'
Unload kernel module index.
Unloaded link configuration context.
```

Comprobar en los registros el script en `/usr/bin/android-tethering`:

```sh
journalctl -u systemd-udevd --grep=android-tethering
```



# punto de acceso wifi

Habilítelo usando uno de los siguientes:

- *Ajustes > Conexiones inalámbricas y redes > Anclaje a Internet > Punto de acceso Wi-Fi*
- *Ajustes > Más... > Anclaje a red y punto de acceso móvil > Punto de acceso Wi-Fi móvil*









