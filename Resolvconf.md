# Resolvconf

## Instalación

```
su
pacman -S openresolv --needed --noconfirm
```

```sh
#bash -c 'cat >> /etc/resolvconf.conf' << EOF
# 🌟🌟🌟 Fichero de configuración de la implementación de resolvconf Openresolv 🌟🌟🌟
# 📂 /etc/resolvconf.conf 👑📝
# 🌐 wiki: https://wiki.archlinux.org/title/Openresolv#Defining_multiple_values_for_options
# 📜 man-page: https://man.archlinux.org/man/resolvconf.conf.5.en
# 📜 man-page: https://man.archlinux.org/man/resolv.conf.5.en

# Utilizar servidor DNS interno.
name_servers="::1 127.0.0.1"
# Confiar el bit AD tanto a la ruta red como a la validación de DNSSEC. Debe habilitar DNSSEC en el administrador de red completo (systemd-netowrkd).
# 🌐 wiki: https://wiki.archlinux.org/title/Openresolv#Defining_multiple_values_for_options
# 🌐 wiki: https://wiki.archlinux.org/title/Dnscrypt-proxy#Modify_resolv.conf
# 🌐 wiki: https://wiki.archlinux.org/title/Dnscrypt-proxy_(Espa%C3%B1ol)#Modificar_resolv.conf
resolv_conf_options="trust-ad edns0"

# 🌐 wiki: https://wiki.archlinux.org/title/Unbound#Using_openresolv
private_interface="*"

# Escribir el fichero de configuración independiente de unbound.
unbound_conf=/etc/unbound/resolvconf.conf

# 👤 Autor: https://t.me/dakataca 💻 🐬 #
EOF
```



## Junto a Unbound

```sh
nvim /etc/unbound/unbound.conf
---------------------------------------------------------
# 🌟🌟🌟 Edite la configuración del servidor DNS unbound (antiguo bind) 🌟🌟🌟
# 📂 /etc/unbound/unbound.conf 👑📝
...
# 🌐 wiki: https://wiki.archlinux.org/title/Unbound#Using_openresolv_2
include: "/etc/unbound/resolvconf.conf"
```

```sh
resolvconf -u
exit
```
