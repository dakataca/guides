



[toc]

# Configurar segunda pantalla

## Listar monitores

```sh
xrandr --listmonitors
-----------------------------------------------------------------------
Monitors: 2
 0: +*eDP1 1366/310x768/170+0+0  eDP1
 1: +DP2 1366/410x768/230+0+768  DP2
```

> `eDP1`: Pantalla principal (portátil).
> `DP2`: Pantalla secundaria.



## Extender pantalla temporalmente

Extender pantalla secundaria `DP2` temporalmente hacia abajo de la principal `eDP1`:

```sh
xrandr --output eDP1 --auto --output DP2 --auto --below eDP1
```

Ahora deberia aplicarse transitoriamente la configuración, compruebe con el mouse el desplazamiento en cada pantalla.

¤-------------------------------------¤
|              **eDP1**                       |
|        `1366x768  60KHz`        |
|                                              |
¤-------------------------------------¤
|                **DP2**                       |
|        `1366x768  60KHz`        |
|                                              |
¤-------------------------------------¤



## [Configuración permanente](https://man.archlinux.org/man/extra/xorg-server/xorg.conf.5.en#MONITOR_SECTION)

Si la configuración temporal fue satisfactoria establezca de manera permanente dicha configuación.

### Modeline

Seleccionamos el *Modeline* con mejores números:

```sh
grep -E 'Monitor name:*|Modeline "[1-6][0-9]{3}x[1579][0-9]{,4}("x[4-9][0-9]|_[4-9][0-9]).*' .local/share/xorg/Xorg.0.log
#grep -oE 'Monitor name: |Modeline "[1-6][0-9]{3}x[1579][0-9]{,4}"x[4-9][0-9]\.[0-9].*' .local/share/xorg/Xorg.0.log
#grep 'Monitor name: \| +[hv]sync' .local/share/xorg/Xorg.0.log
grep 'Modeline "1\|Monitor name:' .local/share/xorg/Xorg.0.log
..........................................................................
[  2783.877] (II) intel(0): Modeline "1366x768"x0.0   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (47.5 kHz eP)
[  2783.877] (II) intel(0): Modeline "1366x768"x0.0   50.80  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (31.6 kHz e)
[  2783.877] (II) intel(0): Modeline "1366x768"x60.0   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (47.5 kHz eP)
[  2783.877] (II) intel(0): Modeline "1366x768_60.0"x60.0   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (47.5 kHz)
[  2783.877] (II) intel(0): Modeline "1366x768"x40.0   50.80  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (31.6 kHz e)
[  2783.877] (II) intel(0): Modeline "1280x720"x59.9   74.50  1280 1344 1472 1664  720 723 728 748 -hsync +vsync (44.8 kHz d)
[  2783.877] (II) intel(0): Modeline "1280x720"x60.0   74.48  1280 1336 1472 1664  720 721 724 746 -hsync +vsync (44.8 kHz)
[  2783.877] (II) intel(0): Modeline "1280x720"x59.7   63.75  1280 1328 1360 1440  720 723 728 741 +hsync -vsync (44.3 kHz d)
[  2783.877] (II) intel(0): Modeline "1024x768"x60.0   65.00  1024 1048 1184 1344  768 771 777 806 -hsync -vsync (48.4 kHz d)
[  2783.877] (II) intel(0): Modeline "1024x576"x60.0   46.99  1024 1064 1168 1312  576 577 580 597 -hsync +vsync (35.8 kHz)
[  2783.877] (II) intel(0): Modeline "1024x576"x59.9   46.50  1024 1064 1160 1296  576 579 584 599 -hsync +vsync (35.9 kHz d)
[  2783.877] (II) intel(0): Modeline "1024x576"x59.8   42.00  1024 1072 1104 1184  576 579 584 593 +hsync -vsync (35.5 kHz d)
[  2783.933] (II) intel(0): Monitor name: SA300/SA350
[  2783.933] (II) intel(0): Modeline "1366x768_60.0"x59.8   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync (47.7 kHz UP)
[  2783.933] (II) intel(0): Modeline "1366x768"x60.0   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync (47.7 kHz eP)
[  2783.933] (II) intel(0): Modeline "1024x768"x75.0   78.75  1024 1040 1136 1312  768 769 772 800 +hsync +vsync (60.0 kHz e)
[  2783.933] (II) intel(0): Modeline "1024x768"x70.0   75.00  1024 1048 1184 1328  768 771 777 806 -hsync -vsync (56.5 kHz e)
[  2783.933] (II) intel(0): Modeline "1024x768"x60.0   65.00  1024 1048 1184 1344  768 771 777 806 -hsync -vsync (48.4 kHz e)
```

> **Monitor del protátil**:`[  2783.877] (II) intel(0): Modeline "1366x768_60.0"x60.0   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync (47.5 kHz)`.
> **Monitor SA300/SA350**: `[  2783.933] (II) intel(0): Modeline "1366x768"x60.0   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync (47.7 kHz eP)`



### Monitor del portátil

Adaptar al formato modeline:

```sh
Modeline "1366x768_60.0_60.0" 76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync
```

> **Nota**: Nótese que no permite la sincronización horizontal ni vertical (`-hsync -vsync`).

Configuración:

```sh
Section "Monitor"
    VendorName	"Hewlett Packard"
    ModelName	"14 Inch"
    Identifier  "eDP1"
    Modeline	"1366x768_60.0"   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync
    Option	"PreferredMode"	"1366x768_60.00"
    Option      "Primary" "true"
EndSection
```



### Monitor Samsung SA300

Adaptar al formato modeline:

```sh
Modeline "1366x768_60.0" 85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync
```

> **Nota**: Soporta sincronización horizontal y vertical (`+hsync +vsync`).

Configuración:

```sh
# Monitor LED Samsung Widescreen 18.5" SyncMaster SA300.
# https://pcel.com/Samsung-LS19A300NS-ZX-Monitor-strongLED-strong-Samsung-Widescreen-18-5-SyncMaster-SA300-Contraste-dinamico-5000000-1-74865
Section "Monitor"
    Identifier  "DP2"
    VendorName	"Samsung"
    ModelName	"SyncMaster SA300 18.5 Inch"
    Modeline	"1366x768_60.0"   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync
    Option	"PreferredMode"	"1366x768_60.0"
    Option	"Below" "eDP1"      # Debajo de la pantalla del portátil (eDP1).
EndSection
```



### Fichero de configuración

```sh
nvim /etc/X11/xorg.conf.d/10-monitor.conf
----------------------------------------------------------------------
Section "Monitor"
    VendorName	"Hewlett Packard"
    ModelName	"14 Inch"
    Identifier  "eDP1"
    Modeline	"1366x768_60.0"   76.30  1366 1430 1478 1606  768 770 775 792 -hsync -vsync
    Option	"PreferredMode"	"1366x768_60.00"
    Option      "Primary" "true"
EndSection

# Monitor LED Samsung Widescreen 18.5" SyncMaster SA300.
# https://pcel.com/Samsung-LS19A300NS-ZX-Monitor-strongLED-strong-Samsung-Widescreen-18-5-SyncMaster-SA300-Contraste-dinamico-5000000-1-74865
Section "Monitor"
    Identifier  "DP2"
    VendorName	"Samsung"
    ModelName	"SyncMaster SA300 18.5 Inch"
    Modeline	"1366x768_60.0"   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync
    Option	"PreferredMode"	"1366x768_60.0"
    Option	"Below" "eDP1"      # Debajo de la pantalla del portátil.
EndSection
```
