**Información del Sistema Desde la Terminal Linux**



[TOC]



# Hardware

Procesador:

```sh
lscpu
```

```sh
hwinfo --cpu
```

> **NOTA:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Memoria RAM y SWAP

```sh
free -h
```

```sh
swapon
```

Monitores y gráficas

```sh
hwinfo --monitor
```

> **NOTA:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

```sh
lspci | awk -F ": " '/VGA|3D/{print $2}'
```

```sh
inxi -Ga
```

> **Nota:** Requiere el paquete [inxi](https://archlinux.org/packages/extra/any/inxi/).

Dispositivos PCI:

```sh
lspci
```

```sh
hwinfo --pci
```

> **Nota:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Dispositivos de bloque:

```sh
lsblk
```

```
hwinfo --disk
```

> **Nota:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Tipo de conexión SATA

```sh
sudo dmesg | grep "SATA link up"
```

Dispositivo de bloque específico: 

```sh
lsblk -po NAME,FSTYPE,LABEL,MOUNTPOINT,SIZE /dev/mmcblk1
```

Particiones físicas de dispositivos de bloque

```
hwinfo --partition
```

> **Nota:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Información de dispositivos lsscsi (Small Computer System Interface) como DVD's y discos duros.

```sh
lsscsi
```

> **Nota:** Requiere el paquete  [lsscsi](https://archlinux.org/packages/community/x86_64/lsscsi/).

Espacio utilizado y disponible de dispositivos de bloque:

```sh
df -h
```

```sh
duf
```

> **Nota:** Requiere el paquete [duf](https://archlinux.org/packages/extra/x86_64/duf/).

Arbol de conexiones usb:

```sh
lsusb -t
```

Arranque seguro habilitado/deshabilitado.

```sh
sudo dmesg | grep 'Secure boot'
```

## Usando dmidecode

> **Nota:** Requiere paquete [dmidecode](https://archlinux.org/packages/extra/x86_64/dmidecode/).

```sh
dmidecode -t
```

> dmidecode: option requires an argument -- 't'
> Type number or keyword expected
> Valid type keywords are:
>   bios
>   system
>   baseboard
>   chassis
>   processor
>   memory
>   cache
>   connector
>   slot

Memoria ram:

```sh
dmidecode -t memory
```

> **Nota:** Requiere paquete [dmidecode](https://archlinux.org/packages/extra/x86_64/dmidecode/).



## Dispositivos de red

Dispositivos de red PCI

```sh
lspci | grep 'Ethernet\|Network'
```

Dispositivos de red USB

```sh
lsusb | grep 'Ethernet\|Network'
```

Dispositivos de red

```sh
hwinfo --network
```

> **NOTA:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Dispositivos de red inalámbrica

```sh
hwinfo --wlan
```

> **Nota:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).



# Tiempo, temperatura, fecha y hora

Temperatura:

```sh
sensors
```

Hora y fecha actual:

```sh
date
```

Calendario del mes actual:

```sh
cal
```

Calendario de 2024:

```sh
cal 2024
```

Previsión meteorológica:

```sh
curl wttr.in
```

Previsión meteorológica de una ciudad:

```sh
curl wttr.in/sincelejo
```



# Conexión de red 🛜

Tabla de enrutamiento:

```sh
sudo route -n
```

Verificar servidor DNS:

```sh
drill badsig.go.dnscheck.tools
```

```sh
drill go.dnscheck.tools
```

MAC Adress de interface cableada

```sh
ip address | sed -nE '/^[0-9]{,2}:.*enp.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}'
```

MAC Adress de interface inalámbrica

```sh
ip address | sed -nE '/^[0-9]{,2}:.*wl.*/ {N; s/.*ether.*([0-9a-f:]{17}).*brd.*/\1/p}'
```



# Servidores gráficos



## Xorg

Frames por segundo:

```sh
timeout 60 glxgears
```

Verificar soporte de gráficos:

```sh
glxinfo | grep -i render
```

```sh
glxinfo | grep OpenGL
```

Resolución actual:

```sh
xrandr | grep '*'
```

Controlador usado por la gráfica.

```sh
sed -nE "/(DRI|VDPAU) driver/ s/^.*II\) //p" {~/.local/share/xorg,/var/log}/Xorg.0.log 2>/dev/null
```

> intel(0): [DRI2]   DRI driver: crocus
> intel(0): [DRI2]   VDPAU driver: va_gl

```sh
sed -nE "/(DRI|VDPAU) driver/ s/^.*driver: //p" {~/.local/share/xorg,/var/log}/Xorg.0.log 2>/dev/null
```

> crocus
> va_gl



## Wayland

### Sway

Aplicaciones ejecutándose (sway)

```sh
swaymsg -t get_tree | grep 'app_id\|class' | uniq
```



# Sistema operativo

Arquitectura

```sh
hwinfo --arch
```

> **Nota:** Requiere el paquete [hwinfo](https://archlinux.org/packages/extra/x86_64/hwinfo/).

Usuarios conectados y tiempo de uso

```sh
who -a
```

Nombre del usuario conectado

```sh
whoami
```

Antigüedad del sistema que tengo instalado

```sh
stat -c %w /
```

```sh
ls -lct --time-style=+"%d-%m-%Y %H:%M:%S" / | tail -l | awk '{print $6, $7, $8}'
```

Nombre y versión del kernel, hardware (xi86 o x86_64), nombre del host del nodo de red y sistema operativo

```sh
uname -snrmo
```

Driver usado por dispositivos PCI:

```sh
lspci -k
```

Memoria usada y disponible

```sh
free -h
```

### Espacio de intercambio

Apagar espacio de intercambio

```sh
swapoff -a
```

Encender espacio de intercambio

```sh
swapon -a
```

Reiniciar espacio de intercambio

```sh
swapoff -a && swapon -a
```

Tiempo de uso

```sh
uptime
```



### Kernel

Parámetros del kernel aplicados en GRUB

```sh
cat /proc/cmdline
```



Promedio de la temperatura

$ sensors -A | awk '$1 ~ /temp1:/ { sum+= $2 } END {printf "%.1f°C\n", sum/3}'

- Esto omite imprimir "adapter" -A.
- Suma las tres temperaturas y las promedia.
- Limita la salida decimal a un dígito seguido del punto.





Referencias
- computernewage.com
 (https://computernewage.com/2013/04/21/como-obtener-informacion-del-sistema-desde-la-terminal-de-linux/#cal)- cybercity

 (https://www.cyberciti.biz/faq/linux-command-to-find-sata-harddisk-link-speed/)Más información del sistema en https://pastebin.com/9itu8v0w.